<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Episode extends Model
{
	protected $fillable = ['nama_episode', 'slug_episode', 'v240p', 'nama_link1_240p', 'link1_240p', 'nama_link2_240p', 'link2_240p', 'nama_link3_240p', 'link3_240p', 'nama_link4_240p', 'link4_240p', 'v360p', 'nama_link1_360p', 'link1_360p', 'nama_link2_360p', 'link2_360p', 'nama_link3_360p', 'link3_360p', 'nama_link4_360p', 'link4_360p', 'v480p', 'nama_link1_480p', 'link1_480p', 'nama_link2_480p', 'link2_480p', 'nama_link3_480p', 'link3_480p', 'nama_link4_480p', 'link4_480p', 'v720p', 'nama_link1_720p', 'link1_720p', 'nama_link2_720p', 'link2_720p', 'nama_link3_720p', 'link3_720p', 'nama_link4_720p', 'link4_720p',];
	protected $table = 'episode';
	protected $primaryKey = 'id_episode';

	public function anime()
	{
		return $this->belongsToMany('App\Anime');
	}
}

?>