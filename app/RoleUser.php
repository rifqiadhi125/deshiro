<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class RoleUser extends Model
{
    protected $fillable = ['nama_role'];
	protected $table = 'role_user';
    protected $primaryKey = 'id_role';
}
