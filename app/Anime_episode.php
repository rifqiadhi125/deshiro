<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anime_episode extends Model
{
	protected $fillable = ['anime_id_anime', 'episode_id_episode'];
	protected $table = 'anime_episode';
	protected $primaryKey = 'id_anime_episode';

	public function anime()
	{
		return $this->belongsToMany('App\Anime');
	}
}

?>