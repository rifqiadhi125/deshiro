<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _Service extends Model
{
    protected $fillable = ['service_id', 'service_title', 'service_desc', 'service_images', 'service_status', 'service_create_date'];
    protected $table = '_service';
    protected $primaryKey = 'service_id';
    public $timestamps = false;
}
