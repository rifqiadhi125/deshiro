<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{
    protected $fillable = ['nama', 'gender', 'alamat'];
	protected $table = 'customer';
	protected $primaryKey = 'id';
}
