<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _Slider extends Model
{
    protected $fillable = ['slider_id', 'slider_type', 'slider_title', 'slider_shortdesc', 'slider_image', 'slider_url', 'slider_status','slider_create_at'];
    protected $table = '_slider';
    protected $primaryKey = 'slider_id';
    public $timestamps = false;
}
