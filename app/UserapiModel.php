<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class UserapiModel extends Model
{
    protected $fillable = ['portfolio_id', 'portfolio_title', 'portfolio_sortdesc', 'portfolio_desc','portfolio_image','portfolio_status','portfolio_create_date'];
    protected $table = '_portfolio';
    protected $primaryKey = 'portfolio_id';
    public $timestamps = false;
}
