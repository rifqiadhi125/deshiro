<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _Setting extends Model
{
    protected $fillable = ['setting_id', 'setting_type', 'setting_name', 'setting_value'];
    protected $table = '_setting';
    protected $primaryKey = 'setting_id';
}
