<?php

namespace App\Http\Controllers;

use App\Anime;
use App\Anime_episode;
use App\Download_server;
use App\Episode;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;

class EpisodeControllerBackup extends Controller
{
	public function tambah_episode($id_anime)
	{
		$anime = Anime::where('id_anime', $id_anime)->first();
		$server = Download_server::all();
		return view('admin/episode/tambah', compact('anime', 'server'));
	}

	public function tambah_simpan_episode(Request $data)
	{
		$episode = Episode::create([
			'nama_episode' => $data->nama_episode,
			'slug_episode' => $data->slug_anime . Str::slug($data->nama_episode),
			'v240p' => $data->v240p,
			'nama_link1_240p' => $data->nama_link1_240p,
			'link1_240p' => $data->link1_240p,
			'nama_link2_240p' => $data->nama_link2_240p,
			'link2_240p' => $data->link2_240p,
			'nama_link3_240p' => $data->nama_link3_240p,
			'link3_240p' => $data->link3_240p,
			'nama_link4_240p' => $data->nama_link4_240p,
			'link4_240p' => $data->link4_240p,
			'v360p' => $data->v360p,
			'nama_link1_360p' => $data->nama_link1_360p,
			'link1_360p' => $data->link1_360p,
			'nama_link2_360p' => $data->nama_link2_360p,
			'link2_360p' => $data->link2_360p,
			'nama_link3_360p' => $data->nama_link3_360p,
			'link3_360p' => $data->link3_360p,
			'nama_link4_360p' => $data->nama_link4_360p,
			'link4_360p' => $data->link4_360p,
			'v480p' => $data->v480p,
			'nama_link1_480p' => $data->nama_link1_480p,
			'link1_480p' => $data->link1_480p,
			'nama_link2_480p' => $data->nama_link2_480p,
			'link2_480p' => $data->link2_480p,
			'nama_link3_480p' => $data->nama_link3_480p,
			'link3_480p' => $data->link3_480p,
			'nama_link4_480p' => $data->nama_link4_480p,
			'link4_480p' => $data->link4_480p,
			'v720p' => $data->v720p,
			'nama_link1_720p' => $data->nama_link1_720p,
			'link1_720p' => $data->link1_720p,
			'nama_link2_720p' => $data->nama_link2_720p,
			'link2_720p' => $data->link2_720p,
			'nama_link3_720p' => $data->nama_link3_720p,
			'link3_720p' => $data->link3_720p,
			'nama_link4_720p' => $data->nama_link4_720p,
			'link4_720p' => $data->link4_720p
		]);

		$episode->Anime()->attach($data->id_anime);

		return redirect('admin/anime/edit/'.$data->id_anime)->with('success', 'Anime berhasil ditambahkan');
	}

	public function edit_episode($id)
	{
		$episode = Episode::findorfail($id);
		$episode_anime = Anime_episode::where('episode_id_episode', $id)->first();
		$anime = Anime::where('id_anime', $episode_anime->anime_id_anime)->first();
		$server = Download_server::all();
		return view('admin/episode/edit', compact('episode', 'anime', 'server'));
	}

	public function update_episode(Request $data, $id)
	{
		$episode_data = Episode::findorfail($id);

		$episode = [
			'nama_episode' => $data->nama_episode,
			'slug_episode' => $data->slug_anime . Str::slug($data->nama_episode),
			'v240p' => $data->v240p,
			'nama_link1_240p' => $data->nama_link1_240p,
			'link1_240p' => $data->link1_240p,
			'nama_link2_240p' => $data->nama_link2_240p,
			'link2_240p' => $data->link2_240p,
			'nama_link3_240p' => $data->nama_link3_240p,
			'link3_240p' => $data->link3_240p,
			'nama_link4_240p' => $data->nama_link4_240p,
			'link4_240p' => $data->link4_240p,
			'v360p' => $data->v360p,
			'nama_link1_360p' => $data->nama_link1_360p,
			'link1_360p' => $data->link1_360p,
			'nama_link2_360p' => $data->nama_link2_360p,
			'link2_360p' => $data->link2_360p,
			'nama_link3_360p' => $data->nama_link3_360p,
			'link3_360p' => $data->link3_360p,
			'nama_link4_360p' => $data->nama_link4_360p,
			'link4_360p' => $data->link4_360p,
			'v480p' => $data->v480p,
			'nama_link1_480p' => $data->nama_link1_480p,
			'link1_480p' => $data->link1_480p,
			'nama_link2_480p' => $data->nama_link2_480p,
			'link2_480p' => $data->link2_480p,
			'nama_link3_480p' => $data->nama_link3_480p,
			'link3_480p' => $data->link3_480p,
			'nama_link4_480p' => $data->nama_link4_480p,
			'link4_480p' => $data->link4_480p,
			'v720p' => $data->v720p,
			'nama_link1_720p' => $data->nama_link1_720p,
			'link1_720p' => $data->link1_720p,
			'nama_link2_720p' => $data->nama_link2_720p,
			'link2_720p' => $data->link2_720p,
			'nama_link3_720p' => $data->nama_link3_720p,
			'link3_720p' => $data->link3_720p,
			'nama_link4_720p' => $data->nama_link4_720p,
			'link4_720p' => $data->link4_720p
		];

		$episode_data->update($episode);

		return redirect('admin/anime/edit/'.$data->id_anime)->with('success', 'Anime berhasil diupdate');
	}

	public function hapus_episode($id)
	{
		$episode = Episode::findorfail($id);
		$episode_anime = Anime_episode::where('episode_id_episode', $id)->first();
		$episode->delete();
		$episode_anime->delete();

		return redirect()->back()->with('success', 'Anime berhasil dihapus');
	}

}

?>
