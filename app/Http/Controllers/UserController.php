<?php

namespace App\Http\Controllers;

use App\User;
use File;
use SweetAlert;
use App\RoleUser;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Hash;

class UserController extends Controller
{
    public function tampil_user()
	{
		$user = User::paginate(10);
		return view('admin/user', compact('user'));
    }
    
    public function tambah_user()
	{
		$roleuser = RoleUser::all();
		return view('admin/user/tambah', compact('roleuser'));
    }
    
    public function tambah_simpan_user(Request $data, User $user)
    {
        if ($data->has('foto_profile')) {
            $foto = $data->foto_profile;
            $new_foto = time().$foto->getClientOriginalName();
			$foto->move('assets/uploads/foto_profile/', $new_foto);

			$user = User::create([
				'foto_profile' => $new_foto,
				'name' => $data->nama_user,
                'email' => $data->email,
                'password' => Hash::make($data->password),
				'role' => $data->role
            ]);
		}
		else{
			$user = User::create([
				'name' => $data->nama_user,
                'email' => $data->email,
                'password' => Hash::make($data->password),
				'role' => $data->role
            ]);
		}
		
        return redirect('admin/user')->with('success', 'User berhasil ditambahkan');
    }

    public function edit_user($id)
	{
		$user = User::findorfail($id);
		$roleuser = RoleUser::all();
		return view('admin/user/edit', compact('user', 'roleuser'));
    }
    
    public function update_user(Request $data, $id)
	{
		$user_data = User::findorfail($id);

		if ($data->has('foto_profile')) {
			$foto = $data->foto_profile;
			$new_foto = time().$foto->getClientOriginalName();
			$foto->move('assets/uploads/foto_profile/', $new_foto);

			$user = [
				'foto_profile' => $new_foto,
				'name' => $data->nama_user,
                'email' => $data->email,
				'role' => $data->role
			];

            if($user_data->foto_profile === 'user-pic.jpg'):
            else:
            File::delete('assets/uploads/thumbnail/'.$user_data->foto_profile);
            endif;
		}
		else{
			$user = [
				'name' => $data->nama_user,
                'email' => $data->email,
				'role' => $data->role
			];
		}

		$user_data->update($user);

		return redirect('admin/user')->with('success', 'User berhasil diupdate');
	}

	public function update_password(Request $data, $id)
	{
		$user_data = User::findorfail($id);

		$password_lama = $data->password_lama;
		$password_baru = Hash::make($data->password_baru);

		if(Hash::check($password_lama, $user_data->password)):
			$user = [
				'password' => $password_baru
			];

			$user_data->update($user);
			return redirect()->back()->with('success', 'Password User berhasil diupdate');
		else:
			return redirect()->back()->with('error', 'Password User yang lama salah');
		endif;
	}

	public function hapus_user($id)
	{
		$user = User::findorfail($id);
		File::delete('assets/uploads/foto_profile/'.$user->foto_profile);
		$user->delete();

		return redirect()->back()->with('success', 'User berhasil dihapus');
	}
}
