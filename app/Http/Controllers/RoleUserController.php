<?php

namespace App\Http\Controllers;

use App\RoleUser;
use Illuminate\Http\Request;

class RoleUserController extends Controller
{
    public function tampil_role_user()
	{
		$role = RoleUser::paginate(10);
		return view('admin/roleuser', compact('role'));
    }
    
    public function tambah_simpan_role_user(Request $data)
	{
		$role = RoleUser::create([
			'nama_role' => $data->nama_role
		]);

		return redirect()->back()->with('success', 'Role User berhasil ditambahkan');
	}

	public function edit_role_user($id)
	{
		$role = RoleUser::paginate(10);
		$editrole = RoleUser::findorfail($id);
		return view('admin/roleuser', compact('editrole', 'role'));
	}

	public function update_role_user(Request $data, $id)
	{
		$role = [
			'nama_role' => $data->nama_role
		];

		RoleUser::whereid_role($id)->update($role);
		return redirect('admin/role-user')->with('success', 'Role User berhasil diedit');
	}

	public function hapus_role_user($id)
	{
		$role = RoleUser::findorfail($id);
		$role->delete();

		return redirect('admin/role-user')->with('success', 'Role User berhasil dihapus');
	}
}
