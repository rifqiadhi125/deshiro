<?php

namespace App\Http\Controllers;
use App\_Service;
use Illuminate\Http\Request;

class ServiceController extends Controller
{
    public function tampil_service()
    {
        $service = _Service::orderBy('service_create_date','desc')->paginate(10);
        return view('admin/service', compact('service'));
    }

    public function tambah_service()
    {
        return view('admin/service/tambah-service');
    }

    public function tambah_simpan_service(Request $data)
    {
        $status = $data->status;
        if ($status === 'on'):
            $status_service = '1';
        else:
            $status_service = '0';
        endif;

        date_default_timezone_set('Asia/Jakarta');
        $slider = _Service::create([
            'service_title' => $data->judul,
            'service_images' => $data->gambar,
            'service_desc' => $data->deskripsi,
            'service_status' => $status_service,
            'service_create_date' => date('Y-m-d H:i:s')
        ]);

        return redirect('admin/service')->with('success', 'Service berhasil ditambahkan');
    }

    public function edit_service($id)
    {
        $service = _Service::findorfail($id);
        return view('admin/service/edit-service', compact('service'));
    }

    public function update_service(Request $data, $id)
    {
        $status = $data->status;
        if ($status === 'on'):
            $status_service = '1';
        else:
            $status_service = '0';
        endif;

        $service = [
            'service_title' => $data->judul,
            'service_desc' => $data->deskripsi,
            'service_status' => $status_service
        ];

        _Service::whereservice_id($id)->update($service);
        return redirect('admin/service')->with('success', 'Service berhasil diupdate');
    }

    public function update_gambar(Request $data, $id)
    {
        $service = [
            'service_images' => $data->gambar
        ];

        _Service::whereservice_id($id)->update($service);
        return redirect()->back()->with('success', 'Gambar Service berhasil diedit');
    }

    public function hapus_service($id)
    {
        $service = _Service::findorfail($id);
        $service->delete();

        return redirect()->back()->with('success', 'Service berhasil dihapus');
    }


}
