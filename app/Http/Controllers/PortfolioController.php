<?php

namespace App\Http\Controllers;
use App\Portfolio;
use Illuminate\Http\Request;


class PortfolioController extends Controller
{
    public function tampil_portfolio()
    {
        $dataPortfolio = Portfolio::orderBy('portfolio_create_date','desc')-> paginate(10);
        return view('admin/portfolio/index', compact('dataPortfolio'));
    }
    public function tambah_portfolio()
    {

        return view('admin/portfolio/tambah_port');
    }

    public function edit()
    {
        $dataPortfolio = Portfolio::paginate(10);
        return view('admin/portfolio/edit', compact('dataPortfolio'));
    }
    public function hapus(Request  $Portfolio)
    {

        Portfolio::destroy($Portfolio->id);
        return redirect('admin/portfolio');

    }
    public function simpan(Request $request)
    {
//       dd($_POST);exit();
        $Portfolio = new Portfolio;

        $Portfolio->portfolio_title = $request->judulPortfolio;
        $Portfolio->portfolio_desc = $request->DeskripsiPortfolio;
        $Portfolio->portfolio_sortdesc = $request->Deskripsisingakt;
        $Portfolio->portfolio_status = $request->status;
        $Portfolio->portfolio_image = $request->gambar;
        $Portfolio->portfolio_create_date = date('Y-m-d H:i:s');

        $Portfolio->save();

        return redirect('admin/portfolio');
    }

    public function edit_portfolio($id)
    {
        $portfolio = Portfolio::findorfail($id);
        return view('admin/portfolio/edit', compact('portfolio'));
    }
    public function update(Request $data, $id)
    {
        $portfolio =[
            'portfolio_title' => $data ->judul,
            'portfolio_sortdesc' => $data ->deskripsi,
            'portfolio_desc' => $data ->deskripsi,
            'portfolio_status' => $data -> status


        ];
        Portfolio::where( 'portfolio_id', $id)->update($portfolio);
        return redirect('admin/portfolio');
    }
    public  function update_gambar(Request $data, $id ){
        $portfolio =[
            'portfolio_image' => $data -> gambar
        ];
        Portfolio::whereportfolio_id($id)->update($portfolio);
        return redirect()->back();
    }


}
