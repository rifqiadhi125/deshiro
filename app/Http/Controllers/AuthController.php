<?php 

namespace App\Http\Controllers;

Use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Session;

class AuthController extends Controller
{
	public function loginform()
	{
		return view('login');
	}

	public function registerform()
	{
		return view('register');
	}

	public function login(Request $data)
	{
		$data_login = $data->only('email', 'password');

	    if (Auth::attempt($data_login))
	    {
	    	if (Auth::user()->role == '1') {
	    		return redirect('/admin/dashboard');
	    	}else{
	    		return redirect('/user/dashboard');
	    	}
	        
	    }
	    return redirect('/login')->with('error', 'Username atau Password Salah!');
	}

	public function register(Request $request)
	{  
	    $data = $request->all();
	 
	    $check = $this->create($data);
	       
	    return redirect('login')->with('success', 'Akun berhasil dibuat, silahkan login pada form dibawah!');
	}

	public function create(array $data)
    {
      return User::create([
        'email' => $data['email'],
        'password' => Hash::make($data['password']),
        'role' => '2'
      ]);
    }

	public function logout(Request $data)
	{
	    if(Auth::check())
	    {
	        Auth::logout();
	        $data->session()->invalidate();
	    }
	    return  redirect('login');
	}

}
?>