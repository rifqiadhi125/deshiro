<?php

namespace App\Http\Controllers;

use App\User;
use DB;

use App\Charts\UserChart;
use Illuminate\Http\Request;

class UserChartController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function chartLine()

    {

        $api = url('/chart-line-ajax');

        $chart = new UserChart;

        $chart->labels(['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Dec'])->load($api);

        return view('chartLine', compact('chart'));

    }

    /**

     * The attributes that are mass assignable.

     *

     * @var array

     */

    public function chartLineAjax(Request $request)

    {
        $users = User::select(DB::raw("COUNT(*) as count"))

                    ->groupBy(DB::raw("Day(created_at)"))

                    ->pluck('count');

        $chart = new UserChart;

        $chart->dataset('New User Register Chart', 'line', $users)->options([

                    'fill' => 'true',

                    'borderColor' => '#51C1C0'

                ]);

        return $chart->api();

    }
}
