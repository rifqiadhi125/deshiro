<?php

namespace App\Http\Controllers;

use App\_Setting;
use Illuminate\Http\Request;

class SettingController extends Controller
{
    public function tampil_setting()
    {
        return view('admin/setting-general');
    }

    public function tampil_about()
    {
        return view('admin/setting-about');
    }

    public function tampil_privacy()
    {
        return view('admin/setting-privacy');
    }

    public function update_setting(Request $request)
    {
        $name =  _Setting::where('setting_type', '=', 'app')
                    ->where('setting_name', '=', 'name')
                    ->update(['setting_value' => $request->formname]);

        $instagram =  _Setting::where('setting_type', '=', 'contact')
            ->where('setting_name', '=', 'instagram_link')
            ->update(['setting_value' => $request->forminstagram]);

        $facebook =  _Setting::where('setting_type', '=', 'contact')
            ->where('setting_name', '=', 'facebook_link')
            ->update(['setting_value' => $request->formfacebook]);

        $youtube =  _Setting::where('setting_type', '=', 'contact')
            ->where('setting_name', '=', 'youtube_link')
            ->update(['setting_value' => $request->formyoutube]);

        $phone =  _Setting::where('setting_type', '=', 'contact')
            ->where('setting_name', '=', 'phone')
            ->update(['setting_value' => $request->formphone]);

        $whatsapp =  _Setting::where('setting_type', '=', 'contact')
            ->where('setting_name', '=', 'whatsapp')
            ->update(['setting_value' => $request->formwhatsapp]);

        $email =  _Setting::where('setting_type', '=', 'contact')
            ->where('setting_name', '=', 'email')
            ->update(['setting_value' => $request->formemail]);

        $alamat =  _Setting::where('setting_type', '=', 'contact')
            ->where('setting_name', '=', 'address')
            ->update(['setting_value' => $request->formaddress]);

        return redirect('admin/setting-general')->with('success', 'Berhasil memperbarui setting general');

    }

    public function update_about(Request $request)
    {
        $about =  _Setting::where('setting_type', '=', 'app')
            ->where('setting_name', '=', 'about')
            ->update(['setting_value' => $request->formabout]);

        return redirect('admin/setting-about')->with('success', 'Berhasil memperbarui setting about');

    }

    public function update_privacy(Request $request)
    {
        $about =  _Setting::where('setting_type', '=', 'app')
            ->where('setting_name', '=', 'privacy')
            ->update(['setting_value' => $request->formprivacy]);

        return redirect('admin/setting-privacy')->with('success', 'Berhasil memperbarui setting privacy');

    }
}
