<?php 

namespace App\Http\Controllers;

use App\Genre;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;

class GenreController extends Controller
{
	public function tampil_genre()
	{
		$genre = Genre::paginate(10);
		return view('admin/genre', compact('genre'));
	}

	public function tambah_genre()
	{
		return view('admin/genre/tambah');
	}

	public function tambah_simpan_genre(Request $data)
	{
		$genre = Genre::create([
			'nama_genre' => $data->nama_genre,
			'slug_genre' => Str::slug($data->nama_genre)
		]);

		return redirect()->back()->with('success', 'Genre berhasil ditambahkan');
	}

	public function edit_genre($id)
	{
		$genre = Genre::paginate(10);
		$editgenre = Genre::findorfail($id);
		return view('admin/genre', compact('editgenre', 'genre'));
	}

	public function update_genre(Request $data, $id)
	{
		$genre = [
			'nama_genre' => $data->nama_genre,
			'slug_genre' => Str::slug($data->nama_genre)
		];

		Genre::whereid_genre($id)->update($genre);
		return redirect('admin/genre')->with('success', 'Genre berhasil diedit');
	}

	public function hapus_genre($id)
	{
		$genre = Genre::findorfail($id);
		$genre->delete();

		return redirect()->back()->with('success', 'Genre berhasil dihapus');
	}
}

?>