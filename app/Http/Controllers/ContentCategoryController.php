<?php

namespace App\Http\Controllers;

use App\_Content_Category;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ContentCategoryController extends Controller
{
    public function tampil_category()
    {
        $category = _Content_Category::paginate(10)->where('c_cat_parent', '=', NULL);
        $data_kategori = _Content_Category::all()->where('c_cat_parent', '=', NULL);
        return view('admin/content-category', compact('category', 'data_kategori'));
    }

    public function tambah_simpan_category(Request $data)
    {
        $status = $data->status;
        if ($status === 'on'):
            $status_kategori = '1';
        else:
            $status_kategori = '0';
        endif;

        $category = _Content_Category::create([
            'c_cat_name' => $data->c_cat_name,
            'c_cat_image' => $data->gambar,
            'c_cat_alias' => Str::slug($data->c_cat_name),
            'c_cat_desc' => $data->c_cat_desc,
            'c_cat_parent' => $data->c_cat_parent,
            'c_cat_status' => $status_kategori
        ]);

        return redirect('admin/content-category')->with('success', 'Kategori berhasil ditambahkan');
    }

    public function edit_category($id)
    {
        $category = _Content_Category::paginate(10)->where('c_cat_parent', '=', NULL);
        $data_kategori = _Content_Category::all()->where('c_cat_parent', '=', NULL);
        $editkategori = _Content_Category::findorfail($id);
        return view('admin/content-category', compact('category', 'data_kategori', 'editkategori'));
    }

    public function update_category(Request $data, $id)
    {
        $status = $data->status;
        if ($status === 'on'):
            $status_kategori = '1';
        else:
            $status_kategori = '0';
        endif;

        $category = [
            'c_cat_name' => $data->c_cat_name,
            'c_cat_alias' => Str::slug($data->c_cat_name),
            'c_cat_desc' => $data->c_cat_desc,
            'c_cat_parent' => $data->c_cat_parent,
            'c_cat_status' => $status_kategori
        ];

        _Content_Category::wherec_cat_id($id)->update($category);
        return redirect('admin/content-category')->with('success', 'Kategori Konten berhasil diupdate');
    }

    public function update_gambar(Request $data, $id)
    {
        $category = [
            'c_cat_image' => $data->gambar
        ];

        _Content_Category::wherec_cat_id($id)->update($category);
        return redirect()->back()->with('success', 'Gambar Kategori berhasil diedit');
    }

    public function hapus_category($id)
    {
        $category = _Content_Category::findorfail($id);
        $category->delete();

        return redirect()->back()->with('success', 'Kategori Konten berhasil dihapus');
    }
}
