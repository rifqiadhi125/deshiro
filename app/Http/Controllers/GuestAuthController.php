<?php 

namespace App\Http\Controllers;

Use App\Guest;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Session;

class GuestAuthController extends Controller
{
	use AuthenticatesUsers;
	protected $redirectTo = '/customer/home';

	public function __construct()
	{
	    $this->middleware('guest:guest')->except('logout')->except('index');
	}

	public function loginform()
	{
		return view('login');
	}

	public function login(Request $data)
	{
		$data_login = $data->only('email', 'password');

	    if (Auth::attempt($data_login))
	    {
	        return redirect('/user/dashboard');
	    }
	    return redirect('/login')->with('error', 'Username atau Password Salah!');
	}

	public function logout(Request $data)
	{
	    if(Auth::check())
	    {
	        Auth::logout();
	        $data->session()->invalidate();
	    }
	    return  redirect('/admin/login');
	}

	public function username()
	{
	    return 'email';
	}

	protected function guard()
	{
	      return Auth::guard('customer');
	}

}
?>