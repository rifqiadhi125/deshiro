<?php

namespace App\Http\Controllers;

use App\Content;
use Illuminate\Http\Request;

class TambahController extends Controller
{
    public function tambah()
    {
        $Tambah = Content::paginate(10);
        return view('admin/content/Tambah', compact('Tambah'));
    }
}
