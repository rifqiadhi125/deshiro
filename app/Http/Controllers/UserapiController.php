<?php

namespace App\Http\Controllers;
use App\Content;
use App\Portfolio;
use App\UserapiModel;
use Illuminate\Support\Facades\Validator;
use App\BukuModel;
use http\Client\Response;
use Illuminate\Http\Request;use Illuminate\Support\Facades\App;

class UserapiController extends Controller
{
    public function api()
    {
        $api = Portfolio::orderBy('portfolio_create_date','desc')->take(10)->get();

        return response()->json($api);
    }

    public function store(Request $request)
    {
        $validator = Validator::make($request->all(),
            [
                'portfolio_title' => 'required',
                'portfolio_sortdesc' => 'required',
                'portfolio_desc' => 'required',
                'portfolio_image' => 'required',
                'portfolio_status' => 'required',
            ],
            [
                'portfolio_title.required' => 'masuk_title',
                'portfolio_sortdesc.required' => 'masuk_sortdesc',
                'portfolio_desc' => ' masuk_desc',
                'portfolio_image' => 'masuk_image',
                'portfolio_status' => 'masuk_status',
            ]);


        if ($validator->fails()) {
            return response()->json([
                'success' => false,
                'message' => 'silahkan isi bidang yang kosong',
                'portfolio_sortdesc' => $validator->errors()
            ]);
        } else {
            $post = UserapiModel::create([
                'portfolio_title' => $request->input('portfolio_title'),
                'portfolio_sortdesc' => $request->input('portfolio_sortdesc'),
                'portfolio_desc' => $request->input('portfolio_desc'),
                'portfolio_image' => $request->input('portfolio_image'),
                'portfolio_status' => $request->input('portfolio_status'),
                'portfolio_create_date' => date('Y-m-d H:i:s')
            ]);
            if ($post) {
                return response()->json([
                    'susccess' => true,
                    'message' => 'Post Berhasil Disimpan',
                ]);
            } else {
                return response()->json([
                    'success' => false,
                    'message' => 'Post Gagal Disimpan'
                ]);
            }
        }
    }

    public function hapus(Request $request)
    {
        $post = Content::where('content_id', $request->input('id'))->delete();

        if ($post) {
            return response()->json([
                'susccess' => true,
                'message' => 'Post Berhasil Dihapus'
            ]);
        } else {
            return response()->json([
                'susccess' => true,
                'massage' => 'Gagal Berhasil Dihapus'
            ]);
        }
    }

    public function update(Request $request)
    {
        $contend_id = [
            'content_id' => $request -> content_id ,
            'content_name' => $request -> content_name,
            'content_type' => $request -> content_type,
            'content_alias' => $request -> content_alias,
            'content_sortdesc' => $request ->content_sortdesc,
        ];

       $spot =Content::where('content_id', $request->input('content_id'))->update($contend_id);

       if ($spot){
            return response()->json([
                'susccess' => true,
                'message' => 'Post Berhasil Diupdate'
            ]);
       }
        else {
            return response()->json([
                'susccess' => true,
                'massage' => 'Gagal Diudpate'
            ]);
        }

    }
    public function tambah(Request $request)
    {
       $tambah = new UserapiModel ;

       $tambah-> portfolio_title = $request ->portfolio_title;
        $tambah-> portfolio_sortdesc = $request ->portfolio_sortdesc;
        $tambah-> portfolio_desc = $request ->portfolio_desc;
        $tambah -> portfolio_image = $request ->portfolio_image;
        $tambah -> portfolio_status = $request ->portfolio_status;

        $tambah->save();
    }
}
