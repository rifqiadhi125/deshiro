<?php

namespace App\Http\Controllers;

use App\BukuModel;
use Illuminate\Http\Request;
use function Sodium\compare;

class BukuController extends Controller
{
        public function tampil_buku()
    {
        $dataBuku = BukuModel::paginate(10);
        return view('admin/buku/index', compact('dataBuku'));
    }
        public function read($id)
    {
        $dataBuku = BukuModel ::where( 'guest_id', $id)->first();
        return view('admin/buku/read', compact('dataBuku'));

    }

}
