<?php

namespace App\Http\Controllers;

use App\_Service;
use Illuminate\Http\Request;

class NewTabController extends Controller
{
 public function service()
 {
     $NewTab = _Service::paginate(10);
     return view('admin/NewTab', compact('NewTab'));
 }

}
