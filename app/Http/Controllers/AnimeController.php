<?php 

namespace App\Http\Controllers;

use App\Anime;
use App\Genre;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;

class AnimeController extends Controller
{
	public function dashboard(Anime $anime, Genre $genre)
	{
		$jml_anime = $anime->count();
		$jml_genre = $genre->count();
		return view('admin/dashboard', compact('jml_anime', 'jml_genre'));
	}

	public function tampil_anime()
	{
		$anime = Anime::paginate(10);
		return view('admin/anime', compact('anime'));
	}

	public function tambah_anime()
	{
		$genre = Genre::all();
		return view('admin/anime/tambah', compact('genre'));
	}

	public function tambah_simpan_anime(Request $data, Anime $anime)
	{
		$thumbnail = $data->thumbnail_anime;
		$new_thumbnail = time().$thumbnail->getClientOriginalName();

		$anime = Anime::create([
			'id_user' => Auth::user()->id,
			'thumbnail_anime' => $new_thumbnail,
			'judul_anime' => $data->judul_anime,
			'judul_alternatif_anime' => $data->judul_alternatif_anime,
			'slug_anime' => Str::slug($data->judul_anime),
			'rating_anime' => $data->rating_anime,
			'sinopsis_anime' => $data->sinopsis_anime
		]);

		$anime->Genre()->attach($data->genre);

		$thumbnail->move('public/uploads/thumbnail/', $new_thumbnail);

		$last_anime = $anime->orderBy('id_anime', 'desc')->first();
		$now_id_anime = $last_anime->id_anime;

		return redirect('admin/anime/edit/'.$now_id_anime)->with('success', 'Anime berhasil ditambahkan');
	}

	public function edit_anime($id)
	{
		$anime = Anime::findorfail($id);
		$genre = Genre::all();
		return view('admin/anime/edit', compact('anime', 'genre'));
	}

	public function update_anime(Request $data, $id)
	{
		$anime_data = Anime::findorfail($id);

		if ($data->has('thumbnail_anime')) {
			$thumbnail = $data->thumbnail_anime;
			$new_thumbnail = time().$thumbnail->getClientOriginalName();
			$thumbnail->move('public/uploads/thumbnail/', $new_thumbnail);

			$anime = [
				'thumbnail_anime' => $new_thumbnail,
				'judul_anime' => $data->judul_anime,
				'judul_alternatif_anime' => $data->judul_alternatif_anime,
				'slug_anime' => Str::slug($data->judul_anime),
				'sinopsis_anime' => $data->sinopsis_anime
			];

			File::delete('public/uploads/thumbnail/'.$anime_data->thumbnail_anime);
		}
		else{
			$anime = [
				'judul_anime' => $data->judul_anime,
				'judul_alternatif_anime' => $data->judul_alternatif_anime,
				'slug_anime' => Str::slug($data->judul_anime),
				'sinopsis_anime' => $data->sinopsis_anime
			];
		}

		$anime_data->Genre()->sync($data->genre);
		$anime_data->update($anime);

		return redirect('admin/anime')->with('success', 'Anime berhasil diupdate');
	}

	public function hapus_anime($id)
	{
		$anime = Anime::findorfail($id);
		File::delete('public/uploads/thumbnail/'.$anime->thumbnail_anime);
		$anime->delete();

		return redirect()->back()->with('success', 'Anime berhasil dihapus');
	}
}

?>