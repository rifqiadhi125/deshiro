<?php 

namespace App\Http\Controllers;

use App\Tag;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;

class TagController extends Controller
{
	public function tampil_tag()
	{
		$tag = Tag::paginate(10);
		return view('admin/tag', compact('tag'));
	}

	public function tambah_tag()
	{
		return view('admin/tag/tambah');
	}

	public function tambah_simpan_tag(Request $data)
	{
		$tag = Tag::create([
			'nama_tag' => $data->nama_tag,
			'slug_tag' => Str::slug($data->nama_tag)
		]);

		return redirect()->back()->with('success', 'Tag berhasil ditambahkan');
	}

	public function edit_tag($id)
	{
		$tag = Tag::paginate(10);
		$edittag = Tag::findorfail($id);
		return view('admin/tag', compact('edittag', 'tag'));
	}

	public function update_tag(Request $data, $id)
	{
		$tag = [
			'nama_tag' => $data->nama_tag,
			'slug_tag' => Str::slug($data->nama_tag)
		];

		Tag::whereid_tag($id)->update($tag);
		return redirect('admin/tag')->with('success', 'Tag berhasil diedit');
	}

	public function hapus_tag($id)
	{
		$tag = Tag::findorfail($id);
		$tag->delete();

		return redirect()->back()->with('success', 'Tag berhasil dihapus');
	}
}

?>