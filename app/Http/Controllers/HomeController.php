<?php

namespace App\Http\Controllers;

use App\_Content;
use App\_Setting;
use App\_Service;
use App\Portfolio;
use App\_Slider;
use App\Content;
use App\_Content_Category;
use App\User;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function tentangkami()
    {
        $about = _Setting::where('setting_name','about')->first();
        $dataprofilbelanja=Content::where('c_cat_id',2)->where('content_status','publish')-> get();

        return view('front/tentangkami/index',compact('about','dataprofilbelanja'));
    }
    public function home()
    {
        $about = _Setting::where('setting_name','about')->first();
        $barucontend =_Service::orderBy('service_create_date','desc') ->take(6)->get();
        $databaru=Portfolio::orderBy('portfolio_create_date','desc')->take(5)->get();
        $datablog=_Content::where('c_cat_id','!=','2')->orderBy('content_create_date','desc')->take(4)->get();
        $dataslider=_Slider::orderBy('slider_create_at','desc')->take(5)->get();
        return view('front/layout', compact('about','barucontend','databaru','datablog','dataslider'));
    }
    public function layanan()
    {

       $layanan =_Service::orderBy('service_create_date','desc')->take(5)->get();
       return view('front/layanan/layanan',compact('layanan'));
    }
   public function mitrakami()
  {
    $datamitrakami=Portfolio::orderBy('portfolio_create_date','desc')->get();
    return view('front/mitrakami/mitrakami',compact('datamitrakami'));
  }
    public function blog()
    {
      $contend =_Content::where('c_cat_id','!=','2')->orderBy('content_create_date','desc') -> paginate(10);
      $barucontend=_Content::where('c_cat_id','!=','2')->orderBy('content_create_date','desc') ->take(5)->get();
        return view('front/blog/blog',['contend' => $contend,'barucontend' =>$barucontend]);
    }
    public function detail($id)
    {
        $barudetail =_Content::findorfail($id);
        $detailbaru =_Content::where('c_cat_id','!=','2')->orderBy('content_create_date','desc')->take(5)->get();
        return view('front/blog/detail',compact('barudetail','detailbaru'));
    }
    public function kontakkami()
    {
        $kontak =_setting::where('setting_name','phone')->first();
        return view('front/kontakkami/kontakkami',compact('kontak'));
    }
}
