<?php

namespace App\Http\Controllers;

use App\_Slider;
use App\Slider;

use Illuminate\Http\Request;

class SliderController extends Controller
{
    public function tampil_slider()
	{
		$slider = _Slider::paginate(10);
		return view('admin/slider', compact('slider'));
	}

	public function tambah_slider()
	{
		return view('admin/slider/tambah-slider');
	}

	public function tambah_simpan_slider(Request $data)
	{
        $tanggal =date('Y-m-d H:i:s');
		$slider = _Slider::create([
			'slider_title' => $data->judul,
			'slider_image' => $data->gambar,
			'slider_type' => $data -> type,
			'slider_shortdesc' => $data->deskripsi,
			'slider_status' => $data->status,
            'slider_url' => $data ->url,
            'slider_create_at' => $tanggal,
		]);

		return redirect('admin/slider')->with('success', 'Slider berhasil ditambahkan');
	}

	public function edit_slider($id)
	{
		$slider = _Slider::findorfail($id);
		return view('admin/slider/edit-slider', compact('slider'));
	}

	public function update_slider(Request $data, $id)
	{
		$slider = [
			'slider_title' => $data->judul,
			'slider_shortdesc' => $data->deskripsi,
			'slider_status' => $data->status,
            'slider_create_at' => date('Y-m-d H:i:s'),
		];

        _Slider::findOrFail($id)->update($slider);
		return redirect('admin/slider')->with('success', 'Slider berhasil diupdate');
	}

	public function update_gambar(Request $data, $id)
	{
		$slider = [
			'slider_image' => $data->gambar
		];

        _Slider::findOrFail($id)->update($slider);
		return redirect()->back()->with('success', 'Gambar Slider berhasil diedit');
	}

	public function hapus_slider($id)
	{
		$slider = _Slider::findorfail($id);
		$slider->delete();

		return redirect()->back()->with('success', 'Slider berhasil dihapus');
	}

}
