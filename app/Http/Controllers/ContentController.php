<?php

namespace App\Http\Controllers;

use App\_Content;
use App\_Content_Category;
use App\User;
use Illuminate\Http\Request;
use Illuminate\Support\Str;

class ContentController extends Controller
{
    public function tampil_content()
    {
        $content = _Content::orderBy('content_create_date','desc')-> paginate(10);
        return view('admin/content', compact('content'));
    }

    public function tambah_content()
    {
        $user = User::all();
        $kategori = _Content_Category::all();
        return view('admin/content/tambah-content', compact('user', 'kategori'));
    }

    public function tambah_simpan_content(Request $data)
    {
        $status = $data->content_status;
        if ($status === 'on'):
            $status_content = 'publish';
        else:
            $status_content = 'draft';
        endif;

        date_default_timezone_set('Asia/Jakarta');
        $content = _Content::create([
            'c_cat_id' => $data->c_cat_id,
            'user_id' => $data->user_id,
            'content_name' => $data->content_name,
            'content_type' => $data->type,
            'content_image' => $data->gambar,
            'content_alias' => Str::slug($data->content_name),
            'content_sortdesc' => $data->content_sortdesc,
            'content_desc' => $data->content_desc,
            'content_tags' => $data->content_tags,
            'content_status' => $status_content,
            'content_create_date' => date('Y-m-d H:i:s')
        ]);

        return redirect('admin/content')->with('success', 'Konten berhasil ditambahkan');
    }

    public function edit_content($id)
    {
        $content = _Content::findorfail($id);
        $user = User::all();
        $kategori = _Content_Category::all();
        return view('admin/content/edit-content', compact('content', 'user', 'kategori'));
    }

    public function update_content(Request $data, $id)
    {
        $status = $data->content_status;
        if ($status === 'on'):
            $status_content = 'publish';
        else:
            $status_content = 'draft';
        endif;

        $content = [
            'c_cat_id' => $data->c_cat_id,
            'user_id' => $data->user_id,
            'content_name' => $data->content_name,
            'content_type' => $data->type,
            'content_alias' => Str::slug($data->content_name),
            'content_sortdesc' => $data->content_sortdesc,
            'content_desc' => $data->content_desc,
            'content_tags' => $data->content_tags,
            'content_status' => $status_content,
            'content_create_date' => date('Y-m-d H:i:s')
        ];

        _Content::wherecontent_id($id)->update($content);
        return redirect('admin/content')->with('success', 'Content berhasil diupdate');
    }

    public function update_gambar(Request $data, $id)
    {
        $content = [
            'content_image' => $data->gambar
        ];

        _Content::wherecontent_id($id)->update($content);
        return redirect()->back()->with('success', 'Gambar Konten berhasil diedit');
    }

    public function hapus_content($id)
    {
        $content = _Content::findorfail($id);
        $content->delete();

        return redirect()->back()->with('success', 'Content berhasil dihapus');
    }
}
