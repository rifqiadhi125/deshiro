<?php

namespace App\Http\Controllers;

use App\Customer;
use Illuminate\Http\Request;
use App\Exports\CustomerExport;
use App\Imports\CustomerImport;
use Maatwebsite\Excel\Facades\Excel;

class CustomerController extends Controller
{
    public function tampil_customer()
    {
        $customer = Customer::paginate(10);
		return view('admin/customer', compact('customer'));
    }

    public function tambah_customer()
	{
		return view('admin/customer/tambah-customer');
    }

    public function tambah_simpan_customer(Request $data)
	{
		$customer = Customer::create([
			'nama' => $data->nama,
            'gender' => $data->gender,
            'alamat' => $data->alamat
		]);

		return redirect('admin/customer')->with('success', 'Customer berhasil ditambahkan');
    }
    
    public function edit_customer($id)
    {
        $customer = Customer::findorfail($id);
        return view('admin/customer/edit-customer', compact('customer'));
    }

    public function update_customer(Request $data, $id)
	{
		$customer = [
			'nama' => $data->nama,
            'gender' => $data->gender,
            'alamat' => $data->alamat
		];

		Customer::whereid($id)->update($customer);
		return redirect('admin/customer')->with('success', 'Customer berhasil diupdate');
    }
    
    public function hapus_customer($id)
	{
		$customer = Customer::findorfail($id);
		$customer->delete();

		return redirect()->back()->with('success', 'Customer berhasil dihapus');
    }
    
    public function import_customer() 
    {
        Excel::import(new CustomerImport,request()->file('file'));
           
        return back()->with('success', 'Customer berhasil diexport');
    }

    public function export_customer() 
    {
        $session_export = "yes";
        return Excel::download(new CustomerExport, 'customer.xlsx');
        // return redirect()->with('success', 'Customer berhasil diexport');
    }
}
