<?php 

namespace App\Http\Controllers;

use App\Slider;
use Auth;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;

class ApiController extends Controller
{
	public function api()
	{
		$slider = Slider::where('slider_id', 1);
        return response()->json($slider);
	}

}
