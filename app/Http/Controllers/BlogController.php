<?php

namespace App\Http\Controllers;

use App\Anime;
use App\Anime_episode;
use App\Episode;
use App\Genre;
use Auth;
use Illuminate\Http\Request;
use Illuminate\Support\Str;
use Illuminate\Routing\Redirector;

class BlogController extends Controller
{

	public function homepage(Anime $anime)
	{
		$data = $anime->orderBy('created_at', 'desc')->get();
		return view('front/layout');
	}

	public function animepage($slug)
	{
		$data = Anime::where('slug_anime', $slug)->get();
		return view('animepage', compact('data'));
	}

	public function episodepage($slug)
	{
		$episode = Episode::where('slug_episode', $slug)->first();
		$episode_anime = Anime_episode::where('episode_id_episode', $episode->id_episode)->first();
		$anime = Anime::where('id_anime', $episode_anime->anime_id_anime)->first();

		return view('episodepage', compact('episode', 'anime'));
	}

	public function halamansemuagenre(Genre $genre)
	{
		$data = $genre->get();
		return view('allgenrepage', compact('data'));
	}
}

?>
