<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Tag extends Model
{
	protected $fillable = ['nama_tag', 'slug_tag'];
	protected $table = 'tag';
	protected $primaryKey = 'id_tag';
}

?>