<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class BukuModel extends Model
{
    protected $fillable = ['guest_id  ', 'guest_name', 'guest_email', 'guest_subject','guest_message','guest_created_date'];
    protected $table = '_guest';
    protected $primaryKey = 'guest_id';
    public $timestamps = false;
}
