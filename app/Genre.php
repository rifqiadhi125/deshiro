<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Genre extends Model
{
	protected $fillable = ['nama_genre', 'slug_genre'];
	protected $table = 'genre';
	protected $primaryKey = 'id_genre';

	public function anime()
	{
		return $this->belongsToMany('App\Anime');
	}
}

?>