<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Slider extends Model
{
    protected $fillable = ['judul', 'gambar', 'deskripsi', 'status'];
	protected $table = 'slider';
	protected $primaryKey = 'id';
}
