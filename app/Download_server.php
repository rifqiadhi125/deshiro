<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Download_server extends Model
{
	protected $fillable = ['nama_server'];
	protected $table = 'download_server';
	protected $primaryKey = 'id_server';

	public function anime()
	{
		return $this->belongsTo('App\Anime');
	}
}

?>