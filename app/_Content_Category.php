<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _Content_Category extends Model
{
    protected $guarded = [];
    protected $table = '_content_category';
    protected $primaryKey = 'c_cat_id';
}
