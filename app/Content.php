<?php

namespace App;

use Illuminate\Database\Eloquent\Model;
use App\Content;

class Content extends Model
{
    protected $fillable = ['c_cat_id', 'content_title', 'content_alias_','content_image','content_shortdesc','content_desc','content_status'];
    protected $table = '_content';
    protected $primaryKey = 'content_id';
}