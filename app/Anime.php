<?php 

namespace App;

use Illuminate\Database\Eloquent\Model;

class Anime extends Model
{
	protected $fillable = ['id_user','thumbnail_anime', 'judul_anime', 'judul_alternatif_anime', 'slug_anime', 'rating_anime', 'sinopsis_anime'];
	protected $table = 'anime';
	protected $primaryKey = 'id_anime';

	public function genre()
	{
		return $this->belongsToMany('App\Genre');
	}

	public function episode()
	{
		return $this->belongsToMany('App\Episode');
	}

	public function users()
	{
		return $this->belongsTo('App\User', 'id_user', 'id');
	}
}

?>