<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class _Content extends Model
{
    protected $guarded = [];
    protected $table = '_content';
    protected $primaryKey = 'content_id';

    public function users()
    {
        return $this->belongsTo('App\User', 'user_id', 'id');
    }

    public function kategori()
    {
        return $this->belongsTo('App\_Content_Category', 'c_cat_id', 'c_cat_id');
    }
}
