@include('template.header')
<style>
.streaming-frame {
  display: block;
  text-align: center;
  margin: auto;
  height: 500px;
  width: 90%;
}

@media only screen and (max-width: 600px) {
  .streaming-frame {
  display: block;
  text-align: center;
  margin: auto;
  height: 200px;
  width: 100%;
}
}
</style>
  <div class="uk-container uk-container-small">
    <div class="uk-grid-large" data-uk-grid>
      <div class="uk-width-expand@m">
        <div class="uk-article">
          <h2>{{ $anime->judul_anime }}</h2>
          <h5 style="margin-top: 5px">{{ $anime->judul_anime }}&nbsp;{{ str_replace('$anime->judul_anime', '', $episode->nama_episode) }}</h5>
          @if(!$episode->streaming_link)
          <h4 style="text-align: center;">Video Streaming Tidak Tersedia</h4>
          @else
          <iframe class="streaming-frame" src="{{ $episode->streaming_link }}"></iframe>
          @endif
        </div>
      </div>
    </div>
  </div>



<div class="uk-section uk-section-default">
  <div class="uk-container uk-container-small">
    <div class="uk-grid-large" data-uk-grid>
      <div class="uk-width-expand@m">
        <div class="uk-article">
          <h3>Download Links</h3>
          <div id="step-1" class="uk-grid-small uk-margin-medium-top" data-uk-grid>
            <div class="uk-width-auto">
              <a href="javascript:0" class="uk-step-icon" data-uk-icon="icon: play; ratio: 0.9"></a>
            </div>
            <div class="uk-width-expand">
              <a href="javascript:0"><h5 class="uk-step-title uk-text-500 uk-text-uppercase uk-text-primary" data-uk-leader="fill:—">{{ $episode->nama_episode }}</h5></a>
              @if($episode->v240p == '1')
              <div class="uk-step-content">
                <b>240p</b>
                @if(!empty($episode->link1_240p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link1_240p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link1_240p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link2_240p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link2_240p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link2_240p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link3_240p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link3_240p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link3_240p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link4_240p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link4_240p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link4_240p }}</span></a>
                @else
                @endif 
              </div>
              @else
              @endif
              @if($episode->v360p == '1')
              <br>
              <div class="uk-step-content">
                <b>360p</b>
                @if(!empty($episode->link1_360p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link1_360p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link1_360p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link2_360p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link2_360p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link2_360p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link3_360p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link3_360p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link3_360p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link4_360p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link4_360p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link4_360p }}</span></a>
                @else
                @endif
              </div>
              @else
              @endif
              @if($episode->v480p == '1')
              <br>
              <div class="uk-step-content">
                <b>480p</b>
                @if(!empty($episode->link1_480p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link1_480p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link1_480p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link2_480p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link2_480p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link2_480p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link3_480p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link3_480p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link3_480p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link4_480p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link4_480p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link4_480p }}</span></a>
                @else
                @endif
              </div>
              @else
              @endif
              @if($episode->v720p == '1')
              <br>
              <div class="uk-step-content">
                <b>720p</b>
                @if(!empty($episode->link1_720p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link1_720p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link1_720p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link2_720p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link2_720p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link2_720p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link3_720p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link3_720p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link3_720p }}</span></a>
                @else
                @endif
                @if(!empty($episode->link4_720p))
                <a class="uk-display-inline-block" href="//{{ str_replace('https:', '', str_replace('http:', '', $episode->link4_720p)) }}"><span class="uk-label uk-label-light">{{ $episode->nama_link4_720p }}</span></a>
                @else
                @endif
              </div>
              @else
              @endif
            </div>
          </div>
          <hr class="uk-margin-medium-top uk-margin-large-bottom">
          <h3>Comments</h3>
          <ul class="uk-comment-list uk-margin-medium-top">
            <li>
              <article class="uk-comment uk-visible-toggle" tabindex="-1">
                <header class="uk-comment-header uk-position-relative">
                  <div class="uk-grid-medium uk-flex-middle" data-uk-grid>
                    <div class="uk-width-auto">
                      <img class="uk-comment-avatar uk-border-circle" src="https://via.placeholder.com/100x100" width="50" height="50" alt="Alice Thomson">
                    </div>
                    <div class="uk-width-expand">
                      <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Alice Thomson</a></h4>
                      <p class="uk-comment-meta uk-margin-remove"><a class="uk-link-reset" href="#">12 days ago</a></p>
                      <div class="uk-rating">
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                        <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                        <span data-uk-icon="icon: star; ratio: 0.8"></span>
                      </div>
                    </div>
                  </div>
                  <div class="uk-position-top-right uk-position-small uk-hidden-hover"><a class="uk-link-muted" href="#">Reply</a>
                  </div>
                </header>
                <div class="uk-comment-body">
                  <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore et
                    dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum. Stet
                    clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                </div>
              </article>
              <ul>
                <li>
                  <article class="uk-comment uk-comment-primary uk-visible-toggle uk-border-rounded" tabindex="-1">
                    <header class="uk-comment-header uk-position-relative">
                      <div class="uk-grid-medium uk-flex-middle" data-uk-grid>
                        <div class="uk-width-auto">
                          <img class="uk-comment-avatar uk-border-circle" src="https://via.placeholder.com/100x100" width="50" height="50" alt="Tom Solender">
                        </div>
                        <div class="uk-width-expand">
                          <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Tom Solender</a></h4>
                          <p class="uk-comment-meta uk-margin-remove-top"><a class="uk-link-reset" href="#">12 days ago</a></p>
                        </div>
                      </div>
                      <div class="uk-position-top-right uk-position-small uk-hidden-hover"><a class="uk-link-muted"
                          href="#">Reply</a></div>
                    </header>
                    <div class="uk-comment-body">
                      <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                        et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                    </div>
                  </article>
                </li>
                <li>
                  <article class="uk-comment uk-visible-toggle" tabindex="-1">
                    <header class="uk-comment-header uk-position-relative">
                      <div class="uk-grid-medium uk-flex-middle" data-uk-grid>
                        <div class="uk-width-auto">
                          <img class="uk-comment-avatar uk-border-circle" src="https://via.placeholder.com/100x100" width="50" height="50" alt="Alice Thomson">
                        </div>
                        <div class="uk-width-expand">
                          <h4 class="uk-comment-title uk-margin-remove"><a class="uk-link-reset" href="#">Alice Thomson</a></h4>
                          <p class="uk-comment-meta uk-margin-remove"><a class="uk-link-reset" href="#">12 days ago</a></p>
                          <div class="uk-rating">
                            <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                            <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                            <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                            <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.8"></span>
                            <span data-uk-icon="icon: star; ratio: 0.8"></span>
                          </div>
                        </div>
                      </div>
                      <div class="uk-position-top-right uk-position-small uk-hidden-hover"><a class="uk-link-muted"
                          href="#">Reply</a></div>
                    </header>
                    <div class="uk-comment-body">
                      <p>Lorem ipsum dolor sit amet, consetetur sadipscing elitr, sed diam nonumy eirmod tempor invidunt ut labore
                        et dolore magna aliquyam erat, sed diam voluptua. At vero eos et accusam et justo duo dolores et ea rebum.
                        Stet clita kasd gubergren, no sea takimata sanctus est Lorem ipsum dolor sit amet.</p>
                    </div>
                  </article>
                </li>
              </ul>
            </li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="uk-section uk-section-muted">
  <div class="uk-container">
    <h3>Other Recipes You May Like</h3>
    <div class="uk-child-width-1-2 uk-child-width-1-3@s uk-child-width-1-4@m uk-margin-medium-top" data-uk-grid>
      <div>
        <div
          class="uk-card">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-medium" src="https://via.placeholder.com/300x160" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
            <div class="uk-position-xsmall uk-position-top-right">
              <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                data-uk-icon="heart"></a>
            </div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Chef John's Turkey Sloppy Joes</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">5.0</span>
                <span>(73)</span>
              </div>
              <div class="uk-width-expand uk-text-right">by John Keller</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-medium" src="https://via.placeholder.com/300x160" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
            <div class="uk-position-xsmall uk-position-top-right">
              <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                data-uk-icon="heart"></a>
            </div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Brown Sugar Meatloaf</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">3.0</span>
                <span>(94)</span>
              </div>
              <div class="uk-width-expand uk-text-right">by Danial Caleem</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-medium" src="https://via.placeholder.com/300x160" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
            <div class="uk-position-xsmall uk-position-top-right">
              <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                data-uk-icon="heart"></a>
            </div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Awesome Slow Cooker Pot Roast</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">4.5</span>
                <span>(153)</span>
              </div>
              <div class="uk-width-expand uk-text-right">by Janet Small</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-medium" src="https://via.placeholder.com/300x160" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
            <div class="uk-position-xsmall uk-position-top-right">
              <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                data-uk-icon="heart"></a>
            </div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Broiled Tilapia Parmesan</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">5.0</span>
                <span>(524)</span>
              </div>
              <div class="uk-width-expand uk-text-right">by Aleaxa Dorchest</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-medium" src="https://via.placeholder.com/300x160" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
            <div class="uk-position-xsmall uk-position-top-right">
              <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                data-uk-icon="heart"></a>
            </div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Baked Teriyaki Chicken</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">4.6</span>
                <span>(404)</span>
              </div>
              <div class="uk-width-expand uk-text-right">by Ben Kaller</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-medium" src="https://via.placeholder.com/300x160" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
            <div class="uk-position-xsmall uk-position-top-right">
              <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                data-uk-icon="heart"></a>
            </div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Zesty Slow Cooker Chicken</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">3.9</span>
                <span>(629)</span>
              </div>
              <div class="uk-width-expand uk-text-right">by Sam Brown</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-medium" src="https://via.placeholder.com/300x160" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
            <div class="uk-position-xsmall uk-position-top-right">
              <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                data-uk-icon="heart"></a>
            </div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Rosemary Ranch Chicken Kabobs</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">3.6</span>
                <span>(149)</span>
              </div>
              <div class="uk-width-expand uk-text-right">by Theresa Samuel</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-medium" src="https://via.placeholder.com/300x160" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
            <div class="uk-position-xsmall uk-position-top-right">
              <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                data-uk-icon="heart"></a>
            </div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Slow Cooker Pulled Pork</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">2.9</span>
                <span>(309)</span>
              </div>
              <div class="uk-width-expand uk-text-right">by Adam Brown</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      
    </div>
  </div>
</div>
@include('template.footer')