@include('template.header')

<div class="uk-section uk-section-default">
  <div class="uk-container">
    <div data-uk-grid>
      <div class="uk-width-expand@m">
        <h2>Genre Anime</h2>
        <style>
        .img-post {
          max-height: 150px;
          height: 100%;
          width: auto;
          object-fit: cover;
        }

        @media only screen and (max-width: 600px) {
          .img-post {
            max-height: 250px;
            height: 100%;
            object-fit: cover;
          }
        }
        </style>
        <div class="uk-child-width-1-2 uk-child-width-1-2@s uk-child-width-1-4@l" data-uk-grid>
          @foreach($data as $genres => $genre)
            <div class="uk-card">
              <div class="uk-card-media-top uk-inline uk-light">
                <img class="uk-border-rounded-medium img-post"
                src="{{ asset('public/uploads/thumbnail_genre/'.$genre->thumbnail_genre) }}" style="">
                <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
              </div>
              <div>
                <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">{{ $genre->nama_genre }}</h3>
              </div>
              <a href="{{ url('genre/'.$genre->slug_genre)}}" class="uk-position-cover"></a>
            </div>
            @endforeach
          </div>
        <div class="uk-margin-large-top uk-text-small">
          <ul class="uk-pagination uk-flex-center uk-text-500 uk-margin-remove" data-uk-margin>
            <li><a href="#"><span data-uk-pagination-previous></span></a></li>
            <li><a href="#">1</a></li>
            <li class="uk-active"><span>2</span></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#"><span data-uk-pagination-next></span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="uk-section uk-section-default">
  <div class="uk-container">
    <div data-uk-grid>
      <div class="uk-width-expand">
        <h2>Videos</h2>          
      </div>
      <div class="uk-width-1-3 uk-text-right uk-light">
        <select class="uk-select uk-select-light uk-width-auto uk-border-pill uk-select-primary">
          <option>Featured</option>
          <option>Top Rated</option>
          <option>Trending</option>
        </select>
      </div>
    </div>      
    <div class="uk-child-width-1-2 uk-child-width-1-5@s" data-uk-grid>
      <div>
        <div
          class="uk-card uk-card-video">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-large" src="https://via.placeholder.com/300x400" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-large"></div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Judul</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">5.0</span>
                <span>(73)</span>
              </div>
              <div class="uk-width-expand uk-text-right">Post by admin</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card uk-card-video">
          <div class="uk-inline uk-light">
            <img class="uk-border-rounded-large" src="https://via.placeholder.com/300x400" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-large"></div>
            <div class="uk-position-center">
              <span data-uk-icon="icon: play-circle; ratio: 3.4"></span>
            </div>
            <div class="uk-position-small uk-position-bottom-left">
              <h5 class="uk-margin-small-bottom">Business Presentation Course</h5>
              <div class="uk-text-xsmall">by Thomas Haller</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card uk-card-video">
          <div class="uk-inline uk-light">
            <img class="uk-border-rounded-large" src="https://via.placeholder.com/300x400" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-large"></div>
            <div class="uk-position-center">
              <span data-uk-icon="icon: play-circle; ratio: 3.4"></span>
            </div>
            <div class="uk-position-small uk-position-bottom-left">
              <h5 class="uk-margin-small-bottom">Business Presentation Course</h5>
              <div class="uk-text-xsmall">by Thomas Haller</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card uk-card-video">
          <div class="uk-inline uk-light">
            <img class="uk-border-rounded-large" src="https://via.placeholder.com/300x400" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-large"></div>
            <div class="uk-position-center">
              <span data-uk-icon="icon: play-circle; ratio: 3.4"></span>
            </div>
            <div class="uk-position-small uk-position-bottom-left">
              <h5 class="uk-margin-small-bottom">Business Presentation Course</h5>
              <div class="uk-text-xsmall">by Thomas Haller</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>      
    </div>
  </div>
</div>


@include('template.footer')