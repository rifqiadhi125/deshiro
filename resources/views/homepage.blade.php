@include('template.header')

<div class="uk-container">
  <div class="uk-border-rounded-large uk-background-top-center uk-background-cover 
    uk-background-norepeat uk-light uk-inline uk-overflow-hidden uk-width-1-1" 
    style="background-image: url({{ asset('public/assets/images/tatenoyuusha.png') }});">
    <div class="uk-position-cover uk-header-overlay"></div>
    <div class="uk-position-relative" data-uk-grid>
      <div class="uk-width-1-2@m uk-flex uk-flex-middle">
        <div class="uk-padding-large uk-padding-remove-right">
          <h1 class="uk-heading-small uk-margin-remove-top">Download Anime Ongoing & Batch</h1>
          <p class="uk-text-secondary">Dari yang nyesek, lucu, seru, sampe yang gore...</p>
          <a class="uk-text-secondary uk-text-600 uk-text-small hvr-forward" href="sign-up.html">Suprise Me!<span
            class="uk-margin-small-left" data-uk-icon="arrow-right"></span></a>
        </div>
      </div>
      <div class="uk-width-expand@m">
      </div>
    </div>
  </div>
</div>

<div class="uk-section uk-section-default">
  <div class="uk-container">
    <div data-uk-grid>
      <div class="uk-width-expand@m">
        <h2>Anime Terbaru</h2> 
        <div data-uk-grid style="margin-bottom: 25px">
          <div class="uk-width-expand@m">
            <form class="uk-search uk-search-default uk-width-1-1">
              <span data-uk-search-icon></span>
              <input class="uk-search-input uk-text-small uk-border-rounded uk-form-large" type="search" placeholder="Search Anime...">
            </form>          
          </div>
            
          <div class="uk-width-1-3@m uk-text-right@m uk-light">
            <select class="uk-select uk-select-light uk-width-auto uk-border-pill uk-select-primary">
              <option>Sort by: Latest</option>
              <option>Sort by: Top Rated</option>
            </select>
          </div>
        </div>   
        <style>
        .img-post {
          max-height: 300px;
          height: 100%;
          width: auto;
          object-fit: cover;
        }

        @media only screen and (max-width: 600px) {
          .img-post {
            max-height: 250px;
            height: 100%;
            object-fit: cover;
          }
        }
        </style>
        <div class="uk-child-width-1-2 uk-child-width-1-2@s uk-child-width-1-5@l" data-uk-grid>
          @foreach($data as $animes => $anime)
            <div class="uk-card">
              <div class="uk-card-media-top uk-inline uk-light">
                <img class="uk-border-rounded-medium img-post"
                src="{{ asset('public/uploads/thumbnail/'.$anime->thumbnail_anime) }}" style="">
                <div class="uk-position-cover uk-card-overlay uk-border-rounded-medium"></div>
                <div class="uk-position-xsmall uk-position-top-right">
                  <a href="#" class="uk-icon-button uk-like uk-position-z-index uk-position-relative"
                    data-uk-icon="heart"></a>
                </div>
              </div>
              <div>
                <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">{{ $anime->judul_anime }}</h3>
                <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
                  <div class="uk-width-auto uk-flex uk-flex-middle">
                    <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                    <span class="uk-margin-xsmall-left">{{ $anime->rating_anime }}</span>
                  </div>
                  <div class="uk-width-expand uk-text-right">{{ $anime->created_at->diffForHumans() }}</div>
                </div>
              </div>
              <a href="{{ url('anime/'.$anime->slug_anime)}}" class="uk-position-cover"></a>
            </div>
            @endforeach
          </div>
        <div class="uk-margin-large-top uk-text-small">
          <ul class="uk-pagination uk-flex-center uk-text-500 uk-margin-remove" data-uk-margin>
            <li><a href="#"><span data-uk-pagination-previous></span></a></li>
            <li><a href="#">1</a></li>
            <li class="uk-active"><span>2</span></li>
            <li><a href="#">3</a></li>
            <li><a href="#">4</a></li>
            <li><a href="#"><span data-uk-pagination-next></span></a></li>
          </ul>
        </div>
      </div>
    </div>
  </div>
</div>

<div class="uk-section uk-section-default">
  <div class="uk-container">
    <div data-uk-grid>
      <div class="uk-width-expand">
        <h2>Videos</h2>          
      </div>
      <div class="uk-width-1-3 uk-text-right uk-light">
        <select class="uk-select uk-select-light uk-width-auto uk-border-pill uk-select-primary">
          <option>Featured</option>
          <option>Top Rated</option>
          <option>Trending</option>
        </select>
      </div>
    </div>      
    <div class="uk-child-width-1-2 uk-child-width-1-5@s" data-uk-grid>
      <div>
        <div
          class="uk-card uk-card-video">
          <div class="uk-card-media-top uk-inline uk-light">
            <img class="uk-border-rounded-large" src="https://via.placeholder.com/300x400" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-large"></div>
          </div>
          <div>
            <h3 class="uk-card-title uk-text-500 uk-margin-small-bottom uk-margin-top">Judul</h3>
            <div class="uk-text-xsmall uk-text-muted" data-uk-grid>
              <div class="uk-width-auto uk-flex uk-flex-middle">
                <span class="uk-rating-filled" data-uk-icon="icon: star; ratio: 0.7"></span>
                <span class="uk-margin-xsmall-left">5.0</span>
                <span>(73)</span>
              </div>
              <div class="uk-width-expand uk-text-right">Post by admin</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card uk-card-video">
          <div class="uk-inline uk-light">
            <img class="uk-border-rounded-large" src="https://via.placeholder.com/300x400" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-large"></div>
            <div class="uk-position-center">
              <span data-uk-icon="icon: play-circle; ratio: 3.4"></span>
            </div>
            <div class="uk-position-small uk-position-bottom-left">
              <h5 class="uk-margin-small-bottom">Business Presentation Course</h5>
              <div class="uk-text-xsmall">by Thomas Haller</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card uk-card-video">
          <div class="uk-inline uk-light">
            <img class="uk-border-rounded-large" src="https://via.placeholder.com/300x400" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-large"></div>
            <div class="uk-position-center">
              <span data-uk-icon="icon: play-circle; ratio: 3.4"></span>
            </div>
            <div class="uk-position-small uk-position-bottom-left">
              <h5 class="uk-margin-small-bottom">Business Presentation Course</h5>
              <div class="uk-text-xsmall">by Thomas Haller</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>
      <div>
        <div
          class="uk-card uk-card-video">
          <div class="uk-inline uk-light">
            <img class="uk-border-rounded-large" src="https://via.placeholder.com/300x400" alt="Course Title">
            <div class="uk-position-cover uk-card-overlay uk-border-rounded-large"></div>
            <div class="uk-position-center">
              <span data-uk-icon="icon: play-circle; ratio: 3.4"></span>
            </div>
            <div class="uk-position-small uk-position-bottom-left">
              <h5 class="uk-margin-small-bottom">Business Presentation Course</h5>
              <div class="uk-text-xsmall">by Thomas Haller</div>
            </div>
          </div>
          <a href="recipe.html" class="uk-position-cover"></a>
        </div>
      </div>      
    </div>
  </div>
</div>


@include('template.footer')