@include('front.template.header')
@include('front.template.navbar')

<div class="site-wrapper-reveal">
    <!--===========  Projects wrapper Start =============-->
    <div class="projects-wrapper section-space--pb_70 section-space--pt_100 projects-masonary-wrapper">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center section-space--mb_40">
                        <h3 class="heading">Mitra kami<span class="text-color-primary"> Profil</span></h3>
                    </div>

                    {{--<div class="messonry-button text-center  section-space--mb_60">--}}
                        {{--<button data-filter="*" class="is-checked"><span class="filter-text">All</span><span class="filter-counter">6</span></button>--}}
                        {{--<button data-filter=".cat--1"><span class="filter-text">Cloud Services</span> <span class="filter-counter">1</span></button>--}}
                        {{--<button data-filter=".cat--2"><span class="filter-text">Cyber Security</span> <span class="filter-counter">3</span></button>--}}
                        {{--<button data-filter=".cat--3"><span class="filter-text">IT consultancy</span> <span class="filter-counter">1</span></button>--}}
                        {{--<button data-filter=".cat--4"><span class="filter-text">IT Security</span> <span class="filter-counter">1</span></button>--}}
                    {{--</div>--}}
                </div>
            </div>

            <div class="projects-case-wrap">
                <div class="row mesonry-list">
                    <!--<div class="resizer"></div>-->
                    @foreach($datamitrakami as $mitra)
                    <div class="col-lg-4 col-md-6 cat--2">
                        <!-- Projects Wrap Start -->
                        <a href="#" class="projects-wrap style-2">
                            <div class="projects-image-box">
                                <div class="projects-image">
                                    <img  class="img-fluid" src="{{ $mitra->portfolio_image }}" alt="">
                                </div>
                                <div class="content">
                                    <h6 class="heading">{{ $mitra->portfolio_title}}</h6>
                                    <div class="post-categories">{!!  $mitra->portfolio_sortdesc !!}</div>
                                </div>
                            </div>
                        </a>
                    </div>
                    @endforeach
                </div>
            </div>
        </div>
    </div>
    <!--===========  Projects wrapper End =============-->
    <!--========== Call to Action Area Start ============-->
    {{--<div class="cta-image-area_one section-space--ptb_80 cta-bg-image_one">--}}
        {{--<div class="container">--}}
            {{--<div class="row align-items-center">--}}
                {{--<div class="col-xl-8 col-lg-7">--}}
                    {{--<div class="cta-content md-text-center">--}}
                        {{--<h3 class="heading text-white">We run all kinds of IT services that vow your <span class="text-color-secondary"> success</span></h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xl-4 col-lg-5">--}}
                    {{--<div class="cta-button-group--one text-center">--}}
                        {{--<a href="#" class="btn btn--white btn-one"><span class="btn-icon mr-2"><i class="far fa-comment-alt-dots"></i></span> Let's talk</a>--}}
                        {{--<a href="#" class="btn btn--secondary  btn-two"><span class="btn-icon mr-2"><i class="far fa-info-circle"></i></span> Get info</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
</div>
<div class="projects-case-wrap">
    <div class="row mesonry-list">
    </div>
</div>

@include('front.template.footer')

