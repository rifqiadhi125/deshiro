@include('front.template.header')
@include('front.template.navbar')


<div class="site-wrapper-reveal">

    <div id="carouselExampleIndicators" class="carousel slide" data-ride="carousel">
        <ol class="carousel-indicators">
            @php $i=1 @endphp
            @foreach($dataslider as $slider)
                @php $nomor=$i++; @endphp
                <?php if($nomor==1){ $set = 'active'; }else{$set='';} ?>
            <li data-target="#carouselExampleIndicators" data-slide-to="{{ $loop->iteration }}" class="{{ $set }}"></li>
            @endforeach
        </ol>
        <div class="carousel-inner">
            @php $i=1 @endphp
            @foreach($dataslider as $slider)
                @php $nomor=$i++; @endphp
                <?php if($nomor==1){ $set = 'active'; }else{$set='';} ?>
                <div class="carousel-item {{ $set }}">
                    <img class="d-block w-100" src="{{ $slider->slider_image }}" alt="First slide">
                    <div class="carousel-caption d-none d-md-block">
                        @if($slider->slider_type === 'text')
                            <h5>{!! $slider->slider_title !!}</h5>
                             <div>{!!  $slider->slider_shortdesc !!}</div>
                        @else
                            <h5> <a href ="{{ $slider->slider_url }}">{{ $slider->slider_title  }}</a></h5>
                        @endif


                    </div>
                </div>
            @endforeach
        </div>
        <a class="carousel-control-prev" href="#carouselExampleIndicators" role="button" data-slide="prev">
            <span class="carousel-control-prev-icon" aria-hidden="true"></span>
            <span class="sr-only">Previous</span>
        </a>
        <a class="carousel-control-next" href="#carouselExampleIndicators" role="button" data-slide="next">
            <span class="carousel-control-next-icon" aria-hidden="true"></span>
            <span class="sr-only">Next</span>
        </a>
    </div>

    <!--====================  Accordion area ====================-->
    <div class="accordion-wrapper section-space--ptb_100">
        <div class="container-fluid">
            <div class="row align-items-center">
                <div class="col-lg-6">
                    <div class="faq-wrapper faq-custom-col">
                        <div class="section-title section-space--mb_40">
                            <h3 class="heading">Apa itu <span class="text-color-primary"> Aplikasitoko Whitelabel</span></h3>
                        </div>
                        <div id="accordion">
                                <div id="collapseOne" class="show" aria-labelledby="headingOne" data-parent="#accordion">
                                    <div class="card-body">
                                   {!!$about ->setting_value!!}
                                    </div>
                                </div>
                        </div>
                    </div>
                </div>

                <div class="col-lg-6">
                    <div class="rv-video-section">

                        <div class="ht-banner-01 ">
                            <img class="img-fluid border-radus-5 animation_images one wow fadeInDown" src="{{asset('public/frontend/assets/images/banners/home-processing-video-intro-slider-slide-01-image-02.jpg')}}" alt="">
                        </div>

                        <div class="ht-banner-02">
                            <img class="img-fluid border-radus-5 animation_images two wow fadeInDown" src="{{asset('public/frontend/assets/images/banners/home-processing-video-intro-slider-slide-01-image-03.jpg')}}" alt="">
                        </div>

                        <div class="main-video-box video-popup">
                            <a href="https://www.youtube.com/watch?v=9No-FiEInLA" class="video-link  mt-30">
                                <div class="single-popup-wrap">
                                    <img class="img-fluid border-radus-5" src="{{asset('public/frontend/assets/images/banners/home-processing-video-intro-slider-slide-01-image-01.jpg')}}" alt="">
                                    <div class="ht-popup-video video-button">
                                        <div class="video-mark">
                                            <div class="wave-pulse wave-pulse-1"></div>
                                            <div class="wave-pulse wave-pulse-2"></div>
                                        </div>
                                        <div class="video-button__two">
                                            <div class="video-play">
                                                <span class="video-play-icon"></span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </a>
                        </div>

                        <div class="ht-banner-03">
                            <img class="img-fluid border-radus-5 animation_images three wow fadeInDown" src="{{asset('public/frontend/assets/images/banners/home-processing-video-intro-slider-slide-01-image-04.jpg')}}" alt="">
                        </div>

                        <div class="ht-banner-04">
                            <img class="img-fluid border-radus-5 animation_images four wow fadeInDown" src="{{asset('public/frontend/assets/images/banners/home-processing-video-intro-slider-slide-01-image-05.jpg')}}" alt="">
                        </div>

                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="feature-images-wrapper bg-gray section-space--ptb_100">
        <div class="container">

            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center section-space--mb_10">
                        {{--<h6 class="section-sub-title mb-20">Our services</h6>--}}
                        <h3 class="heading">Layanan kami</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="feature-images__one">
                        <div class="row">
                            @foreach($barucontend as $about )
                                <div class="col-lg-4 col-md-6 wow move-up">
                                    <!-- ht-box-icon Start -->
                                    <div class="ht-box-images style-01">
                                        <div class="image-box-wrap">
                                            <div class="box-image">
                                                <img class="img-fulid" src="{{ $about->service_images }}" alt="">
                                            </div>
                                            <div class="content">
                                                <h5 class="heading">{!!   $about->service_title !!}</h5>
                                                <div class="text">{!! $about->service_desc !!}</div>
                                                <div class="circle-arrow">
                                                    <div class="middle-dot"></div>
                                                    <div class="middle-dot dot-2"></div>
                                                    <a href="#">
                                                        <i class="far fa-long-arrow-right"></i>
                                                    </a>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            @endforeach
                        </div>
                    </div>

                    {{--<div class="section-under-heading text-center section-space--mt_80">Challenges are just opportunities in disguise. <a href="#">Take the challenge!</a></div>--}}

                </div>
            </div>

        </div>
    </div>

    {{--<div class="fun-fact-wrapper section-space--ptb_100">--}}
        {{--<div class="container">--}}

            {{--<div class="row">--}}
                {{--<div class="col-lg-4 offset-lg-1">--}}
                    {{--<div class="modern-number-01 mr-5">--}}
                        {{--<h2 class="heading"><span class="mark-text">38</span>Years’ Experience in IT</h2>--}}
                        {{--<h3 class="heading">Learn more about our <span class="text-color-primary">Success Stories</span></h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-lg-7 col-md-8">--}}
                    {{--<div class="row">--}}
                        {{--<div class="col-md-6 col-sm-6 wow move-up">--}}
                            {{--<div class="fun-fact--three text-center">--}}
                                {{--<div class="fun-fact__count counter">1790</div>--}}
                                {{--<h6 class="fun-fact__text">Account numbers</h6>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 wow move-up">--}}
                            {{--<div class="fun-fact--three text-center">--}}
                                {{--<div class="fun-fact__count counter">32</div>--}}
                                {{--<h6 class="fun-fact__text">Finished projects</h6>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 wow move-up">--}}
                            {{--<div class="fun-fact--three text-center">--}}
                                {{--<div class="fun-fact__count counter">73</div>--}}
                                {{--<h6 class="fun-fact__text">Happy clients</h6>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                        {{--<div class="col-md-6 col-sm-6 wow move-up">--}}
                            {{--<div class="fun-fact--three text-center">--}}
                                {{--<div class="fun-fact__count counter">318</div>--}}
                                {{--<h6 class="fun-fact__text">Blog posts</h6>--}}
                            {{--</div>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}


        {{--</div>--}}
    {{--</div>--}}
    <!--=========== fun fact Wrapper End ==========-->

    {{--<div class="processing-computing-area bg-gray-3">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12">--}}
                    {{--<div class="computing-info-box text-center">--}}
                        {{--<h2>IT Security & Computing</h2>--}}
                        {{--<div class="section-under-heading text-center mt-30">Challenges are just opportunities in disguise. <a href="#">Take the challenge!</a></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    {{--<div class="feature-large-images-wrapper section-space--ptb_100">--}}
        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12">--}}

                    {{--<div class="section-title-wrap text-center section-space--mb_30">--}}
                        {{--<h6 class="section-sub-title mb-20">Optimal Technology Solutions</h6>--}}
                        {{--<h3 class="heading">Mitra Kami</h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
            {{----}}
            {{--<div class="row">--}}
                {{--<div class="col-12">--}}
                    {{--<div class="row small-mt__30">--}}
                        {{--@foreach($databaru as $about)--}}
                        {{--<div class="col-lg-3 col-md-6 mt-30">--}}
                            {{--<a href="#" class="box-large-image__two">--}}
                                {{--<div class="box-large-image__two__box">--}}
                                    {{--<div class="box-large-image__midea">--}}
                                        {{--<div class="single-gallery__thum bg-item-images bg-img" data-bg="{{ $about->portfolio_image }}"></div>--}}
                                    {{--</div>--}}
                                    {{--<div class="box-info">--}}
                                        {{--<h5 class="heading">{{ $about->portfolio_title }}</h5>--}}
                                        {{--<div class="box-more-info">--}}
                                            {{--<div class="text"> </div>--}}
                                            {{--<div class="btn">--}}
                                                {{--<i class="button-icon far fa-long-arrow-right"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                     {{--@endforeach--}}
                        {{--<div class="col-lg-6 col-md-6  mt-30">--}}
                            {{--<!-- Box large image warap Start -->--}}
                            {{--<a href="#" class="box-large-image__two">--}}
                                {{--<div class="box-large-image__two__box">--}}
                                    {{--<div class="box-large-image__midea">--}}
                                        {{--<div class="single-gallery__thum bg-item-images bg-img" data-bg="assets/images/box-image/home-processing-software-image-02.jpg"></div>--}}
                                    {{--</div>--}}

                                    {{--<div class="box-info">--}}
                                        {{--<h5 class="heading">IT Management</h5>--}}
                                        {{--<div class="box-more-info">--}}
                                            {{--<div class="text">It’s possible to simultaneously manage and transform key information from one server to another.</div>--}}
                                            {{--<div class="btn">--}}
                                                {{--<i class="button-icon far fa-long-arrow-right"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-3 col-md-6 mt-30">--}}
                            {{--<!-- Box large image warap Start -->--}}
                            {{--<a href="#" class="box-large-image__two">--}}
                                {{--<div class="box-large-image__two__box">--}}
                                    {{--<div class="box-large-image__midea">--}}
                                        {{--<div class="single-gallery__thum bg-item-images bg-img" data-bg="assets/images/box-image/home-processing-software-image-03.jpg"></div>--}}
                                    {{--</div>--}}

                                    {{--<div class="box-info">--}}
                                        {{--<h5 class="heading">Data Security</h5>--}}
                                        {{--<div class="box-more-info">--}}
                                            {{--<div class="text">Back up your database, store in a safe and secure place while still maintaining its accessibility.</div>--}}
                                            {{--<div class="btn">--}}
                                                {{--<i class="button-icon far fa-long-arrow-right"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-4 col-md-6 mt-30">--}}
                            {{--<a href="#" class="box-large-image__two">--}}
                                {{--<div class="box-large-image__two__box">--}}
                                    {{--<div class="box-large-image__midea">--}}
                                        {{--<div class="single-gallery__thum bg-item-images bg-img" data-bg="assets/images/box-image/home-processing-software-image-04.jpg"></div>--}}
                                    {{--</div>--}}

                                    {{--<div class="box-info">--}}
                                        {{--<h5 class="heading">Business Reform</h5>--}}
                                        {{--<div class="box-more-info">--}}
                                            {{--<div class="text">We provide the most responsive and functional IT design for companies and businesses worldwide.</div>--}}
                                            {{--<div class="btn">--}}
                                                {{--<i class="button-icon far fa-long-arrow-right"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-4 col-md-6 mt-30">--}}
                            {{--<a href="#" class="box-large-image__two">--}}
                                {{--<div class="box-large-image__two__box">--}}
                                    {{--<div class="box-large-image__midea">--}}
                                        {{--<div class="single-gallery__thum bg-item-images bg-img" data-bg="assets/images/box-image/mitech-home-infotechno-box-large-image_02-330x330.jpg"></div>--}}
                                    {{--</div>--}}

                                    {{--<div class="box-info">--}}
                                        {{--<h5 class="heading">Infrastructure Plan</h5>--}}
                                        {{--<div class="box-more-info">--}}
                                            {{--<div class="text">We provide the most responsive and functional IT design for companies and businesses worldwide.</div>--}}
                                            {{--<div class="btn">--}}
                                                {{--<i class="button-icon far fa-long-arrow-right"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}

                        {{--<div class="col-lg-4 col-md-6 mt-30">--}}
                            {{--<a href="#" class="box-large-image__two">--}}
                                {{--<div class="box-large-image__two__box">--}}
                                    {{--<div class="box-large-image__midea">--}}
                                        {{--<div class="single-gallery__thum bg-item-images bg-img" data-bg="assets/images/box-image/mitech-home-infotechno-box-large-image_04-330x330.jpg"></div>--}}
                                    {{--</div>--}}

                                    {{--<div class="box-info">--}}
                                        {{--<h5 class="heading">Firewall Advancement</h5>--}}
                                        {{--<div class="box-more-info">--}}
                                            {{--<div class="text">We provide the most responsive and functional IT design for companies and businesses worldwide.</div>--}}
                                            {{--<div class="btn">--}}
                                                {{--<i class="button-icon far fa-long-arrow-right"></i>--}}
                                            {{--</div>--}}
                                        {{--</div>--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                            {{--</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                    {{--<div class="col-lg-12">--}}
                        {{--<div class="feature-list-button-box mt-40 text-center">--}}
                            {{--<a href="#" class="ht-btn ht-btn-md">Learn more</a>--}}
                            {{--<a href="#" class="ht-btn ht-btn-md ht-btn--outline">Contact us</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}

                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}

    <div class="projects-wrapper projectinfotechno-bg section-space--ptb_100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center section-space--mb_40">
                        {{--<h6 class="section-sub-title mb-20">Case studies</h6>--}}
                        <h3 class="heading">Blog</h3>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="col-12">
                    <div class="projects-wrap swiper-container projects-slider__container">
                        <div class="swiper-wrapper">
                            @foreach($datablog as $about)
                            <div class="swiper-slide">
                                <a href="{{ url('blog/detail', $about->content_id) }}" class="projects-wrap style-01 wow move-up">
                                    <div class="projects-image-box">
                                        <div class="projects-image">
                                            <img class="img-fluid" src="{{ $about->content_image }}" alt="">
                                        </div>
                                        <div class="content">
                                            <h6 class="heading">{{ $about->content_name }}</h6>
                                            <div class="post-categories">{{$about->content_sortdesc }}</div>
                                            <div class="text">{{ date('d F y',strtotime($about['created_at'])) }}</div>
                                            <div class="box-projects-arrow">
                                                <span class="button-text ">Detail Blog</span>
                                                <i class="fa fa-long-arrow-right ml-1"></i>
                                            </div>
                                        </div>
                                    </div>
                                </a>
                            </div>
                            @endforeach
                        </div>
                        <div class="swiper-pagination swiper-pagination-project mt_20"></div>
                    </div>
                    {{--<div class="section-under-heading text-center section-space--mt_40">Challenges are just opportunities in disguise. <a href="#">Take the challenge!</a></div>--}}
                </div>
            </div>
        </div>
    </div>

    <div class="projects-wrapper projectinfotechno-bg section-space--ptb_100">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="section-title-wrap text-center section-space--mb_40">
                        {{--<h6 class="section-sub-title mb-20">Case studies</h6>--}}
                        <h3 class="heading">Mitra Kami</h3>
                    </div>
                </div>
            </div>


    <div class="brand-logo-slider-area section-space--ptb_50">
        <div class="container">
            <div class="row">
                <div class="col-lg-12">
                    <div class="brand-logo-slider__container-area">
                        <div class="swiper-container brand-logo-slider__container">
                            <div class="swiper-wrapper brand-logo-slider__one">
                            @foreach($databaru as $mitra)
                                <div class="swiper-slide brand-logo brand-logo--slider">
                                    <a href="#">
                                        <div class="brand-logo__image">
                                            <img src="{{ $mitra->portfolio_image }}" class="img-fluid" alt="">
                                        </div>
                                    </a>
                                </div>
                            @endforeach
                            </div>
                        </div>
                    </div>
                </div>
             </div>
              </div>
            </div>
        </div>
    </div>

    <div class="container-fluid ace-container pb-5 pt-5">
        <div class="row ace-tes-height align-items-center">

            <div class="col-md-12 col-xl-12 text-center">
                <div id="king-745785" class="wpb_row  feature_section101">
                    <div class="container">
                        <div class=" wpb_column vc_column_container wpb_column vc_column_container vc_col-sm-12 ">
                            <div class="wpb_wrapper">
                                <h5 class="caps"><strong>ORDER CEPAT VIA WHATSAPP</strong></h5>
                                <h5><strong>Klik Tombol WA di Bawah, dan Isikan Data Order yang Diminta</strong></h5><div class="clearfix margin_top2"></div>
                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p></p><center><a target="_blank" href="https://api.whatsapp.com/send?phone=6281239585855&amp;text=Hallo,%20Belanjatech%0A"><img style="max-width: 50%;" src="https://www.belanjatech.com/template/frontend/assets/images/wa.png" alt="Whatsapp Belanjatech"></a></center><p></p>

                                    </div>
                                </div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <p style="text-align: center;">Dengan order cepat via Whatsapp, Anda akan tinggal mengisi form yg telah tersedia pada pesan WA. Selanjutnya Anda akan di respon dengan ramah oleh customer service kami.</p>

                                    </div>
                                </div>

                                <div class="wpb_text_column wpb_content_element ">
                                    <div class="wpb_wrapper">
                                        <h6 style="text-align: center;">Whatsapp: <span style="color: #ff6600;">{{ $setting[8]['setting_value'] }}</span>&nbsp; Layanan Tiket dan <span style="color: #ff6600;"><a style="color: #ff6600;" href="#">Live Chat</a></span> Online 24 Jam.</h6>

                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div><!-- endc single testimonials box -->
        </div>
    </div>

    <!--====================  Conact us Section Start ====================-->
    {{--<div class="contact-us-section-wrappaer processing-contact-us-bg section-space--ptb_120">--}}
        {{--<div class="container">--}}
            {{--<div class="row align-items-center">--}}
                {{--<div class="col-lg-6 col-lg-6">--}}
                    {{--<div class="conact-us-wrap-one">--}}
                        {{--<h3 class="heading text-white">Obtaining further information by make a contact with our experienced IT staffs. </h3>--}}

                        {{--<div class="sub-heading text-white">We’re available for 8 hours a day!<br>Contact to require a detailed analysis and assessment of your plan.</div>--}}

                    {{--</div>--}}
                {{--</div>--}}

                {{--<div class="col-lg-6 col-lg-6">--}}
                    {{--<div class="contact-info-two text-center">--}}
                        {{--<div class="icon">--}}
                            {{--<span class="fal fa-phone"></span>--}}
                        {{--</div>--}}
                        {{--<h6 class="heading font-weight--reguler">Reach out now!</h6>--}}
                        {{--<h2 class="call-us"><a href="tel:190068668">1900 68668</a></h2>--}}
                        {{--<div class="contact-us-button mt-20">--}}
                            {{--<a href="#" class="btn btn--secondary">Contact us</a>--}}
                        {{--</div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    {{--<!--====================  Conact us Section End  ====================-->--}}
</div>

@include('front.template.footer')
