@include('front.template.header')
@include('front.template.navbar')


<!-- breadcrumb-area start -->
<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_box text-center">
                    <h2 class="breadcrumb-title">Blog Update</h2>
                    <!-- breadcrumb-list start -->
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                    </ul>
                    <!-- breadcrumb-list end -->
                </div>
            </div>
        </div>
    </div>
</div>
<!-- breadcrumb-area end -->

<div class="site-wrapper-reveal">
    <div class="blog-pages-wrapper section-space--ptb_100">
        <div class="container">
            <div class="row">
                <div class="col-lg-4 order-lg-1 order-2">
                    <div class="page-sidebar-content-wrapper page-sidebar-left  small-mt__40 tablet-mt__40">
                        <div class="sidebar-widget widget-blog-recent-post wow move-up">
                            <h4 class="sidebar-widget-title">Menampilkan Blog Yang Terbaru</h4>
                            @foreach($barucontend as $ctn)
                            <ul>
                                <li>
                                    <a href="{{ url('blog/detail', $ctn->content_id) }}">{{ $ctn->content_name }}</a>
                                </li>
                            </ul>
                                @endforeach
                        </div>

                        <div class="sidebar-widget widget-images wow move-up">
                        </div>
                        <div class="sidebar-widget widget-tag wow move-up">
                            <h4 class="sidebar-widget-title">Tags</h4>
                            <a href="#" class="ht-btn ht-btn-xs">business</a>
                            <a href="#" class="ht-btn ht-btn-xs">featured</a>
                            <a href="#" class="ht-btn ht-btn-xs">IT Security</a>
                            <a href="#" class="ht-btn ht-btn-xs">IT services</a>
                        </div>
                    </div>
                </div>
                <div class="col-lg-8 order-lg-2 order-1">
                    <div class="main-blog-wrap">

                        <div class="">

                            <div class="post-info lg-blog-post-info">
                                <div class="post-meta mt-20">
                                </div>

                            </div>
                        </div>
                        @foreach($contend as $ctn)
                        <div class="single-blog-item lg-blog-item border-bottom wow move-up">
                            <div class="post-feature blog-thumbnail">
                                <a href="{{ url('blog/detail', $ctn->content_id) }}">
                                    <img class="img-fluid" src="{{ $ctn->content_image }}" alt="Blog Images">
                                </a>
                            </div>

                            <div class="post-info lg-blog-post-info">

                                <h3 class="post-title">
                                    <a href="blog-post-layout-one.html">{{ $ctn->content_name }}</a>
                                </h3>

                                <div class="post-meta mt-20">
                                    {{--<div class="post-author">--}}
                                        {{--<a href="#">--}}
                                            {{--<img class="img-fluid avatar-96" src="{{asset('public/frontend/assets/images/team/admin.jpg')}}" alt=""> Harry Ferguson--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    <div class="post-date">
                                        <span class="far fa-calendar meta-icon"> {{ date('d F y',strtotime($ctn['created_at'])) }}</span>
                                    </div>
                                    {{--<div class="post-view">--}}
                                        {{--<span class="meta-icon far fa-eye"></span>--}}
                                        {{--346 views--}}
                                    {{--</div>--}}
                                </div>

                                <div class="post-excerpt mt-15">
                                    <p>{{ $ctn->content_sortdesc }}</p>
                                </div>

                                <div class="read-post-share-wrap  mt-20">
                                    <div class="row align-items-center">
                                        <div class="col-lg-6 col-md-6 col-sm-6">
                                            <div class="post-read-more">
                                                <a href="{{ url('blog/detail', $ctn->content_id) }}" class="ht-btn ht-btn-md">Selengkapnya </a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        @endforeach
                        <div class="ht-pagination mt-30 pagination justify-content-center">
                            <div class="col-md-12">
                                <div style="float: right">{{ $contend->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('front.template.footer')

