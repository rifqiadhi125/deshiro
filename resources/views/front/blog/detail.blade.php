    @include('front.template.header')
@include('front.template.navbar')



<div class="breadcrumb-area">
    <div class="container">
        <div class="row">
            <div class="col-12">
                <div class="breadcrumb_box text-center">
                    <h2 class="breadcrumb-title">Blog Details</h2>
                    <!-- breadcrumb-list start -->
                    <ul class="breadcrumb-list">
                        <li class="breadcrumb-item"><a href="index.html">Home</a></li>
                        <li class="breadcrumb-item active">Blog Details</li>
                    </ul>
                    <!-- breadcrumb-list end -->
                </div>
            </div>
        </div>
    </div>
</div>

<div class="site-wrapper-reveal">

    <div class="blog-pages-wrapper section-space--ptb_100">
        <div class="container">
            <div class="row">

                <div class="col-lg-4 order-lg-1 order-2">
                    <div class="page-sidebar-content-wrapper page-sidebar-left  small-mt__40 tablet-mt__40">
                        <div class="sidebar-widget widget-blog-recent-post wow move-up">
                            <h4 class="sidebar-widget-title">Detail Blog Terbaru</h4>
                            @foreach($detailbaru as $baru)
                            <ul>
                                <li>
                                    <a href="{{ url('blog/detail', $baru->content_id) }}">{{ $baru->content_name }}</a>
                                </li>
                            </ul>
                            @endforeach
                        </div>

                        <div class="sidebar-widget widget-tag wow move-up">
                            <h4 class="sidebar-widget-title">Tags</h4>
                            <a href="#" class="ht-btn ht-btn-xs">business</a>
                            <a href="#" class="ht-btn ht-btn-xs">featured</a>
                            <a href="#" class="ht-btn ht-btn-xs">IT Security</a>
                            <a href="#" class="ht-btn ht-btn-xs">IT services</a>
                        </div>

                    </div>
                </div>
                <div class="col-lg-8 order-lg-2 order-1">
                    <div class="main-blog-wrap">
                        <div class="single-blog-item">
                            <div class="post-feature blog-thumbnail wow move-up">
                                <img class="img-fluid"  src=" {{ $barudetail->content_image }}" alt="Blog Images">
                            </div>

                            <div class="post-info lg-blog-post-info  wow move-up">
                                {{--<div class="post-categories">--}}
                                    {{--<a href="#"> Success Story, Tips</a>--}}
                                {{--</div>--}}

                                <h3 class="post-title">{{ $barudetail->content_name }}</h3>

                                <div class="post-meta mt-20">
                                    {{--<div class="post-author">--}}
                                        {{--<a href="#">--}}
                                            {{--<img class="img-fluid avatar-96" src="assets/images/team/admin.jpg" alt=""> Harry Ferguson--}}
                                        {{--</a>--}}
                                    {{--</div>--}}
                                    <div class="post-date">
                                        <span class="far fa-calendar meta-icon">{{ date('d F y',strtotime($barudetail['created_at'])) }} </span>

                                    </div>
                                    {{--<div class="post-view">--}}
                                        {{--<span class="meta-icon far fa-eye"></span>--}}
                                        {{--346 views--}}
                                    {{--</div>--}}
                                    {{--<div class="post-comments">--}}
                                        {{--<span class="far fa-comment-alt meta-icon"></span>--}}
                                        {{--<a href="#" class="smooth-scroll-link">4 Comments</a>--}}
                                    {{--</div>--}}
                                </div>

                                <div class="post-excerpt mt-15">
                                    <p>{{ $barudetail->content_sortdesc }}</p>

                                    <p>{!! $barudetail->content_desc !!}</p>

                                    <div class="entry-post-share-wrap  border-bottom">
                                        <div class="row align-items-center">
                                            <div class="col-lg-6 col-md-6">
                                                <div class="entry-post-tags">
                                                    <div class="tagcloud-icon">
                                                        <i class="far fa-tags"></i>
                                                    </div>
                                                    <div class="tagcloud">
                                                        <a href="#"> business</a>, <a href="#">IT Security</a>, <a href="#">IT services</a>
                                                    </div>
                                                </div>
                                            </div>

                                            <div class="col-lg-6 col-md-6">
                                                <div id="entry-post-share" class="entry-post-share">
                                                    <div class="share-label">
                                                        Share this post
                                                    </div>
                                                    <div class="share-media">
                                                        <span class="share-icon far fa-share-alt"></span>

                                                        <div class="share-list">
                                                            <a class="hint--bounce hint--top hint--primary twitter" target="_blank" aria-label="Twitter" href="#">
                                                                <i class="fab fa-twitter"></i>
                                                            </a>
                                                            <a class="hint--bounce hint--top hint--primary facebook" target="_blank" aria-label="Facebook" href="#">
                                                                <i class="fab fa-facebook-f"></i>
                                                            </a>
                                                            <a class="hint--bounce hint--top hint--primary linkedin" target="_blank" aria-label="Linkedin" href="#">
                                                                <i class="fab fa-linkedin"></i>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>


@include('front.template.footer')