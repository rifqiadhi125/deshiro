@include('front.template.header')
@include('front.template.navbar')



<div class="about-banner-wrap banner-space about-us-bg">
    <div class="container">
        <div class="row">
            <div class="col-lg-8 ml-auto mr-auto">
                <div class="about-banner-content text-center">
                    <h1 class="mb-15 text-white">Tentang kami</h1>
                    <h5 class="font-weight--normal text-white">{!!$about ->setting_value!!}</h5>
                </div>
            </div>
        </div>
    </div>
</div>

    <div class="site-wrapper-reveal">

    <!-- Flexible image slider wrapper Start -->
    {{--<div class="flexible-image-slider-wrapper section-space--ptb_100 border-bottom">--}}

        {{--<div class="container">--}}
            {{--<div class="row">--}}
                {{--<div class="col-lg-12">--}}
                    {{--<div class="section-title-wrap text-center section-space--mb_60">--}}
                        {{--<h3> Alur dan Data Dana </h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

            {{--<div class="row">--}}
                {{--<div class="col-lg-12">--}}
                    {{--<div class="flexible-image-slider-wrap">--}}
                        {{--<div class="swiper-container single-flexible__container">--}}
                            {{--<div class="swiper-wrapper">--}}
                                {{--@foreach($dataprofilbelanja as $profilbelanja)--}}
                                {{--<div class="swiper-slide">--}}
                                    {{--<div class="single-flexible-slider">--}}
                                        {{--<img src="{{ $profilbelanja->content_image }}" class="img-fluid" alt="Slider Image 01">--}}
                                    {{--</div>--}}
                                {{--</div>--}}
                                {{--@endforeach--}}
                            {{--</div>--}}
                            {{--<div class="swiper-nav-button swiper-button-next"><i class="nav-button-icon"></i></div>--}}
                            {{--<div class="swiper-nav-button swiper-button-prev"><i class="nav-button-icon"></i></div>--}}
                        {{--</div>--}}
                        {{--<div class="swiper-pagination swiper-pagination-1 section-space--mt_50"></div>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}

        {{--</div>--}}
    {{--</div>--}}
    <!-- Flexible image slider wrapper End -->

        <div class="flexible-image-slider-wrapper section-space--ptb_120">
            <div class="container">
                <div class="row">
                    <div class="col-lg-12">
                        <div class="section-title-wrap text-center section-space--mb_60">
                            <h3>Saatnya Outlet Tradisional Berinovasi</h3>
                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-lg-12 wow move-up">
                        <div class="flexible-image-slider-wrap">
                            <div class="swiper-container auto--pree-mode-flexible__container">
                                <div class="swiper-wrapper auto-plexible-row">
                                    @foreach($dataprofilbelanja as $profilbelanja)
                                    <div class="swiper-slide">
                                        <div class="single-flexible-slider">
                                            <img class="img-fluid" src="{{ $profilbelanja->content_image }}" alt="Slider 01">
                                        </div>
                                    </div>
                                    @endforeach
                                </div>
                            </div>
                            <div class="swiper-pagination swiper-pagination-6 section-space--mt_50"></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    <!--========== Call to Action Area Start ============-->
    {{--<div class="cta-image-area_one section-space--ptb_80 cta-bg-image_one">--}}
        {{--<div class="container">--}}
            {{--<div class="row align-items-center">--}}
                {{--<div class="col-xl-8 col-lg-7">--}}
                    {{--<div class="cta-content md-text-center">--}}
                        {{--<h3 class="heading text-white">Assess your business potentials and find opportunities <span class="text-color-secondary">for bigger success</span></h3>--}}
                    {{--</div>--}}
                {{--</div>--}}
                {{--<div class="col-xl-4 col-lg-5">--}}
                    {{--<div class="cta-button-group--one text-center">--}}
                        {{--<a href="#" class="btn btn--white btn-one"><span class="btn-icon mr-2"><i class="far fa-comment-alt-dots"></i></span> Let's talk</a>--}}
                        {{--<a href="#" class="btn btn--secondary  btn-two"><span class="btn-icon mr-2"><i class="far fa-info-circle"></i></span> Get info</a>--}}
                    {{--</div>--}}
                {{--</div>--}}
            {{--</div>--}}
        {{--</div>--}}
    {{--</div>--}}
    <!--========== Call to Action Area End ============-->








{{--</div>--}}


@include('front.template.footer')