<!--====================  footer area ====================-->
    <div class="footer-area-wrapper reveal-footer bg-gray">
        <div class="footer-area section-space--ptb_80">
            <div class="container">
                <div class="row footer-widget-wrapper">
                    <div class="col-lg-4 col-md-6 col-sm-6 footer-widget">
                        <div class="footer-widget__logo mb-30">
                            <img src="{{ asset('public/assets/images/logo.png') }}" class="img-fluid" alt="logo-footer" style="width: 140px">
                        </div>
                        <ul class="footer-widget__list">
                            <li>{{ $setting[9]['setting_value'] }}</li>
                            <li><a href="mailto:{{ $setting[10]['setting_value'] }}" class="hover-style-link">{{ $setting[10]['setting_value'] }}</a></li>
                            <li><a href="{{ $setting[8]['setting_value'] }}" class="hover-style-link text-black font-weight--bold">{{ $setting[8]['setting_value'] }}</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 footer-widget">
                        <h6 class="footer-widget__title mb-20">IT Services</h6>
                        <ul class="footer-widget__list">
                            <li><a href="#" class="hover-style-link">Managed IT</a></li>
                            <li><a href="#" class="hover-style-link">IT Support</a></li>
                            <li><a href="#" class="hover-style-link">IT Consultancy</a></li>
                            <li><a href="#" class="hover-style-link">Cloud Computing</a></li>
                            <li><a href="#" class="hover-style-link">Cyber Security</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-3 col-md-6 col-sm-6 footer-widget">
                        <h6 class="footer-widget__title mb-20">Quick links</h6>
                        <ul class="footer-widget__list">
                            <li><a href="#" class="hover-style-link">Pick up locations</a></li>
                            <li><a href="#" class="hover-style-link">Terms of Payment</a></li>
                            <li><a href="#" class="hover-style-link">Privacy Policy</a></li>
                            <li><a href="#" class="hover-style-link">Where to Find Us</a></li>
                        </ul>
                    </div>
                    <div class="col-lg-2 col-md-4 col-sm-6 footer-widget">
                        <div class="footer-widget__title section-space--mb_50"></div>
                        <ul class="footer-widget__list">
                            <li><a href="#" class="image_btn"><img class="img-fluid" src="{{asset('public/frontend/assets/images/icons/aeroland-button-google-play.jpg')}}" alt=""></a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
        <div class="footer-copyright-area section-space--pb_30">
            <div class="container">
                <div class="row align-items-center">
                    <div class="col-md-6 text-center text-md-left">
                        <span class="copyright-text">&copy; 2020 Aplikasitoko.co.id. All Rights Reserved.</span>
                    </div>
                    <div class="col-md-6 text-center text-md-right">
                        <ul class="list ht-social-networks solid-rounded-icon">

                            <li class="item">
                                <a href="{{ $setting[11]['setting_value'] }}" target="_blank" aria-label="youtube" class="social-link hint--bounce hint--top hint--primary">
                                    <i class="fab fa-youtube link-icon"></i>
                                </a>
                            </li>
                            <li class="item">
                                <a href="{{ $setting[5]['setting_value']}}" target="_blank" aria-label="Facebook" class="social-link hint--bounce hint--top hint--primary">
                                    <i class="fab fa-facebook-f link-icon"></i>
                                </a>
                            </li>
                            <li class="item">
                                <a href="{{ $setting[4]['setting_value']}}" target="_blank" aria-label="Instagram" class="social-link hint--bounce hint--top hint--primary">
                                    <i class="fab fa-instagram link-icon"></i>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <!--====================  End of footer area  ====================-->




    <!--====================  scroll top ====================-->
    <a href="#" class="scroll-top" id="scroll-top">
        <i class="arrow-top fal fa-long-arrow-up"></i>
        <i class="arrow-bottom fal fa-long-arrow-up"></i>
    </a>
    <!--====================  End of scroll top  ====================-->
    <!-- Start Toolbar -->

    <!-- JS
============================================ -->
    <!-- Modernizer JS -->
    <script src="{{asset('public/frontend/assets/js/vendor/modernizr-2.8.3.min.js')}}"></script>

    <!-- jQuery JS -->
    <script src="{{asset('public/frontend/assets/js/vendor/jquery-3.3.1.min.js')}}"></script>

    <!-- Bootstrap JS -->
    <script src="{{asset('public/frontend/assets/js/vendor/bootstrap.min.js')}}"></script>

    <!-- Swiper Slider JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/swiper.min.js')}}"></script>

    <!-- Light gallery JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/lightgallery.min.js')}}"></script>

    <!-- Waypoints JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/waypoints.min.js')}}"></script>

    <!-- Counter down JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/countdown.min.js')}}"></script>

    <!-- Isotope JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/isotope.min.js')}}"></script>

    <!-- Masonry JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/masonry.min.js')}}"></script>

    <!-- ImagesLoaded JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/images-loaded.min.js')}}"></script>

    <!-- Wavify JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/wavify.js')}}"></script>

    <!-- jQuery Wavify JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/jquery.wavify.js')}}"></script>

    <!-- circle progress JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/circle-progress.min.js')}}"></script>

    <!-- counterup JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/counterup.min.js')}}"></script>

    <!-- wow JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/wow.min.js')}}"></script>

    <!-- animation text JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/animation-text.min.js')}}"></script>

    <!-- Vivus JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/vivus.min.js')}}"></script>

    <!-- Some plugins JS -->
    <script src="{{asset('public/frontend/assets/js/plugins/some-plugins.js')}}"></script>

    <script src="{{asset('public/frontend/assets/js/main.js')}}"></script>


    </body>

    </html>
