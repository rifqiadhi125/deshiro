<div class="header-bottom-wrap bg-white header-sticky">
    <div class="container">
        <div class="row">
            <div class="col-lg-12">
                <div class="header position-relative">
                    <!-- brand logo -->
                    <div class="header__logo">
                        <a href="{{url('/')}}">
                            <img src="{{ asset('public/assets/images/logo.png') }}" class="img-fluid" alt="">
                        </a>
                    </div>

                    <div class="header-right">

                        <!-- navigation menu -->
                        <div class="header__navigation menu-style-three d-none d-xl-block">
                            <nav class="navigation-menu">
                                <ul>
                                    <li class="">
                                        <a href="{{url('/')}}"><span>Home</span></a>
                                    </li>
                                    <li class="">
                                        <a href="{{url('tentang-kami')}}"><span>Tentang APLIKASITOKO</span></a>
                                    </li>
                                    <li class="">
                                        <a href="{{url('layanan')}}"><span>Layanan</span></a>
                                    </li>
                                    <li class="">
                                        <a href="{{url('mitra-kami')}}"><span>Mitra kami</span></a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('blog') }}"><span>Blog</span></a>
                                    </li>
                                    <li class="">
                                        <a href="{{ url('kontakkami') }}"><span>Kontak kami </span></a>
                                    </li>
                                </ul>
                            </nav>
                        </div>

                        <!-- mobile menu -->
                        <div class="mobile-navigation-icon d-block d-xl-none" id="mobile-menu-trigger">
                            <i></i>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
