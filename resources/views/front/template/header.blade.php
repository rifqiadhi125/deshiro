<!DOCTYPE html>
<html class="no-js" lang="zxx">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <title>Home - Aplikasitoko.co.id Whitelabel</title>
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- Favicon -->
    <link rel="icon" href="{{ asset('public/assets/images/favicon.png') }}">
    <link rel="stylesheet" href="{{asset('public/frontend/assets/css/vendor/vendor.min.css')}}">
    <link rel="stylesheet" href="{{asset('public/frontend/assets/css/plugins/plugins.min.css')}}">
    <!-- Main Style CSS -->
    <link rel="stylesheet" href="{{asset('public/frontend/assets/css/style.css')}}">

</head>

<body>


<div class="preloader-activate preloader-active open_tm_preloader">
    <div class="preloader-area-wrap">
        <div class="spinner d-flex justify-content-center align-items-center h-100">
            <div class="bounce1"></div>
            <div class="bounce2"></div>
            <div class="bounce3"></div>
        </div>
    </div>
</div>


<!--====================  header area ====================-->
<div class="header-area">
    <div class="header-top-bar-info bg-gray d-none d-lg-block">
        <div class="container">
            <div class="row">
                <div class="col-md-12">
                    <div class="top-bar-wrap">
                        <div class="top-bar-left">
                            <div class="top-bar-text">BISNIS PPOB
                                tidak pernah seMUDAH ini</div>
                        </div>
                        <div class="top-bar-right">
                            <ul class="top-bar-info">
                                <li class="info-item">
                                    <a href="tel:01228899900" class="info-link">
                                        <i class="info-icon fa fa-phone"></i>
                                        <span class="info-text"><strong>{{ $setting[8]['setting_value'] }}</strong></span>
                                    </a>
                                </li>
                                <li class="info-item">
                                    <i class="info-icon fa fa-map-marker-alt"></i>
                                    <span class="info-text">{{ $setting[9]['setting_value'] }}</span>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</div>
