<!DOCTYPE html>
<html lang="en-gb" dir="ltr">

<head>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <title>El - Streaming Anime</title>
  <link rel="shortcut icon" type="image/png" href="{{ asset('public/assets/images/favicon.png') }}" >
  <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600&display=swap" rel="stylesheet">
  <link href="https://fonts.googleapis.com/css?family=Leckerli+One&display=swap" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('public/assets/css/main.css') }}" />
  <script src="{{ asset('public/assets/js/uikit.js') }}"></script>
</head>

<body>
<?php $home = "uk-active"; $genre = "" ?>
@if(Request::segment(1) == 'genre')
<?php $genre = "uk-active"; $home = "" ?>
@endif
<nav class="uk-navbar-container uk-letter-spacing-small">
  <div class="uk-container">
    <div class="uk-position-z-index" data-uk-navbar>
      <div class="uk-navbar-left">
        <a class="uk-navbar-item" href="{{ url('') }}"><img src="{{ asset('public/assets/images/deshiro.png') }}" style="width: 100px"></a>
        <ul class="uk-navbar-nav uk-visible@m uk-margin-large-left">
          <li class="{{ $home }}"><a href="{{ url('') }}">Home</a></li>
          <li class="{{ $genre }}"><a href="{{ url('genre') }}">Genre</a></li>
        </ul>
      </div>
      <div class="uk-navbar-right">
        <div>
          <a class="uk-navbar-toggle" data-uk-search-icon href="#"></a>
          <div class="uk-drop uk-background-default" data-uk-drop="mode: click; pos: left-center; offset: 0">
            <form class="uk-search uk-search-navbar uk-width-1-1">
              <input class="uk-search-input uk-text-demi-bold" type="search" placeholder="Search..." autofocus>
            </form>
          </div>
        </div>
        @if(empty(Auth::user()))
        <ul class="uk-navbar-nav uk-visible@m">
          <li ><a href="{{ url('login') }}">Sign In</a></li>
        </ul>
        <div class="uk-navbar-item">
          <div><a class="uk-button uk-button-primary" href="{{ url('register') }}">Sign Up</a></div>
        </div>
        @endif
        <a class="uk-navbar-toggle uk-hidden@m" href="#offcanvas" data-uk-toggle><span
          data-uk-navbar-toggle-icon></span></a>
      </div>
    </div>
  </div>
</nav>
