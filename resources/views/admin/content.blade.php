@include('admin.template.header')
@include('admin.template.navbar')
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Content</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                    <li class="active">Content</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="panel">
            <div class="panel-heading">MANAJEMEN CONTENT
                <a class="btn btn-info btn-sm btn-rounded float-right" style="float: right;" href="{{ url('admin/content/tambah') }}"><i class="fa fa-plus"></i>&nbsp;TAMBAH CONTENT</a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover manage-u-table">
                    <thead>
                        <tr>
                            <th width="70" class="text-center">#</th>
                            <th width="80" class="text-center">PIC</th>
                            <th>NAMA KONTEN</th>
                            <th>KATEGORI</th>
                            <th width="100" class="text-center">STATUS</th>
                            <th width="150" class="text-center">AKSI</th>
                        </tr>
                    </thead>
                    <tbody>
                    @foreach($content as $contents => $con)
                        <tr>
                            <td class="text-center">{{ $contents + $content->firstitem() }}</td>
                            <td><img src="{{ $con->content_image }}" alt="" style="max-width:100%"></td>
                            <td><span class="font-medium">{{ $con->content_name }}</span></td>
                            <td><span class="font-medium">{{ $con->kategori->c_cat_name }}</span></td>
                            <td align="center"><span class="text-center">
                                    @if($con->content_status == 'draft')
                                        <span class="badge badge-warning">Draft</span>
                                    @else
                                        <span class="badge badge-success">Published</span>
                                    @endif
                                </span></td>
                            <td class="text-center">
                                <a href="{{ url('admin/content/edit', $con->content_id) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                <a href="{{ url('admin/content/hapus', $con->content_id) }}" class="btn btn-sm btn-rounded btn-danger delete-confirm">Hapus</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="col-md-12">
                    <div style="float: right">{{ $content->links() }}</div>
                </div>
            </div>
        </div>
    </div>
@include('admin.template.footer')
