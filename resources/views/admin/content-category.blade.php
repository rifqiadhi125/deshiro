@include('admin.template.header')
@include('admin.template.navbar')
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Kategori Konten</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                    <li class="active">Kategori Konten</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="row">
            <div class="col-md-4">
                @if(isset($editkategori))
                    <div class="panel">
                        <div class="panel-heading">TAMBAH KATEGORI KONTEN
                            <div class="pull-right">
                                <a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a>
                            </div>
                        </div>
                        <div class="panel-wrapper collapse">
                            <form class="form-horizontal" method="POST" action="{{ url('admin/content-category/tambah/simpan') }}" style="padding: 20px; padding-top: 10px" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" id="imagetambahfull">
                                    <label class="col-md-12">Gambar Kategori</label>
                                    <div class="col-md-12">
                                        <a href="javascript:void(0)" class="btn btn-info btn-block"  data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;Input Gambar Kategori</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12" id="label-preview-crop-image" style="display:none;">Gambar Hasil Crop</label>
                                    <div class="col-md-12">
                                        <div id="preview-crop-image" style="background:#fff; width:300px; height:300px; display:none;"></div>
                                    </div>
                                </div>
                                <input type="hidden" name="gambar" id="preview-crop-image-input">
                                <div class="form-group">
                                    <label class="col-md-12">Nama Kategori</label>
                                    <div class="col-md-12">
                                        <input type="text" name="c_cat_name" class="form-control" placeholder="Nama Kategori..." required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Parent Kategori</label>
                                    <div class="col-md-12">
                                        <select name="c_cat_parent" class="form-control">
                                            <option disabled selected>Pilih Kategori Konten</option>
                                            @foreach($data_kategori as $kat)
                                                <option value="{{ $kat->c_cat_id }}">{{ $kat->c_cat_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Deskripsi Kategori</label>
                                    <div class="col-md-12">
                                        <input type="text" name="c_cat_desc" class="form-control" placeholder="Nama Kategori..." required>
                                    </div>
                                </div>
                                <div class="form-group" style="float: right">
                                    <label class="col-md-12" style="text-align: right">Status Kategori</label>
                                    <div class="col-md-12" style="text-align: right">
                                        <label id="statusnon">Non Aktif</label>
                                        <input type="checkbox" checked name="status" id="status" class="js-switch" data-color="#13dafe" data-size="small" onclick="displaystatus()" />
                                        <label id="statusaktif">Aktif</label>
                                    </div>
                                </div>
                                <button class="btn btn-info btn-block">Simpan Kategori</button>
                            </form>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">EDIT KATEGORI KONTEN
                            <div class="pull-right">
                                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-wrapper">
                            <form class="form-horizontal" method="POST" action="{{ url('admin/content-category/update', $editkategori->c_cat_id ) }}" style="padding: 20px; padding-top: 10px">
                                @csrf
                                @method('patch')
                                <div class="form-group" id="imagetambahfull">
                                    <label class="col-md-12">Gambar Kategori</label>
                                    <div class="col-md-12">
                                        <img src="{{ $editkategori->c_cat_image }}" alt="" style="width:100%;max-width:100%; margin-bottom: 10px">
                                        <a href="javascript:void(0)" class="btn btn-info btn-block"  data-toggle="modal" data-target="#editgambar"><i class="fa fa-image"></i>&nbsp;Edit Gambar Kategori</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12" id="label-preview-crop-image" style="display:none;">Gambar Hasil Crop</label>
                                    <div class="col-md-12">
                                        <div id="preview-crop-image" style="background:#fff; width:300px; height:300px; display:none;"></div>
                                    </div>
                                </div>
                                <input type="hidden" name="gambar" id="preview-crop-image-input">
                                <div class="form-group">
                                    <label class="col-md-12">Nama Kategori</label>
                                    <div class="col-md-12">
                                        <input type="text" name="c_cat_name" class="form-control" placeholder="Nama Kategori..." value="{{$editkategori->c_cat_name}}" required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Parent Kategori</label>
                                    <div class="col-md-12">
                                        <select name="c_cat_parent" class="form-control">
                                            <option value="">Pilih Kategori Parent</option>
                                            @foreach($data_kategori as $kat)
                                                <option value="{{ $kat->c_cat_id }}"
                                                        @if($editkategori->c_cat_parent == $kat->c_cat_id)
                                                        selected
                                                    @endif
                                                >{{ $kat->c_cat_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Deskripsi Kategori</label>
                                    <div class="col-md-12">
                                        <input type="text" name="c_cat_desc" class="form-control" placeholder="Deskripsi Kategori..."  value="{{$editkategori->c_cat_desc}}" required>
                                    </div>
                                </div>
                                <div class="form-group" style="float: right">
                                    <label class="col-md-12" style="text-align: right">Status Kategori</label>
                                    <div class="col-md-12" style="text-align: right">
                                        <label id="statusnon">Non Aktif</label>
                                        <input type="checkbox" @if($editkategori->c_cat_status === '1') checked @else @endif name="status" id="status" class="js-switch" data-color="#13dafe" data-size="small" onclick="displaystatus()" />
                                        <label id="statusaktif">Aktif</label>
                                    </div>
                                </div>
                                <button class="btn btn-info btn-block">Simpan Kategori</button>
                            </form>
                        </div>
                    </div>
                @else
                    <div class="panel">
                        <div class="panel-heading">TAMBAH KATEGORI KONTEN
                            <div class="pull-right">
                                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-wrapper">
                            <form class="form-horizontal" method="POST" action="{{ url('admin/content-category/tambah/simpan') }}" style="padding: 20px; padding-top: 10px" enctype="multipart/form-data">
                                @csrf
                                <div class="form-group" id="imagetambahfull">
                                    <label class="col-md-12">Gambar Kategori</label>
                                    <div class="col-md-12">
                                        <a href="javascript:void(0)" class="btn btn-info btn-block"  data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;Input Gambar Kategori</a>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12" id="label-preview-crop-image" style="display:none;">Gambar Hasil Crop</label>
                                    <div class="col-md-12">
                                        <div id="preview-crop-image" style="background:#fff; width:300px; height:300px; display:none;"></div>
                                    </div>
                                </div>
                                <input type="hidden" name="gambar" id="preview-crop-image-input">
                                <div class="form-group">
                                    <label class="col-md-12">Nama Kategori</label>
                                    <div class="col-md-12">
                                        <input type="text" name="c_cat_name" class="form-control" placeholder="Nama Kategori..." required>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Parent Kategori</label>
                                    <div class="col-md-12">
                                        <select name="c_cat_parent" class="form-control">
                                            <option disabled selected>Pilih Kategori Konten</option>
                                            @foreach($data_kategori as $kat)
                                                <option value="{{ $kat->c_cat_id }}">{{ $kat->c_cat_name }}</option>
                                            @endforeach
                                        </select>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label class="col-md-12">Deskripsi Kategori</label>
                                    <div class="col-md-12">
                                        <input type="text" name="c_cat_desc" class="form-control" placeholder="Nama Kategori..." required>
                                    </div>
                                </div>
                                <div class="form-group" style="float: right">
                                    <label class="col-md-12" style="text-align: right">Status Kategori</label>
                                    <div class="col-md-12" style="text-align: right">
                                        <label id="statusnon">Non Aktif</label>
                                        <input type="checkbox" checked name="status" id="status" class="js-switch" data-color="#13dafe" data-size="small" onclick="displaystatus()" />
                                        <label id="statusaktif">Aktif</label>
                                    </div>
                                </div>
                                <button class="btn btn-info btn-block">Simpan Kategori</button>
                            </form>
                        </div>
                    </div>
                @endif
            </div>
            <div class="col-md-8">
                <div class="panel">
                    <div class="panel-heading">MANAJEMEN KATEGORI KONTEN</div>
                    <div class="table-responsive">
                        <table class="table table-hover manage-u-table">
                            <thead>
                            <tr>
                                <th width="70" class="text-center">#</th>
                                <th width="80" class="text-center">PIC</th>
                                <th>NAMA KONTEN</th>
                                <th width="100" class="text-center">STATUS</th>
                                <th width="150" class="text-center">AKSI</th>
                            </tr>
                            </thead>
                            <tbody>
                            <?php $no = 0; ?>
                            @foreach($category as $categorys => $cat)
                            <?php $no++?>
                                <tr>
                                    <td class="text-center">{{ $no }}</td>
                                    <td><img src="{{ $cat->c_cat_image }}" alt="" style="max-width:100%"></td>
                                    <td><span class="font-medium">{{ $cat->c_cat_name }}</span></td>
                                    <td align="center"><span class="text-center">
                                    @if($cat->c_cat_status == '1')
                                                <span class="badge badge-success">Aktif</span>
                                            @else
                                                <span class="badge badge-warning">Non Aktif</span>
                                            @endif
                                    </span></td>
                                    <td class="text-center">
                                        <a href="{{ url('admin/content-category/edit', $cat->c_cat_id) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                        <a href="{{ url('admin/content-category/hapus', $cat->c_cat_id) }}" class="btn btn-sm btn-rounded btn-danger delete-confirm">Hapus</a>
                                    </td>
                                </tr>
                                <?php $subkategori = \Illuminate\Support\Facades\DB::table('_content_category')->where('c_cat_parent', '=', $cat->c_cat_id)->get() ?>
                                @if(isset($subkategori))
                                    @foreach($subkategori as $sub)
                                    <tr>
                                        <td class="text-center">SUB {{ $no }}</td>
                                        <td><img src="{{ $sub->c_cat_image }}" alt="" style="max-width:100%"></td>
                                        <td><span class="font-medium">{{ $sub->c_cat_name }}</span></td>
                                        <td align="center"><span class="text-center">
                                        @if($sub->c_cat_status == '1')
                                            <span class="badge badge-success">Aktif</span>
                                        @else
                                            <span class="badge badge-warning">Non Aktif</span>
                                        @endif
                                        </span></td>
                                        <td class="text-center">
                                            <a href="{{ url('admin/content-category/edit', $sub->c_cat_id) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                            <a href="{{ url('admin/content-category/hapus', $sub->c_cat_id) }}" class="btn btn-sm btn-rounded btn-danger delete-confirm">Hapus</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                @endif
                            @endforeach
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
        </div>

        <div class="modal fade" id="gantigambar" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Crop Gambar Kategori</h4> </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-5">
                                    <div class="form-group">
                                        <label class="control-label">Input Gambar:</label>
                                        <input type="file" class="dropify" id="imagekonten" required>
                                    </div>
                                </div>
                                <div class="col-md-7">
                                    <div class="form-group">
                                        <label class="col-md-12">Crop Gambar</label>
                                        <div class="col-md-12">
                                            <div id="upload-demo-konten"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="modal-footer">
                        <button class="btn btn-primary btn-block upload-image-konten" id="upload-image-konten">Crop Gambar</button>
                    </div>
                </div>
            </div>
        </div>
        @if(isset($editkategori))
        <div class="modal fade bs-example-modal-lg" id="editgambar" tabindex="-1" role="dialog">
            <div class="modal-dialog modal-lg" role="document">
                <div class="modal-content" >
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                        <h4 class="modal-title">Form Ganti Gambar Kategori Konten</h4> </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <label class="control-label">Input Gambar:</label>
                            <input type="file" class="form-control" id="imageeditkategori" required>
                        </div>
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-12">Crop Gambar</label>
                                        <div class="col-md-12">
                                            <div id="upload-demo-konten-edit"></div>
                                            <button class="btn btn-primary btn-block upload-image-edit-kategori" id="upload-image-edit-konten" style="margin-top: 10px">Crop Gambar</button>
                                        </div>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label class="col-md-12"><br>Hasil Crop</label>
                                        <div class="col-md-12">
                                            <div id="preview-crop-image-konten" style="background:#9d9d9d; width:auto; height:300px;"></div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <form method="POST" action="{{ url('admin/content-category/update-gambar', $editkategori->c_cat_id) }}">
                        @csrf
                        @method('PATCH')
                        <input type="hidden" name="gambar" id="preview-crop-image-input-edit">
                        <div class="modal-footer">
                            <button id="submitupdategambar" class="btn btn-primary">Simpan</button>
                    </form>
                </div>
            </div>
        </div>
        @endif
    </div>

    </div>
@include('admin.template.footer')
