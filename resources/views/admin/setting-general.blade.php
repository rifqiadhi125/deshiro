@include('admin.template.header')
@include('admin.template.navbar')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Setting General</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Setting General</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- .row -->
        <form class="form-horizontal" method="POST" action="{{ url('admin/setting-general/update') }}" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="white-box">
                <h3 class="box-title m-b-0">Form Setting General</h3>
                <p class="text-muted m-b-30 font-13">Setting General</p>
                <div class="row">
                    <div class="col-md-3">
                        <div class="form-group">
                            <label class="col-md-12">Logo</label>
                            <div class="col-md-12">
                                <input type="file" name="formlogo" class="dropify" data-default-file="{{ asset('public/uploads/logo/'.$setting[12]->setting_value ) }}">
                                <input type="hidden" name="logolama" value="{{ $setting[12]->setting_value }}">
                            </div>
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="form-group">
                            <label class="col-md-12">Nama App</label>
                            <div class="col-md-12">
                                <input type="text" name="formname" class="form-control" placeholder="Nama App..." value="{{ $setting[0]->setting_value }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Link Instagram</label>
                            <div class="col-md-12">
                                <input type="text" name="forminstagram" class="form-control" placeholder="Instagram Link..." value="{{ $setting[4]->setting_value }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Link Facebook</label>
                            <div class="col-md-12">
                                <input type="text" name="formfacebook" class="form-control" placeholder="Facebook Link..." value="{{ $setting[5]->setting_value }}" required>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-md-12">Link Youtube</label>
                    <div class="col-md-12">
                        <input type="text" name="formyoutube" class="form-control" placeholder="Youtube Link..." value="{{ $setting[6]->setting_value }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Nomor Telepon</label>
                    <div class="col-md-12">
                        <input type="text" name="formphone" class="form-control" placeholder="No Telepon..." value="{{ $setting[7]->setting_value }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Nomor Whatsapp</label>
                    <div class="col-md-12">
                        <input type="text" name="formwhatsapp" class="form-control" placeholder="No Whatsapp..." value="{{ $setting[8]->setting_value }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Email</label>
                    <div class="col-md-12">
                        <input type="text" name="formemail" class="form-control" placeholder="Email..." value="{{ $setting[10]->setting_value }}" required>
                    </div>
                </div>
                <div class="form-group">
                    <label class="col-md-12">Alamat</label>
                    <div class="col-md-12">
                        <textarea name="formaddress" class="form-control" cols="10" rows="5" placeholder="Alamat..." required>{{ $setting[9]->setting_value }}</textarea>
                    </div>
                </div>
                <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Setting</button>
            </div>
    </div>
</div>
</form>
</div>
@include('admin.template.footer')
