@include('admin.template.header')
@include('admin.template.navbar')

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Edit Episode</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="">Anime</a></li>
                            <li class="active">Edit Episode</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                	<form class="form-horizontal" method="POST" action="{{ url('admin/episode/update/'.$episode->id_episode) }}">
                	@csrf
                    @method('patch')
                	<div class="row">
                		<div class="col-md-12">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Edit Episode</h3>
                            <p class="text-muted m-b-30 font-13">Edit Episode</p>
                            	<input type="hidden" name="id_anime" class="form-control" value="{{ $anime->id_anime }}" required>
                            	<div class="form-group">
                            		<label class="col-md-12">Nama Episode</label>
                            		<div class="col-md-12">
                                        <input type="hidden" name="slug_anime" value="{{ $anime->slug_anime }}-">
                            			<input type="text" name="nama_episode" class="form-control" placeholder="Nama Episode" value="{{ $episode->nama_episode }}" required>
                            		</div>
                            	</div>
                                <div class="form-group">
                                    <div class="col-md-12">
                                        <h3 class="box-title m-b-0">Download Links</h3>
                                        <div class="checkbox checkbox-danger">
                                            @if($episode->v240p == '1')
                                            <input name="v240p" id="240p" type="checkbox" onclick="display240p()" value="1" checked>
                                            @else
                                            <input name="v240p" id="240p" type="checkbox" onclick="display240p()" value="1">
                                            @endif
                                            <label for="240p"> 240p </label>
                                        </div>
                                        @if($episode->v240p == '1')
                                        <div id="240pbox" style="display: block;">
                                        @else
                                        <div id="240pbox" style="display: none;">
                                        @endif
                                            <br>
                                            <label>Link 1 240p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link1_240p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link1_240p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link1_240p" placeholder="Link Download..." value="{{ $episode->link1_240p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 2 240p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link2_240p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link2_240p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link2_240p" placeholder="Link Download..." value="{{ $episode->link2_240p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 3 240p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link3_240p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link3_240p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link3_240p" placeholder="Link Download..." value="{{ $episode->link3_240p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 4 240p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link4_240p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link4_240p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link4_240p" placeholder="Link Download" value="{{ $episode->link4_240p }}"><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkbox checkbox-danger">
                                            @if($episode->v360p == '1')
                                            <input name="v360p" id="360p" type="checkbox" onclick="display360p()" value="1" checked>
                                            @else
                                            <input name="v360p" id="360p" type="checkbox" onclick="display360p()" value="1">
                                            @endif
                                            <label for="360p"> 360p </label>
                                        </div>
                                        @if($episode->v360p == '1')
                                        <div id="360pbox" style="display: block;">
                                        @else
                                        <div id="360pbox" style="display: none;">
                                        @endif
                                            <br>
                                            <label>Link 1 360p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link1_360p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link1_360p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link1_360p" placeholder="Link Download..." value="{{ $episode->link1_360p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 2 360p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link2_360p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link2_360p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link2_360p" placeholder="Link Download..." value="{{ $episode->link2_360p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 3 360p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link3_360p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link3_360p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link3_360p" placeholder="Link Download..." value="{{ $episode->link3_360p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 4 360p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link4_360p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link4_360p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link4_360p" placeholder="Link Download..." value="{{ $episode->link4_360p }}"><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkbox checkbox-danger">
                                            @if($episode->v480p == '1')
                                            <input name="v480p" id="480p" type="checkbox" onclick="display480p()" value="1" checked>
                                            @else
                                            <input name="v480p" id="480p" type="checkbox" onclick="display480p()" value="1">
                                            @endif
                                            <label for="480p"> 480p </label>
                                        </div>
                                        @if($episode->v480p == '1')
                                        <div id="480pbox" style="display: block;">
                                        @else
                                        <div id="480pbox" style="display: none;">
                                        @endif
                                            <br>
                                            <label>Link 1 480p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link1_480p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link1_480p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link1_480p" placeholder="Link Download..." value="{{ $episode->link1_480p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 2 480p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link2_480p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link2_480p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link2_480p" placeholder="Link Download..." value="{{ $episode->link2_480p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 3 480p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link3_480p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link3_480p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link3_480p" placeholder="Link Download..." value="{{ $episode->link3_480p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 4 480p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link4_480p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link4_480p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link4_480p" placeholder="Link Download..." value="{{ $episode->link4_480p }}"><br>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="checkbox checkbox-danger">
                                            @if($episode->v720p == '1')
                                            <input name="v720p" id="720p" type="checkbox" onclick="display720p()" value="1" checked>
                                            @else
                                            <input name="v720p" id="720p" type="checkbox" onclick="display720p()" value="1">
                                            @endif
                                            <label for="720p"> 720p </label>
                                        </div>
                                        @if($episode->v720p == '1')
                                        <div id="720pbox" style="display: block;">
                                        @else
                                        <div id="720pbox" style="display: none;">
                                        @endif
                                            <br>
                                            <label>Link 1 720p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link1_720p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link1_720p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link1_720p" placeholder="Link Download..." value="{{ $episode->link1_720p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 2 720p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link2_720p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link2_720p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link2_720p" placeholder="Link Download..." value="{{ $episode->link2_720p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 3 720p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link3_720p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link3_720p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link3_720p" placeholder="Link Download..." value="{{ $episode->link2_720p }}"><br>
                                                </div>
                                            </div>
                                            <label>Link 4 720p</label>
                                            <div class="row">
                                                <div class="col-md-2">
                                                    <select class="form-control" name="nama_link4_720p">
                                                        @foreach($server as $ser)
                                                        <option value="{{ $ser->nama_server }}"
                                                        @if($episode->nama_link4_720p == $ser->nama_server)
                                                        selected
                                                        @endif
                                                        >{{ $ser->nama_server }}</option>
                                                        @endforeach
                                                    </select><br>
                                                </div>
                                                <div class="col-md-10">
                                                    <input class="form-control" type="text" name="link4_720p" placeholder="Link Download..." value="{{ $episode->link4_720p }}"><br>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Episode</button>
                        </div>
                        </div>  
                    </div>
                	</form>
            </div>
@include('admin.template.footer')