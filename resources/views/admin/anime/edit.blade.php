@include('admin.template.header')
@include('admin.template.navbar')

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Edit Anime</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="">Anime</a></li>
                            <li class="active">Edit Anime</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                	<form class="form-horizontal" method="POST" action="{{ url('admin/anime/update', $anime->id_anime) }}" enctype="multipart/form-data">
                	@csrf
                    @method('PATCH')
                	<div class="row">
                		<div class="col-md-9">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Edit Anime</h3>
                            <p class="text-muted m-b-30 font-13">Edit Anime</p>
                            	<div class="form-group">
                            		<label class="col-md-12">Judul Anime</label>
                            		<div class="col-md-12">
                            			<input type="text" name="judul_anime" class="form-control" value="{{ $anime->judul_anime }}" required>
                            		</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="col-md-12">Judul Alternatif Anime</label>
                            		<div class="col-md-12">
                            			<input type="text" name="judul_alternatif_anime" class="form-control" value="{{ $anime->judul_alternatif_anime }}" required>
                            		</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="col-md-12">Sinopsis Anime</label>
                            		<div class="col-md-12">
                            			<textarea class="form-control" rows="8" id="summernote" name="sinopsis_anime">{{ $anime->sinopsis_anime }}</textarea>
                            		</div>
                            	</div>
                        </div>
                	</form>
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Tambah Episode Anime</h3>
                            <p class="text-muted m-b-30 font-13">Tambah Episode Anime</p>
                            <div class="table-responsive">
                                <table class="table table-hover manage-u-table">
                                    <thead>
                                        <tr>
                                            <th>NAMA EPISODE</th>
                                            <th width="150" class="text-center">AKSI</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        @foreach($anime->episode as $epi)
                                        <tr>
                                            <td><span class="font-medium">{{ $epi->nama_episode }}</span></td>
                                            <td class="text-center">
                                                <form action="{{ url('admin/episode/hapus', $epi->id_episode) }}" method="POST">
                                                @csrf
                                                @method('delete')
                                                <a href="{{ url('admin/episode/edit', $epi->id_episode) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                                <button type="submit" class="btn btn-sm btn-rounded btn-danger">Hapus</button>
                                                </form>
                                            </td>
                                        </tr>
                                        @endforeach
                                    </tbody>
                                </table>
                            </div>
                            <a class="btn btn-info btn-block" href="{{ url('admin/episode/tambah/'.$anime->id_anime) }}"><i class="fa fa-plus"></i>&nbsp;Tambah Episode</a> 
                        </div>
                        </div>
                        <div class="col-md-3">
                        	<div class="white-box">
                        	    <div class="form-group">
                                    <label class="col-md-12">Thumbnail Anime</label>
                        	    	<div class="col-md-12">
                        	    			<input type="file" name="thumbnail_anime" class="dropify" data-default-file="{{ asset('public/uploads/thumbnail/'.$anime->thumbnail_anime ) }}">
                        	    	</div>
                        	    </div>
                                <div class="form-group">
                                    <label class="col-md-12">Rating Anime</label>
                                    <div class="col-md-12">
                                            <input type="text" name="rating_anime" class="form-control" value="{{ $anime->rating_anime }}" required>
                                    </div>
                                </div>
                        	    <div class="form-group">
                        	    	<label class="col-md-12">Genre Anime</label>
                        	    	<div class="col-md-12">
	                        	    	<select class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Pilih Genre..." name="genre[]">
	                        	    		@foreach($genre as $gen)
	                        	    	    <option value="{{ $gen->id_genre }}"
                                                @foreach($anime->genre as $genre)
                                                @if($gen->id_genre == $genre->id_genre)
                                                selected
                                                @endif
                                                @endforeach
                                            >{{ $gen->nama_genre }}</option>
	                        	    	    @endforeach
	                        	    	</select>
                        	    	</div>
                        	    </div>
                        	    <button class="btn btn-lg btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Anime</button>	
                        	</div>
                        </div>  
                    </div>
            </div>
@include('admin.template.footer')