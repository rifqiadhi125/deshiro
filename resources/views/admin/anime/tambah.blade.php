@include('admin.template.header')
@include('admin.template.navbar')

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Tambah Anime</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="">Anime</a></li>
                            <li class="active">Tambah Anime</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                	<form class="form-horizontal" method="POST" action="{{ url('admin/anime/tambah/simpan') }}" enctype="multipart/form-data">
                	@csrf
                	<div class="row">
                		<div class="col-md-9">
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Tambah Anime</h3>
                            <p class="text-muted m-b-30 font-13">Tambah Anime</p>
                            	<div class="form-group">
                            		<label class="col-md-12">Judul Anime</label>
                            		<div class="col-md-12">
                            			<input type="text" name="judul_anime" class="form-control" placeholder="Judul Anime..." required>
                            		</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="col-md-12">Judul Alternatif Anime</label>
                            		<div class="col-md-12">
                            			<input type="text" name="judul_alternatif_anime" class="form-control" placeholder="Judul Alternatif Anime..." required>
                            		</div>
                            	</div>
                            	<div class="form-group">
                            		<label class="col-md-12">Sinopsis Anime</label>
                            		<div class="col-md-12">
                            			<textarea id="summernote" name="sinopsis_anime"></textarea>
                            		</div>
                            	</div>
                        </div>
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Tambah Episode Anime</h3>
                            <p class="text-muted m-b-30 font-13">Tambah Episode Anime</p>
                            <h4 class="text-center">EPISODE MASIH KOSONG</h4>
                            <button class="btn btn-info btn-block" alt="alert" id="sa-title"><i class="fa fa-plus"></i>&nbsp;Tambah Episode</button> 
                        </div>
                        </div>
                        <div class="col-md-3">
                        	<div class="white-box">
                        	    <div class="form-group">
                        	    	<label class="col-md-12">Thumbnail Anime</label>
                        	    	<div class="col-md-12">
                        	    			<input type="file" class="dropify" name="thumbnail_anime" placeholder="Judul Anime..." required>
                        	    	</div>
                        	    </div>
                                <div class="form-group">
                                    <label class="col-md-12">Rating Anime</label>
                                    <div class="col-md-12">
                                            <input type="text" name="rating_anime" class="form-control" placeholder="Rating Anime..." required>
                                    </div>
                                </div>
                        	    <div class="form-group">
                        	    	<label class="col-md-12">Genre Anime</label>
                        	    	<div class="col-md-12">
	                        	    	<select class="select2 m-b-10 select2-multiple" multiple="multiple" data-placeholder="Pilih Genre..." name="genre[]">
	                        	    		@foreach($genre as $gen)
	                        	    	    <option value="{{ $gen->id_genre }}">{{ $gen->nama_genre }}</option>
	                        	    	    @endforeach
	                        	    	</select>
                        	    	</div>
                        	    </div>
                        	    <button class="btn btn-lg btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Upload Anime</button>	
                        	</div>
                        </div>  
                    </div>
                	</form>
            </div>
@include('admin.template.footer')