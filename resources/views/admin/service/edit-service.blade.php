@include('admin.template.header')
@include('admin.template.navbar')
<meta name="csrf-token" content="{{ csrf_token() }}">


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Tambah Service</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('admin/user') }}">User</a></li>
                    <li class="active">Tambah Service</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- .row -->

        <div class="white-box">
            <h3 class="box-title m-b-0">Form Tambah Service</h3>
            <p class="text-muted m-b-30 font-13">Tambah Service</p>
            <div class="row">
                <div class="col-md-4" id="imagetambahfull">
                    <div class="form-group">
                        <label class="col-md-12">Gambar Service</label>
                        <div class="col-md-12">
                            <img src="{{ $service->service_images }}" alt="" style="width:100%;max-width:100%">
                            <button class="btn btn-info btn-block" style="margin-top:10px" data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;Ganti Gambar Service</button>
                        </div>
                    </div>
                </div>
                <form class="form-horizontal" method="POST" action="{{ url('admin/service/update', $service->service_id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-12">Judul Service</label>
                            <div class="col-md-12">
                                <input type="text" name="judul" class="form-control" placeholder="Judul Service..." value="{{ $service->service_title }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Deskripsi Service</label>
                            <div class="col-md-12">
                                <textarea name="deskripsi" class="form-control" id="deskripsi" rows="4" placeholder="Deskripsi Service...">{{ $service->service_desc }}</textarea>
                            </div>
                        </div>
                        <div class="form-group" style="float: right">
                            <label class="col-md-12" style="text-align: right">Status Service</label>
                            <div class="col-md-12" style="text-align: right">
                                <label id="statusnon">Non Aktif</label>
                                <input type="checkbox" @if($service->service_status === '1') checked @else @endif name="status" id="status" class="js-switch" data-color="#13dafe" data-size="small" onclick="displaystatus()" />
                                <label id="statusaktif">Aktif</label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Service</button>
            </div>
        </div>
    </div>
</div>
</form>

</div>
<div class="modal fade bs-example-modal-lg" id="gantigambar" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Ganti Gambar Service</h4> </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Input Gambar:</label>
                    <input type="file" class="form-control" id="imageservice" required>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Crop Gambar</label>
                                <div class="col-md-12">
                                    <div id="upload-demo-service"></div>
                                    <button class="btn btn-primary btn-block upload-image-service-edit" id="upload-image-service-edit" style="margin-top: 10px">Crop Gambar</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Hasil Crop</label>
                                <div class="col-md-12">
                                    <div id="preview-crop-image" style="background:#9d9d9d; width:300px; height:300px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ url('admin/service/update-gambar', $service->service_id) }}">
                @csrf
                @method('PATCH')
                <input type="hidden" name="gambar" id="preview-crop-image-input">
                <div class="modal-footer">
                    <button id="submitupdategambar" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
</div>
</div>

@include('admin.template.footer')
