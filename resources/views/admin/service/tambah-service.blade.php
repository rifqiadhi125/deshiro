@include('admin.template.header')
@include('admin.template.navbar')
<meta name="csrf-token" content="{{ csrf_token() }}">


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Tambah Service</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('admin/user') }}">User</a></li>
                    <li class="active">Tambah Service</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- .row -->

        <div class="white-box">
            <h3 class="box-title m-b-0">Form Tambah Service</h3>
            <p class="text-muted m-b-30 font-13">Tambah Service</p>
            @csrf
            <div class="row">
                <div class="col-md-4" id="imagetambahfull">
                    <div class="form-group">
                        <label class="col-md-12">Gambar Service</label>
                        <div class="col-md-12">
                            <button class="btn btn-info btn-block" style="margin-top:10px" data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;Input Gambar Service</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-12" id="label-preview-crop-image" style="display:none;">Gambar Hasil Crop</label>
                        <div class="col-md-12">
                            <div id="preview-crop-image" style="background:#fff; width:300px; height:300px; display:none;"></div>
                        </div>
                    </div>
                </div>
                <form class="form-horizontal" method="POST" action="{{ url('admin/service/tambah/simpan') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="gambar" id="preview-crop-image-input">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-12">Judul Service</label>
                            <div class="col-md-12">
                                <input type="text" name="judul" class="form-control" placeholder="Judul Service..." required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Deskripsi Service</label>
                            <div class="col-md-12">
                                <textarea name="deskripsi" class="form-control" id="deskripsi" rows="4" placeholder="Deskripsi Service..."></textarea>
                            </div>
                        </div>
                        <div class="form-group" style="float: right">
                            <label class="col-md-12" style="text-align: right">Status Service</label>
                            <div class="col-md-12" style="text-align: right">
                                <label id="statusnon">Non Aktif</label>
                                <input type="checkbox" checked name="status" id="status" class="js-switch" data-color="#13dafe" data-size="small" onclick="displaystatus()" />
                                <label id="statusaktif">Aktif</label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Service</button>
            </div>
        </div>
    </div>
</div>
</form>
</div>

<div class="modal fade" id="gantigambar" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Crop Gambar Service</h4> </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label">Input Gambar:</label>
                                <input type="file" class="dropify" id="imageservice" required>
                            </div>
                        </div>
                        <div class="col-md-7">
                            <div class="form-group">
                                <label class="col-md-12">Crop Gambar</label>
                                <div class="col-md-12">
                                    <div id="upload-demo-service"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-block upload-image-service" id="upload-image-service">Crop Gambar</button>
            </div>
        </div>
    </div>
</div>

@include('admin.template.footer')
