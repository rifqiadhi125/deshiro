@include('admin.template.header')
@include('admin.template.navbar')
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman User</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                            <li class="active">User</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- ============================================================== -->
                <!-- Other sales widgets -->
                <!-- ============================================================== -->
            
            <!-- @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('success') }}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('error') }}
                </div>
            @endif -->

                    <div class="panel">
                        <div class="panel-heading">MANAJEMEN CUSTOMER
                        <a class="btn btn-info btn-sm float-right" style="float: right;" href="{{ url('admin/customer/tambah') }}"><i class="fa fa-plus"></i>&nbsp;TAMBAH CUSTOMER</a>
                        </div>
                        <div class="row" style="margin: 10px">
                        <div class="col-md-4">
                        <form action="{{ url('admin/customer-import') }}" method="POST" enctype="multipart/form-data">
                            @csrf
                            <input type="file" name="file" class="form-control" required>
                        </div>
                        <div class="col-md-8">
                            <button class="btn btn-success">Import User Data</button>
                            <a class="btn btn-warning" href="{{ url('admin/customer-export') }}">Export User Data</a>
                        </form>
                        </div>
                        
                        </div>

                        <div class="table-responsive">
                            <table class="table table-hover manage-u-table">
                                <thead>
                                    <tr>
                                        <th width="70" class="text-center">#</th>
                                        <th>NAMA CUSTOMER</th>
                                        <th>GENDER CUSTOMER</th>
                                        <th>ALAMAT CUSTOMER</th>
                                        <th width="150" class="text-center">AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($customer as $customers => $cus)
                                    <tr>
                                        <td class="text-center">{{ $customers + $customer->firstitem() }}</td>
                                        <td><span class="font-medium">{{ $cus->nama }}</span></td>
                                        <td><span class="font-medium">{{ $cus->gender }}</span></td>
                                        <td><span class="font-medium">{{ $cus->alamat }}</span></td>
                                        <td class="text-center">
                                            <a href="{{ url('admin/customer/edit', $cus->id) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                            <a href="{{ url('admin/customer/hapus', $cus->id) }}" class="btn btn-sm btn-rounded btn-danger delete-confirm">Hapus</a>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            
                            <div class="col-md-12">
                                <div style="float: right">{{ $customer->links() }}</div>
                            </div>
                        </div>
                    </div>
            </div>
@include('admin.template.footer')