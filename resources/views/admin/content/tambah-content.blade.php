@include('admin.template.header')
@include('admin.template.navbar')
<meta name="csrf-token" content="{{ csrf_token() }}">


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Tambah Konten</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('admin/content') }}">Konten</a></li>
                    <li class="active">Tambah Konten</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- .row -->

        <div class="white-box">
            <h3 class="box-title m-b-0">Form Tambah Konten</h3>
            <p class="text-muted m-b-30 font-13">Tambah Konten</p>
            <form class="form-horizontal" method="POST" action="{{ url('admin/content/tambah/simpan') }}" enctype="multipart/form-data">
            @csrf
            <div class="row">
                <div class="col-md-4">
                    <div class="form-group" id="imagetambahfull">
                        <label class="col-md-12">Gambar Konten</label>
                        <div class="col-md-12">
                            <button class="btn btn-info btn-block"  data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;Input Gambar Konten</button>
                        </div>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12" id="label-preview-crop-image" style="display:none;">Gambar Hasil Crop</label>
                        <div class="col-md-12">
                            <div id="preview-crop-image" style="background:#fff; width:300px; height:300px; display:none;"></div>
                        </div>
                    </div>
                        <div class="form-group">
                            <label class="col-md-12">Kategori Konten</label>
                            <div class="col-md-12">
                                <select name="c_cat_id" class="form-control">
                                    <option disabled selected>Pilih Kategori Konten</option>
                                    @foreach($kategori as $kat)
                                        <option value="{{ $kat->c_cat_id }}">{{ $kat->c_cat_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Penulis Konten</label>
                            <div class="col-md-12">
                                <select name="user_id" class="form-control">
                                    <option disabled selected>Pilih Penulis Konten</option>
                                    @foreach($user as $use)
                                        <option value="{{ $use->id }}">{{ $use->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Tag Konten</label>
                            <div class="col-md-12">
                                <input type="text" name="content_tags" class="form-control" placeholder="Tags Konten...">
                            </div>
                        </div>
                </div>
                    <input type="hidden" name="gambar" id="preview-crop-image-input">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-12">Nama Konten</label>
                            <div class="col-md-12">
                                <input type="text" name="content_name" class="form-control" placeholder="Nama Konten..." required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Tipe Konten</label>
                            <div class="col-md-12">
                                <select name="type" id="tipekonten" class="form-control" onchange="tipeKonten()">
                                    <option disabled selected>Pilih Tipe Konten</option>
                                    <option value="blog">Blog</option>
                                    <option value="slider">Slider</option>
                                    <option value="profil">Profil</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Deskripsi Singkat Konten</label>
                            <div class="col-md-12">
                                <input type="text" name="content_sortdesc" class="form-control" placeholder="Deskripsi Singkat Konten..." required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Deskripsi Service</label>
                            <div class="col-md-12">
                                <textarea name="content_desc" class="form-control" id="deskripsi" rows="4" placeholder="Deskripsi Service..."></textarea>
                            </div>
                        </div>
                        <div class="form-group" style="float: right">
                            <label class="col-md-12" style="text-align: right">Status Konten</label>
                            <div class="col-md-12" style="text-align: right">
                                <label id="statusnon">Draft</label>
                                <input type="checkbox" checked name="content_status" id="status" class="js-switch" data-color="#13dafe" data-size="small" onclick="displaystatus()" />
                                <label id="statusaktif">Publish</label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Konten</button>
                </form>
            </div>
        </div>
    </div>

</div>

<div class="modal fade" id="gantigambar" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Crop Gambar Konten</h4> </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-5">
                            <div class="form-group">
                                <label class="control-label">Input Gambar:</label>
                                <input type="file" class="dropify" id="imagekonten" required>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-12">Crop Gambar</label>
                                <div class="col-md-12">
                                    <div id="upload-demo-konten"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-block upload-image-konten" id="upload-image-konten">Crop Gambar</button>
            </div>
        </div>
    </div>
</div>

@include('admin.template.footer')
