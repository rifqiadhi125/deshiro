@include('admin.template.header')
@include('admin.template.navbar')
<meta name="csrf-token" content="{{ csrf_token() }}">


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Tambah Konten</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('admin/content') }}">Konten</a></li>
                    <li class="active">Tambah Konten</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- .row -->

        <div class="white-box">
            <h3 class="box-title m-b-0">Form Tambah Konten</h3>
            <p class="text-muted m-b-30 font-13">Tambah Konten</p>
            <form class="form-horizontal" method="POST" action="{{ url('admin/content/update', $content->content_id) }}" enctype="multipart/form-data">
                @csrf
                @method('patch')
                <div class="row">
                    <div class="col-md-4">
                        <div class="form-group" id="imagetambahfull">
                            <label class="col-md-12">Gambar Konten</label>
                            <div class="col-md-12">
                                <img src="{{ $content->content_image }}" alt="" style="width:100%;max-width:100%; margin-bottom: 10px">
                                <a href="javascript:void(0)" class="btn btn-info btn-block"  data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;Ganti Gambar Konten</a>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12" id="label-preview-crop-image" style="display:none;">Gambar Hasil Crop</label>
                            <div class="col-md-12">
                                <div id="preview-crop-image" style="background:#fff; width:300px; height:300px; display:none;"></div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Kategori Konten</label>
                            <div class="col-md-12">
                                <select name="c_cat_id" class="form-control">
                                    <option disabled selected>Pilih Kategori Konten</option>
                                    @foreach($kategori as $kat)
                                        <option value="{{ $kat->c_cat_id }}"
                                            @if($content->c_cat_id == $kat->c_cat_id)
                                            selected
                                            @endif
                                        >{{ $kat->c_cat_name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Penulis Konten</label>
                            <div class="col-md-12">
                                <select name="user_id" class="form-control">
                                    <option disabled selected>Pilih Penulis Konten</option>
                                    @foreach($user as $use)
                                        <option value="{{ $use->id }}"
                                            @if($content->user_id == $use->id)
                                            selected
                                            @endif
                                        >{{ $use->name }}</option>
                                    @endforeach
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Tag Konten</label>
                            <div class="col-md-12">
                                <input type="text" name="content_tags" class="form-control" placeholder="Tags Konten..." value="{{ $content->content_tags }}">
                            </div>
                        </div>
                    </div>
                    <input type="hidden" name="gambar" id="preview-crop-image-input">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-12">Nama Konten</label>
                            <div class="col-md-12">
                                <input type="text" name="content_name" class="form-control" placeholder="Nama Konten..." value="{{ $content->content_name }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Tipe Konten</label>
                            <div class="col-md-12">
                                <select name="type" id="tipekonten" class="form-control" onchange="tipeKonten()">
                                    <option disabled selected>Pilih Tipe Konten</option>
                                    @if($content->content_type == "blog")
                                    <option value="blog" selected>Blog</option>
                                    <option value="slider">Slider</option>
                                    @else
                                    <option value="blog">Blog</option>
                                    <option value="slider" selected>Slider</option>
                                    @endif
                                </select>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Deskripsi Singkat Konten</label>
                            <div class="col-md-12">
                                <input type="text" name="content_sortdesc" class="form-control" placeholder="Deskripsi Singkat Konten..." value="{{ $content->content_sortdesc }}" required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Deskripsi Service</label>
                            <div class="col-md-12">
                                <textarea name="content_desc" class="form-control" id="deskripsi" rows="4" placeholder="Deskripsi Service...">{{ $content->content_desc }}</textarea>
                            </div>
                        </div>
                        <div class="form-group" style="float: right">
                            <label class="col-md-12" style="text-align: right">Status Konten</label>
                            <div class="col-md-12" style="text-align: right">
                                <label id="statusnon">Draft</label>
                                <input type="checkbox" @if($content->content_status === 'publish') checked @else @endif name="content_status" id="status" class="js-switch" data-color="#13dafe" data-size="small" onclick="displaystatus()" />
                                <label id="statusaktif">Publish</label>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Konten</button>
            </form>
        </div>
    </div>
</div>

</div>

<div class="modal fade bs-example-modal-lg" id="gantigambar" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Ganti Gambar Konten</h4> </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Input Gambar:</label>
                    <input type="file" class="form-control" id="imageeditkonten" required>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-12">Crop Gambar</label>
                                <div class="col-md-12">
                                    <div id="upload-demo-konten"></div>
                                    <button class="btn btn-primary btn-block upload-image-edit-konten" id="upload-image-edit-konten" style="margin-top: 10px">Crop Gambar</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label class="col-md-6"><br>Hasil Crop</label>
                                <div class="col-md-6">
                                    <div id="preview-crop-image-konten" style="background:#9d9d9d; width:auto; height:300px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <form method="POST" action="{{ url('admin/content/update-gambar', $content->content_id) }}">
                @csrf
                @method('PATCH')
                <input type="hidden" name="gambar" id="preview-crop-image-input-edit">
                <div class="modal-footer">
                    <button id="submitupdategambar" class="btn btn-primary">Simpan</button>
            </form>

        </div>
    </div>
</div>
</div>

@include('admin.template.footer')
