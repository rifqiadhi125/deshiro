@include('admin.template.header')
@include('admin.template.navbar')

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Edit Customer</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="{{ url('admin/customer') }}">Customer</a></li>
                            <li class="active">Edit Customer</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                	<form class="form-horizontal" method="POST" action="{{ url('admin/customer/update', $customer->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('patch')
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Edit Customer</h3>
                            <p class="text-muted m-b-30 font-13">Edit Customer</p>
                                        <div class="form-group">
                                            <label class="col-md-12">Nama Customer</label>
                                            <div class="col-md-12">
                                                <input type="text" name="nama" class="form-control" placeholder="Nama Customer..." value="{{ $customer->nama }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Gender Customer</label>
                                            <div class="col-md-12">
                                                <select name="gender" class="form-control" required>
                                                    @if($customer->gender == 'L')
                                                    <option value="L" selected>Laki-laki</option>
                                                    <option value="P">Perempuan</option>
                                                    @else
                                                    <option value="L">Laki-laki</option>
                                                    <option value="P" selected>Perempuan</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Alamat Customer</label>
                                            <div class="col-md-12">
                                                <textarea name="alamat" class="form-control" cols="10" rows="5" placeholder="Alamat Customer..." required>{{ $customer->alamat }}</textarea>
                                            </div>
                                        </div>
                                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Customer</button>	
                        </div>
                        </div>
                        </div>
                	</form>
            </div>
@include('admin.template.footer')