@include('admin.template.header')
@include('admin.template.navbar')
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Tag</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                            <li class="active">Tag</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- ============================================================== -->
                <!-- Other sales widgets -->
                <!-- ============================================================== -->
            
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('success') }}
                </div>
            @endif    

            <div class="row">
                <div class="col-md-4">
                    @if(isset($edittag))
                    <div class="panel">
                        <div class="panel-heading">TAMBAH TAG
                            <div class="pull-right">
                                <a href="#" data-perform="panel-collapse"><i class="ti-plus"></i></a>
                            </div>
                        </div>
                        <div class="panel-wrapper collapse">
                            <form class="form-horizontal" method="POST" action="{{ url('admin/tag/tambah/simpan') }}" style="padding: 20px; padding-top: 10px">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Nama Tag</label>
                                <div class="col-md-12">
                                    <input type="text" name="nama_tag" class="form-control" placeholder="Nama Tag..." required>
                                </div>
                            </div>
                            <button class="btn btn-info btn-block">Simpan Tag</button>
                            </form>
                        </div>
                    </div>
                    <div class="panel">
                        <div class="panel-heading">EDIT TAG
                            <div class="pull-right">
                                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-wrapper">
                            <form class="form-horizontal" method="POST" action="{{ url('admin/tag/update', $edittag->id_tag) }}" style="padding: 20px; padding-top: 10px">
                            @csrf
                            @method('patch')
                            <div class="form-group">
                                <label class="col-md-12">Nama Tag</label>
                                <div class="col-md-12">
                                    <input type="text" name="nama_tag" class="form-control" value="{{ $edittag->nama_tag }}" required>
                                </div>
                            </div>
                            <button class="btn btn-info btn-block">Update Tag</button>
                            </form>
                        </div>
                    </div>
                    @else
                    <div class="panel">
                        <div class="panel-heading">TAMBAH TAG
                            <div class="pull-right">
                                <a href="#" data-perform="panel-collapse"><i class="ti-minus"></i></a>
                            </div>
                        </div>
                        <div class="panel-wrapper">
                            <form class="form-horizontal" method="POST" action="{{ url('admin/tag/tambah/simpan') }}" style="padding: 20px; padding-top: 10px">
                            @csrf
                            <div class="form-group">
                                <label class="col-md-12">Nama Tag</label>
                                <div class="col-md-12">
                                    <input type="text" name="nama_tag" class="form-control" placeholder="Nama Tag..." required>
                                </div>
                            </div>
                            <button class="btn btn-info btn-block">Simpan Tag</button>
                            </form>
                        </div>
                    </div>
                    @endif
                </div>
                <div class="col-md-8">
                    <div class="panel">
                        <div class="panel-heading">MANAJEMEN TAG</div>
                        <div class="table-responsive">
                            <table class="table table-hover manage-u-table">
                                <thead>
                                    <tr>
                                        <th width="70" class="text-center">#</th>
                                        <th>NAMA TAG</th>
                                        <th width="150" class="text-center">AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($tag as $tags => $ta)
                                    <tr>
                                        <td class="text-center">{{ $tags + $tag->firstitem() }}</td>
                                        <td><span class="font-medium">{{ $ta->nama_tag }}</span></td>
                                        <td class="text-center">
                                            <form action="{{ url('admin/tag/hapus', $ta->id_tag) }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <a href="{{ url('admin/tag/edit', $ta->id_tag) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                            <button type="submit" class="btn btn-sm btn-rounded btn-danger">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="col-md-12">
                                <div style="float: right">{{ $tag->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
@include('admin.template.footer')