@include('admin.template.header')
@include('admin.template.navbar')
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Service</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                    <li class="active">Service</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>

        <div class="panel">
            <div class="panel-heading">MANAJEMEN SERVICE
                <a class="btn btn-info btn-sm btn-rounded float-right" style="float: right;" href="{{ url('admin/service/tambah') }}"><i class="fa fa-plus"></i>&nbsp;TAMBAH SERVICE</a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover manage-u-table">
                    <thead>
                    <tr>
                        <th width="70" class="text-center">#</th>
                        <th width="100">PIC</th>
                        <th>JUDUL</th>
                        <th>STATUS</th>
                        <th width="150" class="text-center">AKSI</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($service as $services => $ser)
                        <tr>
                            <td class="text-center">{{ $services + $service->firstitem() }}</td>
                            <td><img src="{{ $ser->service_images }}" alt="" style="max-width:100%"></td>
                            <td><span class="font-medium">{{ $ser->service_title }}</span></td>
                            <td><span class="font-medium">
                                    @if($ser->service_status == '1')
                                        <span class="font-medium">Aktif</span>
                                    @else
                                        <span class="font-medium">Tidak Aktif</span>
                                    @endif
                                </span></td>
                            <td class="text-center">
                                <a href="{{ url('admin/service/edit', $ser->service_id) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                <a href="{{ url('admin/service/hapus', $ser->service_id) }}" class="btn btn-sm btn-rounded btn-danger delete-confirm">Hapus</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>

                <div class="col-md-12">
                    <div style="float: right">{{ $service->links() }}</div>
                </div>
            </div>
        </div>
    </div>
@include('admin.template.footer')
