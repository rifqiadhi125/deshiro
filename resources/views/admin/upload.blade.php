<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no, user-scalable=no">
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
    <title>{{ config('app.name', 'Laravel') }} - @yield('title')</title>
    <link rel="stylesheet" href="{{ asset('assets/imgareaselect/distfiles/css/imgareaselect-default.css') }}" />
</head>
<body>

@if(session('success'))
    <div class="alert alert-success">{{session('success')}}</div>
@endif
<div class="container mt-5">
    <form action="{{ url('image') }}" method="post" enctype="multipart/form-data">
        <input type="text">
        <div class="form-group">
            <label for="exampleInputImage">Image:</label>
            <input type="file" name="profile_image" id="exampleInputImage" class="image" required>
            <input type="hidden" name="x1" value="" />
            <input type="hidden" name="y1" value="" />
            <input type="hidden" name="w" value="" />
            <input type="hidden" name="h" value="" />
        </div>
        {{ csrf_field() }}
        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
    <div class="row mt-5">
        <p><img id="previewimage" style="display:none;"/></p>
        @if(session('path'))
            <img src="{{ session('path') }}" />
        @endif
    </div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
<script src="{{ asset('assets/imgareaselect/jquery.imgareaselect.min.js') }}"></script>
<script>
    jQuery(function($) {
        var p = $("#previewimage");

        $("body").on("change", ".image", function(){
            var imageReader = new FileReader();
            imageReader.readAsDataURL(document.querySelector(".image").files[0]);

            imageReader.onload = function (oFREvent) {
                p.attr('src', oFREvent.target.result).fadeIn();
            };
        });

        $('#previewimage').imgAreaSelect({
            onSelectEnd: function (img, selection) {
                $('input[name="x1"]').val(selection.x1);
                $('input[name="y1"]').val(selection.y1);
                $('input[name="w"]').val(selection.width);
                $('input[name="h"]').val(selection.height);
            }
        });
    });
</script>
</body>
</html>
