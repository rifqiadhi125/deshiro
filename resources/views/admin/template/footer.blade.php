<!-- /.container-fluid -->
            <footer class="footer text-center"> 2020 &copy; {{ $setting[0]->setting_value }}</footer>
        </div>
        <!-- /#page-wrapper -->
    </div>
    <!-- /#wrapper -->

    <!-- jQuery -->



    <script src="{{ asset('assets/plugins/bower_components/jquery/dist/jquery.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/switchery/dist/switchery.min.js') }}"></script>
    <script>

      jQuery(document).ready(function () {
                // Switchery
                var elems = Array.prototype.slice.call(document.querySelectorAll('.js-switch'));
                $('.js-switch').each(function () {
                    new Switchery($(this)[0], $(this).data());
                });
      });
    </script>
    <script src="{{ asset('assets/plugins/bower_components/custom-select/custom-select.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/dropify/dist/js/dropify.min.js') }}"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/sweetalert/1.1.3/sweetalert.min.js"></script>
    <script>
        $(document).ready(function () {
            // Basic
            $('.dropify').dropify();
        });
    </script>

    <!-- Bootstrap Core JavaScript -->
    <script src="{{ asset('assets/bootstrap/dist/js/bootstrap.min.js') }}"></script>
    <!-- Menu Plugin JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/sidebar-nav/dist/sidebar-nav.min.js') }}"></script>
    <!--Counter js -->
    <script src="{{ asset('assets/plugins/bower_components/waypoints/lib/jquery.waypoints.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/counterup/jquery.counterup.min.js') }}"></script>
    <!--slimscroll JavaScript -->
    <script src="{{ asset('assets/js/jquery.slimscroll.js') }}"></script>

    <!--Wave Effects -->
    <script src="{{ asset('assets/js/waves.js') }}"></script>
    <!-- Vector map JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/vectormap/jquery-jvectormap-2.0.2.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/vectormap/jquery-jvectormap-world-mill-en.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/vectormap/jquery-jvectormap-in-mill.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/vectormap/jquery-jvectormap-us-aea-en.js') }}"></script>
    <!-- chartist chart -->
    <script src="{{ asset('assets/plugins/bower_components/chartist-js/dist/chartist.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/chartist-plugin-tooltip-master/dist/chartist-plugin-tooltip.min.js') }}"></script>
    <!-- sparkline chart JavaScript -->
    <script src="{{ asset('assets/plugins/bower_components/jquery-sparkline/jquery.sparkline.min.js') }}"></script>
    <script src="{{ asset('assets/plugins/bower_components/jquery-sparkline/jquery.charts-sparkline.js') }}"></script>
    <!-- Custom Theme JavaScript -->
    <script src="{{ asset('assets/js/custom.min.js') }}"></script>
    <script src="{{ asset('assets/js/dashboard3.js') }}"></script>
    <!--Style Switcher -->
    <script src="{{ asset('assets/plugins/bower_components/styleswitcher/jQuery.style.switcher.js') }}"></script>
    <script src="{{ asset('assets/summernote/summernote-bs4.js') }}"></script>
    <!-- Sweet-Alert  -->
    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script type="text/javascript">
        $(document).ready(function () {
          $('.delete-confirm').on('click', function (event) {
              event.preventDefault();
              const url = $(this).attr('href');
              swal({
                  title: 'Anda yakin ingin menghapus data ini?',
                  text: 'Data akan hilang permanen!',
                  icon: 'warning',
                  buttons: ["Batal", "Yakin!"],
              }).then(function(value) {
                  if (value) {
                      window.location.href = url;
                  }
              });
          });
        });
        $(function () {
            "use strict";
            $("#main-wrapper").AdminSettings({
                Theme: false, // this can be true or false ( true means dark and false means light ),
                Layout: 'vertical',
                LogoBg: 'skin6', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
                NavbarBg: 'skin6', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
                SidebarType: 'full', // You can change it full / mini-sidebar / iconbar / overlay
                SidebarColor: 'skin6', // You can change the Value to be skin1/skin2/skin3/skin4/skin5/skin6
                SidebarPosition: true, // it can be true / false ( true means Fixed and false means absolute )
                HeaderPosition: false, // it can be true / false ( true means Fixed and false means absolute )
                BoxedLayout: false, // it can be true / false ( true means Boxed and false means Fluid )
            });
        });

    </script>
    @if(Session::has('success'))
    <script type="text/javascript">
      $(document).ready(function(){
        swal("Berhasil!", "{{ Session('success') }}", "success");
      });
    </script>
    @endif
    <script type="text/javascript">
        jQuery(document).ready(function(){
        $(".select2").select2();
        $('.selectpicker').selectpicker();

        });
    </script>
    <script>
    function display240p() {
      var checkBox = document.getElementById("240p");
      var div = document.getElementById("240pbox");
      if (checkBox.checked == true){
        div.style.display = "block";
      } else {
         div.style.display = "none";
      }
    }
    function display360p() {
      var checkBox = document.getElementById("360p");
      var div = document.getElementById("360pbox");
      if (checkBox.checked == true){
        div.style.display = "block";
      } else {
         div.style.display = "none";
      }
    }
    function display480p() {
      var checkBox = document.getElementById("480p");
      var div = document.getElementById("480pbox");
      if (checkBox.checked == true){
        div.style.display = "block";
      } else {
         div.style.display = "none";
      }
    }
    function display720p() {
      var checkBox = document.getElementById("720p");
      var div = document.getElementById("720pbox");
      if (checkBox.checked == true){
        div.style.display = "block";
      } else {
         div.style.display = "none";
      }
    }
    function displaystatus() {
      var checkBox = document.getElementById("status");
      var labelaktif = document.getElementById("statusaktif");
      var labelnon = document.getElementById("statusnon");
      if (checkBox.checked == true){
        labelaktif.style.display = "block";
        labelnon.style.display = "none";
      } else {
        labelnon.style.display = "block";
        labelaktif.style.display = "none";
      }
    }
    </script>
    <script src="{{ asset('assets/ckeditor-resize/ckeditor.js') }}"></script>
    <script>
      var deskripsi = document.getElementById("deskripsi");
        CKEDITOR.replace(deskripsi,{
          filebrowserUploadUrl: "{{route('post.image', ['_token' => csrf_token() ])}}",
          filebrowserUploadMethod: 'form',
      });
      CKEDITOR.config.allowedContent = true;
    </script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/croppie/2.6.2/croppie.js"></script>
    <script type="text/javascript">

    $.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
    });

    var resizeservice = $('#upload-demo-service').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 710,
            height: 350,
            type: 'square' //square
        },
        boundary: {
            width: 760,
            height: 400,
        }
    });

    var resizekonten = $('#upload-demo-konten').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 750,
            height: 420,
            type: 'square' //square
        },
        boundary: {
            width: 650,
            height: 350,
        }
    });

    var resizekategoriedit = $('#upload-demo-konten-edit').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 300,
            height: 250,
            type: 'square' //square
        },
        boundary: {
            width: 400,
            height: 300
        }
    });


    var resize = $('#upload-demo').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 750,
            height: 370,
            type: 'square' //square
        },
        boundary: {
            width: 800,
            height: 400
        }
    });

    var resizeedit = $('#upload-demo-edit').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 750,
            height: 370,
            type: 'square' //square
        },
        boundary: {
            width: 800,
            height: 400
        }
    });

    var resizeedit2 = $('#upload-demo-edit-2').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 250,
            height: 150,
            type: 'square' //square
        },
        boundary: {
            width: 300,
            height: 250
        }
    });


    $('#imagetambah').on('change', function () {
      var reader = new FileReader();
        reader.onload = function (e) {
          resize.croppie('bind',{
            url: e.target.result
          }).then(function(){
            console.log('jQuery bind complete');
          });
        }
        reader.readAsDataURL(this.files[0]);
    });

    $('#imagekonten').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            resizekonten.croppie('bind',{
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });

    $('#imageeditkonten').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            resizekonten.croppie('bind',{
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });

    $('#imageeditkategori').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            resizekategoriedit.croppie('bind',{
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });

    $('#imageedit2').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            resizeedit2.croppie('bind',{
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });



    $('.upload-image').on('click', function (ev) {
      resize.croppie('result', {
        type: 'canvas',
        size: 'viewport'
      }).then(function (img) {
        $.ajax({
          url: "{{route('croppie.upload-image')}}",
          type: "POST",
          data: {"image":img},
          success: function (data) {
            html = '<img src="' + img + '" style="width:100%" />';
            $("#gantigambar").modal('hide');
            $("#label-preview-crop-image").show();
            $("#preview-crop-image").show();
            $("#imagetambahfull").hide();
            $("#preview-crop-image").html(html);
            $("#preview-crop-image-input").val(img);
          }
        });
      });
    });

    $('.upload-image-konten').on('click', function (ev) {
        resizekonten.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            $.ajax({
                url: "{{route('croppie.upload-image')}}",
                type: "POST",
                data: {"image":img},
                success: function (data) {
                    html = '<img src="' + img + '" style="width:100%" /><br>';
                    $("#gantigambar").modal('hide');
                    $("#label-preview-crop-image").show();
                    $("#preview-crop-image").show();
                    $("#imagetambahfull").hide();
                    $("#preview-crop-image").html(html);
                    $("#preview-crop-image-input").val(img);
                }
            });
        });
    });

    $('.upload-image-service').on('click', function (ev) {
        resizeservice.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            $.ajax({
                url: "{{route('croppie.upload-image')}}",
                type: "POST",
                data: {"image":img},
                success: function (data) {
                    html = '<img src="' + img + '" style="width:100%" />';
                    $("#gantigambar").modal('hide');
                    $("#label-preview-crop-image").show();
                    $("#preview-crop-image").show();
                    $("#imagetambahfull").hide();
                    $("#preview-crop-image").html(html);
                    $("#preview-crop-image-input").val(img);
                }
            });
        });
    });


    $('.upload-image-service-edit').on('click', function (ev) {
        resizeservice.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            $.ajax({
                url: "{{route('croppie.upload-image')}}",
                type: "POST",
                data: {"image":img},
                success: function (data) {
                    html = '<img src="' + img + '" style="width:100%" />';
                    $("#preview-crop-image").html(html);
                    $("#preview-crop-image-input").val(img);
                }
            });
        });
    });



    $('.upload-image-edit').on('click', function (ev) {
        resizeedit.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            $.ajax({
                url: "{{route('croppie.upload-image')}}",
                type: "POST",
                data: {"image":img},
                success: function (data) {
                    html = '<img src="' + img + '" style="width:100%" />';
                    $("#preview-crop-image-edit").html(html);
                    $("#preview-crop-image-input-edit").val(img);
                }
            });
        });
    });

    $('.upload-image-edit-konten').on('click', function (ev) {
        resizekonten.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            $.ajax({
                url: "{{route('croppie.upload-image')}}",
                type: "POST",
                data: {"image":img},
                success: function (data) {
                    html = '<img src="' + img + '" style="width:100%" />';
                    $("#preview-crop-image-konten").html(html);
                    $("#preview-crop-image-input-edit").val(img);
                }
            });
        });
    });

    $('.upload-image-edit-kategori').on('click', function (ev) {
        resizekategoriedit.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            $.ajax({
                url: "{{route('croppie.upload-image')}}",
                type: "POST",
                data: {"image":img},
                success: function (data) {
                    html = '<img src="' + img + '" style="width:100%" />';
                    $("#preview-crop-image-konten").html(html);
                    $("#preview-crop-image-input-edit").val(img);
                }
            });
        });
    });

    $('.upload-image-edit-2').on('click', function (ev) {
        resizeedit2.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            $.ajax({
                url: "{{route('croppie.upload-image')}}",
                type: "POST",
                data: {"image":img},
                success: function (data) {
                    html = '<img src="' + img + '" style="width:100%" />';
                    $("#preview-crop-image-edit-2").html(html);
                    $("#preview-crop-image-input-edit-2").val(img);
                }
            });
        });
    });

    $('#imageservice').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            resizeservice.croppie('bind',{
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });


    </script>
    <script type="text/javascript">

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });

    var resize2 = $('#upload-demo-2').croppie({
        enableExif: true,
        enableOrientation: true,
        viewport: { // Default { width: 100, height: 100, type: 'square' }
            width: 250,
            height: 150,
            type: 'square' //square
        },
        boundary: {
            width: 300,
            height: 250
        }
    });




    $('#imagetambah-2').on('change', function () {
        var reader = new FileReader();
        reader.onload = function (e) {
            resize2.croppie('bind',{
                url: e.target.result
            }).then(function(){
                console.log('jQuery bind complete');
            });
        }
        reader.readAsDataURL(this.files[0]);
    });



    $('.upload-image-2').on('click', function (ev) {
        resize2.croppie('result', {
            type: 'canvas',
            size: 'viewport'
        }).then(function (img) {
            $.ajax({
                url: "{{route('croppie.upload-image')}}",
                type: "POST",
                data: {"image":img},
                success: function (data) {
                    html = '<img src="' + img + '" style="width:100%" />';
                    $("#gantigambar-2").modal('hide');
                    $("#label-preview-crop-image-2").show();
                    $("#preview-crop-image-2").show();
                    $("#imagetambahfull-2").hide();
                    $("#preview-crop-image-2").html(html);
                    $("#preview-crop-image-input-2").val(img);
                }
            });
        });
    });


</script>



</body>

</html>
