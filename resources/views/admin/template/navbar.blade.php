<body class="fix-header">
@include('sweet::alert')
<!-- ============================================================== -->
    <!-- Wrapper -->
    <!-- ============================================================== -->
    <div id="wrapper">
        <!-- ============================================================== -->
        <!-- Topbar header - style you can find in pages.scss -->
        <!-- ============================================================== -->
        <nav class="navbar navbar-default navbar-static-top m-b-0">
            <div class="navbar-header">
                <ul class="nav navbar-top-links navbar-left">
                    <li><a href="javascript:void(0)" class="open-close waves-effect waves-light visible-xs"><i class="ti-close ti-menu"></i></a></li>
                </ul>
                <div class="top-left-part">
                    <!-- Logo -->
                    <a class="logo" href="{{ url('') }}"><img src="{{ asset('public/assets/images/logo.png') }}" style="width: 150px"></a>

                </div>
                <!-- /Logo -->
                <ul class="nav navbar-top-links navbar-right pull-right">
                    <li class="dropdown">
                        <a class="dropdown-toggle profile-pic" data-toggle="dropdown" href="#"> <img src="{{ asset('assets/uploads/foto_profile/'.Auth::user()->foto_profile) }}" alt="user-img" width="36" class="img-circle"><b class="hidden-xs">{{ Auth::user()->name }}</b><span class="caret"></span> </a>
                        <ul class="dropdown-menu dropdown-user animated flipInY">
                            <li>
                                <div class="dw-user-box">
                                    <div class="u-img"><img src="{{ asset('assets/uploads/foto_profile/'.Auth::user()->foto_profile) }}" alt="user" /></div>
                                    <div class="u-text"><h4>{{ Auth::user()->name }}</h4><p class="text-muted">{{ Auth::user()->email }}</p></div>
                                </div>
                            </li>
                            <li role="separator" class="divider"></li>
                            <li><a href="{{ url('logout') }}"><i class="fa fa-power-off"></i> Logout</a></li>
                        </ul>
                        <!-- /.dropdown-user -->
                    </li>

                    <!-- /.dropdown -->
                </ul>
            </div>
            <!-- /.navbar-header -->
            <!-- /.navbar-top-links -->
            <!-- /.navbar-static-side -->
        </nav>
        <!-- End Top Navigation -->
        <!-- ============================================================== -->
        <!-- Left Sidebar - style you can find in sidebar.scss  -->
        <!-- ============================================================== -->
        <div class="navbar-default sidebar" role="navigation">
            <div class="sidebar-nav slimscrollsidebar">
                <div class="sidebar-head">
                    <h3><span class="fa-fw open-close"><i class="ti-menu hidden-xs"></i><i class="ti-close visible-xs"></i></span> <span class="hide-menu">Menu</span></h3> </div>
                <ul class="nav" id="side-menu">
                    <li> <a href="{{ url('admin/dashboard') }}" class="waves-effect"><i class="mdi mdi-home fa-fw" data-icon="v"></i> <span class="hide-menu"> Dashboard</span></a>
                    </li>
                    <li class="devider"></li>
                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-content-copy fa-fw"></i> <span class="hide-menu">Manajemen Konten<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li><a href="{{ url('admin/content-category') }}" class="sidebar-link"><i class="mdi mdi-minus fa-fw"></i> <span class="hide-menu">Kategori Konten</span></a>
                            <li><a href="{{ url('admin/content') }}" class="waves-effect"><i class="mdi mdi-minus fa-fw"></i> <span class="hide-menu">Daftar Konten</span></a>
                            <li><a href="{{ url('admin/slider') }}" class="waves-effect"><i class="mdi mdi-minus fa-fw"></i> <span class="hide-menu">Daftar Slider</span></a>
                        </ul>
                    </li>
                    <li class="devider"></li>
                    <li> <a href="{{ url('admin/service') }}" class="waves-effect"><i class="mdi mdi-heart-outline fa-fw"></i> <span class="hide-menu">Service</span></a>
                    <li class="devider"></li>
                    <li> <a href="{{ url('admin/portfolio') }}" class="waves-effect"><i class="mdi mdi-human-greeting fa-fw"></i> <span class="hide-menu">Mitra</span></a></li>
                    <li class="devider"></li>
                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-settings fa-fw"></i> <span class="hide-menu">Setting<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('admin/setting-general') }}"><i class="mdi mdi-minus fa-fw"></i><span class="hide-menu">Setting General</span></a> </li>
                            <li> <a href="{{ url('admin/setting-about') }}"><i class="mdi mdi-minus fa-fw"></i><span class="hide-menu">Setting Tentang</span></a> </li>
                            <li> <a href="{{ url('admin/setting-privacy') }}"><i class="mdi mdi-minus fa-fw"></i><span class="hide-menu">Setting Privacy</span></a> </li>
                        </ul>
                    </li>
                    <li class="devider"></li>
                    <li> <a href="javascript:void(0)" class="waves-effect"><i class="mdi mdi-account-settings-variant fa-fw"></i> <span class="hide-menu">Manajemen User<span class="fa arrow"></span></span></a>
                        <ul class="nav nav-second-level">
                            <li> <a href="{{ url('admin/role-user') }}"><i data-icon="7" class="mdi mdi-minus fa-fw"></i><span class="hide-menu">Role User</span></a> </li>
                            <li> <a href="{{ url('admin/user') }}"><i class="mdi mdi-minus fa-fw"></i><span class="hide-menu">User</span></a> </li>
                        </ul>
                    </li>
                    <li class="devider"></li>
                </ul>
            </div>
        </div>
        <!-- ============================================================== -->
        <!-- End Left Sidebar -->
        <!-- ============================================================== -->

