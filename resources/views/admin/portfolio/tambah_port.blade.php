@include('admin.template.header')
@include('admin.template.navbar')
<meta name="csrf-token" content="{{ csrf_token() }}">


<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Edit Portfolio</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="">Dashboard</a></li>
                    <li><a href="{{ url('admin/user') }}">User</a></li>
                    <li class="active">Edit Portfolio</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- .row -->

        <div class="white-box">
            <h3 class="box-title m-b-0">Form Tambah Portfolio</h3>
            <p class="text-muted m-b-30 font-13">Tambah Portfolio</p>
            @csrf
            <div class="row">
                <div class="col-md-4" id="imagetambahfull">
                    <div class="form-group">
                        <label class="col-md-12">Gambar Portfolio</label>
                        <div class="col-md-12">
                            <button class="btn btn-info btn-block" style="margin-top:10px" data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;Input Gambar Portfolio</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-4">
                    <div class="form-group">
                        <label class="col-md-12" id="label-preview-crop-image" style="display:none;">Gambar Hasil Crop</label>
                        <div class="col-md-12">
                            <div id="preview-crop-image" style="background:#fff; width:300px; height:300px; display:none;"></div>
                        </div>
                    </div>
                </div>
                <form class="form-horizontal" method="POST" action="{{ url('admin/tambah_portfolio/simpan') }}" enctype="multipart/form-data">
                    @csrf
                    <input type="hidden" name="gambar" id="preview-crop-image-input">
                    <div class="col-md-8">
                        <div class="form-group">
                            <label class="col-md-12">Judul Portfolio</label>
                            <div class="col-md-12">
                                <input type="text" name="judulPortfolio" class="form-control" placeholder="Judul Service..." required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Deskipsi Singkat</label>
                            <div class="col-md-12">
                                <input type="text" name="Deskripsisingakt" class="form-control" placeholder="deskipsi Singk..." required>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="col-md-12">Deskripsi Portfolio</label>
                            <div class="col-md-12">
                                <textarea name="DeskripsiPortfolio" class="form-control" id="deskripsi" rows="4" placeholder="Deskripsi Service..."></textarea>
                            </div>
                        </div>
                        <div class="form-group" style="float: right">
                            <label class="col-md-12" style="text-align: right">Status Portfolio</label>
                            <div class="col-md-12" style="text-align: right">
                                <select name="status">
                                    <option value="aktif">aktif</option>
                                    <option value="non-aktif">non-aktif</option>
                                </select>
                            </div>
                        </div>
                    </div>
                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Portfolio</button>
            </div>
        </div>
    </div>
</div>
</form>
</div>

    <div class="modal fade" id="gantigambar" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Crop Gambar Slider</h4> </div>
            <div class="modal-body">
                <div class="form-group">
                    <div class="form-group">
                        <label class="control-label">Input Gambar:</label>
                        <input type="file" class="form-control" id="imagetambah" required>
                    </div>
                    <div class="form-group">
                        <label class="col-md-12">Crop Gambar</label>
                        <div class="col-md-12">
                            <div id="upload-demo"></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="modal-footer">
                <button class="btn btn-primary btn-block upload-image" id="upload-image">Crop Gambar</button>
            </div>
        </div>
    </div>
</div>

@include('admin.template.footer')
