@include('admin.template.header')
@include('admin.template.navbar')
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Portfolio</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                    <li class="active">Portfolio</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- ============================================================== -->
        <!-- Other sales widgets -->
        <!-- ============================================================== -->

    <!-- @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('success') }}
                </div>
            @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('error') }}
                </div>
            @endif -->

        <div class="panel">
            <div class="panel-heading">MANAJEMEN PORTFOLIO
                <a class="btn btn-info btn-sm float-right" style="float: right;" href="{{ url('admin/tambah_portfolio/') }}"><i class="fa fa-plus"></i>&nbsp;TAMBAH PORTFOLIO</a>
            </div>
            <div class="table-responsive">
                <table class="table table-hover manage-u-table">
                    <thead>
                    <tr>
                        <th width="70" class="text-center">#</th>
                        <th width="50" class="text-center">GAMBAR</th>
                        <th>JUDUL</th>
                        <th class="text-center">STATUS</th>
                        <th>DESKIPSI</th>
                        <th width="150" class="text-center">AKSI</th>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dataPortfolio as $portfolio => $sli)
                        <tr>
                            <td class="text-center">{{ $portfolio + $dataPortfolio->firstitem() }}</td>
                            <td><img src="{{ $sli->portfolio_image }}" alt="" style="max-width:100%"></td>
                            <td><span class="font-medium">{{ $sli->portfolio_title }}</span></td>
                            <td class="text-center">
                                @if($sli->portfolio_status == 'aktif')
                                    <span class="font-medium">Aktif</span>
                                @else
                                    <span class="font-medium">Tidak Aktif</span>
                                @endif
                            </td>
                            <td><span class="font-medium">{{ strip_tags($sli->portfolio_sortdesc)}}</span></td>
                            <td class="text-center">
                                <a href="{{ url('admin/portfolio/edit', $sli->portfolio_id) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                <a href="{{ url('admin/portfolio/hapus', $sli->portfolio_id) }}" class="btn btn-sm btn-rounded btn-danger delete-confirm">Hapus</a>
                            </td>
                        </tr>
                    @endforeach
                    </tbody>
                </table>
                <div class="col-md-12">
                    <div style="float: right">{{ $dataPortfolio->links() }}</div>
                </div>
            </div>
        </div>
    </div>
@include('admin.template.footer')