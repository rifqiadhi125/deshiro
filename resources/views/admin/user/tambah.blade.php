@include('admin.template.header')
@include('admin.template.navbar')

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Tambah User</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="{{ url('admin/user') }}">User</a></li>
                            <li class="active">Tambah User</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
                	<form class="form-horizontal" method="POST" action="{{ url('admin/user/tambah/simpan') }}" enctype="multipart/form-data">
                	@csrf
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Tambah User</h3>
                            <p class="text-muted m-b-30 font-13">Tambah User</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-md-12">Foto Profil</label>
                                            <div class="col-md-12">
                                                <input type="file" class="dropify" name="foto_profile" data-height="275">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="col-md-12">Nama User</label>
                                            <div class="col-md-12">
                                                <input type="text" name="nama_user" class="form-control" placeholder="Nama User..." required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Email User</label>
                                            <div class="col-md-12">
                                                <input type="email" name="email" class="form-control" placeholder="Email User..." required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Password User</label>
                                            <div class="col-md-12">
                                                <input type="password" name="password" class="form-control" placeholder="Password User..." required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Role User</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="role">
                                                    @foreach($roleuser as $role)
                                                    <option value="{{ $role->id_role }}">{{ $role->nama_role }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan User</button>	
                                </div>
                            	</div>
                        </div>
                        	</div>
                	</form>
            </div>
@include('admin.template.footer')