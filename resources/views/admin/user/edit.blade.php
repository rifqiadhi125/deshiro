@include('admin.template.header')
@include('admin.template.navbar')

<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Edit User</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="{{ url('admin/user') }}">User</a></li>
                            <li class="active">Edit User</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('success') }}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('error') }}
                </div>
            @endif
                	<form class="form-horizontal" method="POST" action="{{ url('admin/user/update', $user->id) }}" enctype="multipart/form-data">
                    @csrf
                    @method('PATCH')
                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Edit User</h3>
                            <p class="text-muted m-b-30 font-13">Edit User</p>
                                <div class="row">
                                    <div class="col-md-3">
                                        <div class="form-group">
                                            <label class="col-md-12">Foto Profil</label>
                                            <div class="col-md-12">
                                                <input type="file" class="dropify" name="foto_profile" data-height="275" data-default-file="{{ asset('assets/uploads/foto_profile/'.$user->foto_profile ) }}">
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-9">
                                        <div class="form-group">
                                            <label class="col-md-12">Nama User</label>
                                            <div class="col-md-12">
                                                <input type="text" name="nama_user" class="form-control" placeholder="Nama User..." value="{{ $user->name }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Email User</label>
                                            <div class="col-md-12">
                                                <input type="email" name="email" class="form-control" placeholder="Email User..." value="{{ $user->email }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Password User</label>
                                            <div class="col-md-12">
                                                <a href="javascript:void(0)" class="btn btn-inverse btn-sm" data-toggle="modal" data-target="#gantipassword">Ganti Password</a>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Role User</label>
                                            <div class="col-md-12">
                                                <select class="form-control" name="role">
                                                    @foreach($roleuser as $role)
                                                    <option value="{{ $role->id_role }}"
                                                        @if($user->role == $role->id_role)
                                                        selected
                                                        @endif
                                                        >{{ $role->nama_role }}</option>
                                                    @endforeach
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan User</button>
                                </div>
                            	</div>
                        </div>
                        	</div>
                	</form>
                                        <div class="modal fade" id="gantipassword" tabindex="-1" role="dialog">
                                            <div class="modal-dialog" role="document">
                                                <div class="modal-content" >
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Form Ganti Password</h4> </div>
                                                    <div class="modal-body">
                                                        <form method="POST" action="{{ url('admin/user/update-password', $user->id) }}">
                                                        @csrf
                                                        @method('PATCH')
                                                            <div class="form-group">
                                                                <label class="control-label">Password Lama:</label>
                                                                <input type="password" class="form-control" name="password_lama" required>
                                                            </div>
                                                            <div class="form-group">
                                                                <label class="control-label">Password Baru:</label>
                                                                <input type="password" class="form-control" name="password_baru" required>
                                                            </div>
                                                    </div>
                                                    <div class="modal-footer">
                                                        <button  class="btn btn-primary">Simpan</button>
                                                        </form>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
            </div>
@include('admin.template.footer')
