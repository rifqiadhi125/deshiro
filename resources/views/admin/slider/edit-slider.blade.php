@include('admin.template.header')
@include('admin.template.navbar')
<meta name="csrf-token" content="{{ csrf_token() }}">


<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Edit Slider</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="{{ url('admin/user') }}">User</a></li>
                            <li class="active">Tambah User</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- .row -->

                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Edit Slider</h3>
                            <p class="text-muted m-b-30 font-13">Edit Slider</p>
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group">
                                            <img src="{{ $slider->slider_image }}" alt="" style="width:100%;max-width:100%">
                                            <button class="btn btn-info btn-block" style="margin-top:10px" data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;@if($slider->slider_image == null) Tambah @else Ganti @endif Gambar</button>
                                        </div>
                                    </div>
                                    <form class="form-horizontal" method="POST" action="{{ url('admin/slider/update', $slider->slider_id  ) }}" enctype="multipart/form-data">
                                    @csrf
                                    @method('patch')
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="col-md-12">Type Slider</label>
                                            <div class="col-md-12">
                                                <select name="type" id="tipeslider" class="form-control" onchange="tipeSlider()">
                                                    @if($slider->slider_type == "url")
                                                    <option value="url" selected>url</option>
                                                    <option value="text">text</option>
                                                    @else
                                                    <option value="url">url</option>
                                                    <option value="text" selected>text</option>
                                                    @endif
                                                </select>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12">Judul Slider</label>
                                            <div class="col-md-12">
                                                <input type="text" name="judul" class="form-control" placeholder="Judul Slider..." value="{{ $slider->slider_title }}" required>
                                            </div>
                                        </div>
                                        <div class="form-group" id="deskripsi-slider" style="@if($slider->slider_type == "url") display: none; @else display: block @endif">
                                            <label class="col-md-12">Deskripsi Slider</label>
                                            <div class="col-md-12">
                                                <textarea name="deskripsi" class="form-control" id="deskripsi" rows="4" placeholder="Deskripsi Slider...">{{ $slider->slider_shortdesc }}</textarea>
                                            </div>
                                        </div>
                                        <div class="form-group" id="url-slider" style="@if($slider->slider_type == "text") display: none; @else display: block @endif">
                                            <label class="col-md-12">Url Slider</label>
                                            <div class="col-md-12">
                                                <input type="text" name="url" class="form-control" placeholder="Url Slider..." value="{{ $slider->slider_url }}">
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            function tipeSlider(){
                                                var tipeslider = document.getElementById("tipeslider").value;

                                                if (tipeslider === "url") {
                                                    document.getElementById("deskripsi-slider").style.display = "none";
                                                    document.getElementById("url-slider").style.display = "block";
                                                } else if (tipeslider === "text") {
                                                    document.getElementById("deskripsi-slider").style.display = "block";
                                                    document.getElementById("url-slider").style.display = "none";
                                                } else {}
                                            }
                                        </script>
                                        <div class="form-group" style="float: right">
                                            <label class="col-md-12" style="text-align: right">Status Slider</label>
                                            <div class="col-md-12" style="text-align: right">
                                                <select name="status">
                                                    <option @if($slider->slider_status=="active")selected @endif value="active">aktif</option>
                                                    <option @if($slider->slider_status=="non-active")selected @endif value="non-active">non-aktif</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Slider</button>
                                </div>
                        </div>
                        </div>
                        	</div>
                	</form>
            </div>
</div>


    </div>
    <div class="modal fade bs-example-modal-lg" id="gantigambar" tabindex="-1" role="dialog">
    <div class="modal-dialog modal-lg" role="document">
        <div class="modal-content" >
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                <h4 class="modal-title">Form Ganti Gambar Service</h4> </div>
            <div class="modal-body">
                <div class="form-group">
                    <label class="control-label">Input Gambar:</label>
                    <input type="file" class="form-control" id="imageservice" required>
                </div>
                <div class="form-group">
                    <div class="row">
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-12">Crop Gambar</label>
                                <div class="col-md-12">
                                    <div id="upload-demo-service"></div>
                                    <button class="btn btn-primary btn-block upload-image-service-edit" id="upload-image-service-edit" style="margin-top: 5px">Crop Gambar</button>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label class="col-md-12">Hasil Crop</label>
                                <div class="col-md-12">
                                    <div id="preview-crop-image" style="background:#9d9d9d; width:800px; height:400px;"></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <form method="POST" action="{{ url('admin/slider/update-gambar', $slider->slider_id  ) }}">
                @csrf
                @method('PATCH')
                <input type="hidden" name="gambar" id="preview-crop-image-input">
                <div class="modal-footer">
                    <button id="submitupdategambar" class="btn btn-primary">Simpan</button>
            </form>
        </div>
    </div>
</div>
    </div>



@include('admin.template.footer')
