@include('admin.template.header')
@include('admin.template.navbar')
<meta name="csrf-token" content="{{ csrf_token() }}">


<div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Tambah Slider</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="">Dashboard</a></li>
                            <li><a href="{{ url('admin/slider') }}">Slider</a></li>
                            <li class="active">Tambah Slider</li>
                        </ol>
                    </div>
                </div>
                <!-- .row -->

                        <div class="white-box">
                            <h3 class="box-title m-b-0">Form Tambah Slider</h3>
                            <p class="text-muted m-b-30 font-13">Tambah Slider</p>
                            @csrf
                                <div class="row">
                                    <div class="col-md-4">
                                        <div class="form-group" id="imagetambahfull">
                                            <label class="col-md-12">Gambar Slider</label>
                                            <div class="col-md-12">
                                                <button class="btn btn-info btn-block" style="margin-top:10px" data-toggle="modal" data-target="#gantigambar"><i class="fa fa-image"></i>&nbsp;Input Gambar Slider</button>
                                            </div>
                                        </div>
                                        <div class="form-group">
                                            <label class="col-md-12" id="label-preview-crop-image" style="display:none;">Gambar 1 Hasil Crop</label>
                                            <div class="col-md-12">
                                                <div id="preview-crop-image" style="background:#fff; width:250px; height:150px; display:none;"></div>
                                            </div>
                                        </div>
                                    </div>
                                    <form class="form-horizontal" method="POST" action="{{ url('admin/slider/tambah/simpan') }}" enctype="multipart/form-data">
                	                @csrf
                                    <input type="hidden" name="gambar" id="preview-crop-image-input">
                                    <div class="col-md-8">
                                        <div class="form-group">
                                            <label class="col-md-12">Type Slider</label>
                                            <div class="col-md-12">
                                                <select name="type" id="tipeslider" class="form-control" onchange="tipeSlider()">
                                                    <option disabled selected>Pilih Tipe Slider</option>
                                                    <option value="url">url</option>
                                                    <option value="text">text</option>
                                                </select>
                                            </div>
                                        </div>

                                        <div class="form-group">
                                            <label class="col-md-12">Judul Slider</label>
                                            <div class="col-md-12">
                                                <input type="text" name="judul" class="form-control" placeholder="judul Slider..." required>
                                            </div>
                                        </div>
                                        <div class="form-group" id="deskripsi-slider" style="display: none">
                                            <label class="col-md-12">Deskripsi Slider</label>
                                            <div class="col-md-12">
                                                <textarea name="deskripsi" class="form-control" id="deskripsi" rows="4" placeholder="Deskripsi Slider..."></textarea>
                                            </div>
                                        </div>
                                        <div class="form-group" id="url-slider" style="display: none">
                                            <label class="col-md-12">Url Slider</label>
                                            <div class="col-md-12">
                                                <input type="text" name="url" class="form-control" placeholder="Url Slider...">
                                            </div>
                                        </div>
                                        <script type="text/javascript">
                                            function tipeSlider(){
                                                var tipeslider = document.getElementById("tipeslider").value;

                                                if (tipeslider === "url") {
                                                    document.getElementById("deskripsi-slider").style.display = "none";
                                                    document.getElementById("url-slider").style.display = "block";
                                                } else if (tipeslider === "text") {
                                                    document.getElementById("deskripsi-slider").style.display = "block";
                                                    document.getElementById("url-slider").style.display = "none";
                                                } else {}
                                            }
                                        </script>
                                        <div class="form-group" style="float: right">
                                            <label class="col-md-12" style="text-align: right">Status Slider</label>
                                            <div class="col-md-12" style="text-align: right">
                                                <select name="status">
                                                    <option  value="active">aktif</option>
                                                    <option  value="non-active">non-aktif</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Slider</button>
                                </div>
                            	</div>
                        </div>
                        	</div>
                	</form>
            </div>

                                        <div class="modal fade" id="gantigambar" tabindex="-1" role="dialog">
                                            <div class="modal-dialog modal-lg" role="document">
                                                <div class="modal-content" >
                                                    <div class="modal-header">
                                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                                                        <h4 class="modal-title">Crop Gambar Slider</h4> </div>
                                                        <div class="modal-body">
                                                            <div class="form-group">
                                                                        <div class="form-group">
                                                                            <label class="control-label">Input Gambar:</label>
                                                                            <input type="file" class="form-control" id="imagetambah" required>
                                                                        </div>
                                                                        <div class="form-group">
                                                                            <label class="col-md-12">Crop Gambar</label>
                                                                            <div class="col-md-12">
                                                                                <div id="upload-demo"></div>
                                                                            </div>
                                                                        </div>
                                                            </div>
                                                        </div>
                                                    <div class="modal-footer">
                                                    <button class="btn btn-primary btn-block upload-image" id="upload-image">Crop Gambar</button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>

@include('admin.template.footer')
