@include('admin.template.header')
@include('admin.template.navbar')
<!-- ============================================================== -->
<!-- Page Content -->
<!-- ============================================================== -->
<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Buku Tamu</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                    <li class="active">Buku Tamu</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- ============================================================== -->
        <!-- Other sales widgets -->
        <!-- ============================================================== -->

    <!-- @if(Session::has('success'))
        <div class="alert alert-success alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('success') }}
                </div>
            @endif
    @if(Session::has('error'))
        <div class="alert alert-danger alert-dismissible">
        <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('error') }}
                </div>
            @endif -->

        <div class="panel">
            <div class="panel-heading">MANAJEMEN BUKU TAMU
            </div>
            <div class="table-responsive">
                <table class="table table-hover manage-u-table">
                    <thead>
                    <tr>
                        <th width="10" class="text-center">#</th>
                        <th width="50" class="text-center">NAMA</th>
                        <th width="50" class="text-center">EMAIL</th>
                        <th width="50" class="text-center">SUBJECT</th>
                        <td width="30" class="text-center">AKSI </td>
                    </tr>
                    </thead>
                    <tbody>
                    @foreach($dataBuku as $buku => $sli)
                        <tr>
                            <td class="text-center">{{ $buku + $dataBuku->firstitem() }}</td>
                            <td class="text-center">{{ $sli->guest_name }}</td>
                            <td class="text-center">{{ $sli->guest_email }}</td>
                            <td class="text-center">{{ $sli->guest_subject }}</td>
                            <td  class="text-center">
                                <button type="button" class="btn btn-sm btn-rounded btn-warning" data-toggle="modal" data-target="#myModal{{ $sli->guest_id  }}">read</button>
                            </td>
                        </tr>
                        <div class="modal" tabindex="-1" role="dialog" id="myModal{{ $sli->guest_id  }}">
                            <div class="modal-dialog" role="document">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title">Pesan</h5>
                                        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                            <span aria-hidden="true">&times;</span>
                                        </button>
                                    </div>
                                    <div class="modal-body">
                                        <p>{{ $sli->guest_message }}</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    @endforeach
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    </div>
            <div>
                <div class="col-md-12">
                    <div style="float: right">{{ $dataBuku->links() }}</div>
                </div>
            </div>
        </div>
    </div>
@include('admin.template.footer')