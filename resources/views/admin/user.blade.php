@include('admin.template.header')
@include('admin.template.navbar')
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman User</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                            <li class="active">User</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- ============================================================== -->
                <!-- Other sales widgets -->
                <!-- ============================================================== -->

            <!-- @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('success') }}
                </div>
            @endif
            @if(Session::has('error'))
                <div class="alert alert-danger alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('error') }}
                </div>
            @endif -->

                    <div class="panel">
                        <div class="panel-heading">MANAJEMEN USER
                        <a class="btn btn-info btn-sm btn-rounded float-right" style="float: right;" href="{{ url('admin/user/tambah') }}"><i class="fa fa-plus"></i>&nbsp;TAMBAH USER</a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover manage-u-table">
                                <thead>
                                    <tr>
                                        <th width="70" class="text-center">#</th>
                                        <th width="50" class="text-center">PIC</th>
                                        <th>NAMA USER</th>
                                        <th>EMAIL USER</th>
                                        <th>ROLE USER</th>
                                        <th width="150" class="text-center">AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($user as $users => $use)
                                    <tr>
                                        <td class="text-center">{{ $users + $user->firstitem() }}</td>
                                        <td><img src="{{ asset('assets/uploads/foto_profile/'.$use->foto_profile) }}" alt="" style="max-width:100%"></td>
                                        <td><span class="font-medium">{{ $use->name }}</span></td>
                                        <td><span class="font-medium">{{ $use->email }}</span></td>
                                        <td><span class="font-medium">{{ $use->roleuser->nama_role }}</span></td>
                                        <td class="text-center">
                                            <!-- <form action="{{ url('admin/user/hapus', $use->id) }}" method="POST" id="submit-form">
                                            @csrf
                                            @method('delete') -->
                                            <a href="{{ url('admin/user/edit', $use->id) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                            <a href="{{ url('admin/user/hapus', $use->id) }}" class="btn btn-sm btn-rounded btn-danger delete-confirm">Hapus</a>
                                            <!-- <button type="submit" class="btn btn-sm btn-rounded btn-danger" alt="alert" id="sa-params">Hapus</button>
                                            </form> -->
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>

                            <div class="col-md-12">
                                <div style="float: right">{{ $user->links() }}</div>
                            </div>
                        </div>
                    </div>
            </div>
@include('admin.template.footer')
