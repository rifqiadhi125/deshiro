@include('admin.template.header')
@include('admin.template.navbar')

<div id="page-wrapper">
    <div class="container-fluid">
        <div class="row bg-title">
            <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                <h4 class="page-title">Halaman Setting Privacy</h4> </div>
            <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                <ol class="breadcrumb">
                    <li><a href="">Dashboard</a></li>
                    <li class="active">Setting Privacy</li>
                </ol>
            </div>
            <!-- /.col-lg-12 -->
        </div>
        <!-- .row -->
        <form class="form-horizontal" method="POST" action="{{ url('admin/setting-privacy/update') }}" enctype="multipart/form-data">
            @csrf
            @method('patch')
            <div class="white-box">
                <h3 class="box-title m-b-0">Form Setting Privacy</h3>
                <p class="text-muted m-b-30 font-13">Setting Privacy</p>
                <div class="form-group">
                    <label class="col-md-12">Privacy</label>
                    <div class="col-md-12">
                        <textarea name="formprivacy" id="deskripsi" class="form-control" cols="10" rows="5" placeholder="Privacy Setting..." required>{{ $setting[3]->setting_value }}</textarea>
                    </div>
                </div>
                <button class="btn btn-info btn-block"><i class="fa fa-send"></i>&nbsp;Simpan Setting</button>
            </div>
    </div>
</div>
</form>
</div>
@include('admin.template.footer')
