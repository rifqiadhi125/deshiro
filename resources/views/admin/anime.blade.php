@include('admin.template.header')
@include('admin.template.navbar')
        <!-- ============================================================== -->
        <!-- Page Content -->
        <!-- ============================================================== -->
        <div id="page-wrapper">
            <div class="container-fluid">
                <div class="row bg-title">
                    <div class="col-lg-3 col-md-4 col-sm-4 col-xs-12">
                        <h4 class="page-title">Halaman Genre</h4> </div>
                    <div class="col-lg-9 col-sm-8 col-md-8 col-xs-12">
                        <ol class="breadcrumb">
                            <li><a href="{{ url('admin/dashboard') }}">Dashboard</a></li>
                            <li class="active">Genre</li>
                        </ol>
                    </div>
                    <!-- /.col-lg-12 -->
                </div>
                <!-- ============================================================== -->
                <!-- Other sales widgets -->
                <!-- ============================================================== -->
            
            @if(Session::has('success'))
                <div class="alert alert-success alert-dismissible">
                <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    {{ Session('success') }}
                </div>
            @endif    

            <div class="row">
                <div class="col-md-12">
                    <div class="panel">
                        <div class="panel-heading">MANAJEMEN ANIME
                            <a class="btn btn-info btn-sm btn-rounded float-right" style="float: right;" href="{{ url('admin/anime/tambah') }}"><i class="fa fa-plus"></i>&nbsp;TAMBAH ANIME</a>
                        </div>
                        <div class="table-responsive">
                            <table class="table table-hover manage-u-table">
                                <thead>
                                    <tr>
                                        <th width="70" class="text-center">#</th>
                                        <th>NAMA ANIME</th>
                                        <th class="text-center">GENRE ANIME</th>
                                        <th>EPISODE</th>
                                        <th width="150" class="text-center">AKSI</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @foreach($anime as $animes => $ani)
                                    <tr>
                                        <td class="text-center">{{ $animes + $anime->firstitem() }}</td>
                                        <td><span class="font-medium">{{ $ani->judul_anime }}</span></td>
                                        <td class="text-center">
                                            <span class="font-medium">
                                                @foreach($ani->genre as $gen)
                                                    {{ $gen->nama_genre }}  
                                                @endforeach
                                            </span>
                                        </td>
                                        <td class="text-center">
                                            <?php $episode = DB::table('anime_episode')->where('anime_id_anime', $ani->id_anime)->count();
                                            echo $episode ?>
                                        </td>
                                        <td class="text-center">
                                            <form action="{{ url('admin/anime/hapus', $ani->id_anime) }}" method="POST">
                                            @csrf
                                            @method('delete')
                                            <a href="{{ url('admin/anime/edit', $ani->id_anime) }}" class="btn btn-sm btn-rounded btn-warning">Edit</a>
                                            <button type="submit" class="btn btn-sm btn-rounded btn-danger">Hapus</button>
                                            </form>
                                        </td>
                                    </tr>
                                    @endforeach
                                </tbody>
                            </table>
                            <div class="col-md-12">
                                <div style="float: right">{{ $anime->links() }}</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            </div>
@include('admin.template.footer')