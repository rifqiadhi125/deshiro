<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateServiceTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_service', function (Blueprint $table) {
            $table->bigIncrements('service_id');
            $table->string('service_title', '225');
            $table->text('service_desc');
            $table->string('service_images', '225');
            $table->enum('service_status', ['0', '1']);
            $table->dateTime('service_create_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_service');
    }
}
