<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_content', function (Blueprint $table) {
            $table->bigIncrements('content_id');
            $table->string('c_cat_id', '50');
            $table->integer('user_id');
            $table->string('content_name', '200');
            $table->enum('content_type', ['blog', 'slider']);
            $table->text('content_image');
            $table->string('content_alias', '200');
            $table->text('content_sortdesc');
            $table->longText('content_desc');
            $table->text('content_tags');
            $table->integer('content_hits');
            $table->enum('content_status', ['draft', 'publish']);
            $table->dateTime('content_create_date');
            $table->dateTime('content_publish_date');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_content');
    }
}
