<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSlidersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_slider', function (Blueprint $table) {
            $table->bigIncrements('slider_id');
            $table->enum('slider_type', ['text', 'url']);
            $table->string('slider_title', 50);
            $table->string('slider_shortdesc', 500);
            $table->text('slider_image');
            $table->string('slider_url', 500);
            $table->enum('slider_status', ['active', 'non-active']);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_slider');
    }
}
