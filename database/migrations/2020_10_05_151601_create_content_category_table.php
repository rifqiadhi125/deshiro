<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateContentCategoryTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('_content_category', function (Blueprint $table) {
            $table->bigIncrements('c_cat_id');
            $table->string('c_cat_name', '100');
            $table->string('c_cat_alias', '100');
            $table->text('c_cat_desc');
            $table->string('c_cat_image', '100');
            $table->integer('c_cat_parent');
            $table->integer('c_cat_level');
            $table->enum('c_cat_status', ['0', '1']);
            $table->string('c_cat_root', '30');
            $table->integer('c_cat_order');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('_content_category');
    }
}
