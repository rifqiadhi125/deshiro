<?php

Route::get('/login', 'AuthController@loginform');
Route::post('/ceklogin', 'AuthController@login');
Route::get('/register', 'AuthController@registerform');
Route::post('/registersave', 'AuthController@register');
Route::get('/logout', 'AuthController@logout');
