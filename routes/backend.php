<?php


// ADMIN
route::group(['middleware' => ['auth', 'checkRole:1']], function(){

	Route::get('/admin/dashboard', 'AnimeController@dashboard');

	// ROLE USER
	Route::get('/admin/role-user', 'RoleUserController@tampil_role_user');
	Route::get('/admin/role-user/tambah', 'RoleUserController@tambah_role_user');
	Route::post('/admin/role-user/tambah/simpan', 'RoleUserController@tambah_simpan_role_user');
	Route::get('/admin/role-user/edit/{id}', 'RoleUserController@edit_role_user');
	Route::patch('/admin/role-user/update/{id}', 'RoleUserController@update_role_user');
	Route::delete('/admin/role-user/hapus/{id}', 'RoleUserController@hapus_role_user');

	// USER
	Route::get('/admin/user', 'UserController@tampil_user');
	Route::get('/admin/user/tambah', 'UserController@tambah_user');
	Route::post('/admin/user/tambah/simpan', 'UserController@tambah_simpan_user');
	Route::get('/admin/user/edit/{id}', 'UserController@edit_user');
	Route::patch('/admin/user/update/{id}', 'UserController@update_user');
	Route::get('/admin/user/hapus/{id}', 'UserController@hapus_user');
	Route::patch('/admin/user/update-password/{id}', 'UserController@update_password');

	// CUSTOMER
	Route::get('/admin/customer', 'CustomerController@tampil_customer');
	Route::get('/admin/customer/tambah', 'CustomerController@tambah_customer');
	Route::post('/admin/customer/tambah/simpan', 'CustomerController@tambah_simpan_customer');
	Route::get('/admin/customer/edit/{id}', 'CustomerController@edit_customer');
	Route::patch('/admin/customer/update/{id}', 'CustomerController@update_customer');
	Route::get('/admin/customer/hapus/{id}', 'CustomerController@hapus_customer');
	Route::post('/admin/customer-import', 'CustomerController@import_customer');
	Route::get('/admin/customer-export', 'CustomerController@export_customer');

	// SLIDER
	Route::get('/admin/slider', 'SliderController@tampil_slider');
	Route::get('/admin/slider/tambah', 'SliderController@tambah_slider');
	Route::post('/admin/slider/tambah/simpan', 'SliderController@tambah_simpan_slider');
	Route::get('/admin/slider/edit/{id}', 'SliderController@edit_slider');
	Route::patch('/admin/slider/update/{id}', 'SliderController@update_slider');
	Route::get('/admin/slider/hapus/{id}', 'SliderController@hapus_slider');
	Route::patch('/admin/slider/update-gambar/{id}', 'SliderController@update_gambar');
//    Route::patch('/admin/slider/update-gambar-2/{id}', 'SliderController@update_gambar_2');

	Route::get('crop-image-before-upload-using-croppie', 'CropImageController@index');
	Route::post('crop-image-before-upload-using-croppie', ['as'=>'croppie.upload-image','uses'=>'CropImageController@uploadCropImage']);
    Route::post('/uploadimagesck', 'SliderController@uploadImage')->name('post.image');

    Route::resource('image', 'ImageController');

    // SETTING
    Route::get('/admin/setting-general', 'SettingController@tampil_setting');
    Route::patch('/admin/setting-general/update', 'SettingController@update_setting');
    Route::get('/admin/setting-about', 'SettingController@tampil_about');
    Route::patch('/admin/setting-about/update', 'SettingController@update_about');
    Route::get('/admin/setting-privacy', 'SettingController@tampil_privacy');
    Route::patch('/admin/setting-privacy/update', 'SettingController@update_privacy');

    // SERVICE
    Route::get('/admin/service', 'ServiceController@tampil_service');
    Route::get('/admin/service/tambah', 'ServiceController@tambah_service');
    Route::post('/admin/service/tambah/simpan', 'ServiceController@tambah_simpan_service');
    Route::get('/admin/service/edit/{id}', 'ServiceController@edit_service');
    Route::patch('/admin/service/update/{id}', 'ServiceController@update_service');
    Route::get('/admin/service/hapus/{id}', 'ServiceController@hapus_service');
    Route::patch('/admin/service/update-gambar/{id}', 'ServiceController@update_gambar');

    Route::get('admin/service-about','NewTabController@service');
    Route::get('admin/content','ContentController@content');
    Route::get('admin/content/tambah','TambahController@tambah');
    Route::post('admin/content/simpan','SimpanController@simpan');

    //PORTFOLIO
    Route::get('admin/portfolio','PortfolioController@tampil_portfolio');
    Route::get('admin/tambah_portfolio','PortfolioController@tambah_portfolio');
    Route::get('admin/portfolio/edit/{id}','PortfolioController@edit_portfolio');
    Route::get('admin/portfolio/hapus/{id}','PortfolioController@hapus');
    Route::post('admin/tambah_portfolio/simpan','PortfolioController@simpan');
    Route::patch('admin/tambah_portfolio/update/{id}','PortfolioController@update');
    Route::patch('admin/portfolio/update_gambar/{id}','PortfolioController@update_gambar');


    //BUKU TAMU
    Route::get('admin/buku','BukuController@tampil_buku');
    Route::get('admin/buku/read/{id}','BukuController@read');


    // CONTENT
    Route::get('/admin/content', 'ContentController@tampil_content');
    Route::get('/admin/content/tambah', 'ContentController@tambah_content');
    Route::post('/admin/content/tambah/simpan', 'ContentController@tambah_simpan_content');
    Route::get('/admin/content/edit/{id}', 'ContentController@edit_content');
    Route::patch('/admin/content/update/{id}', 'ContentController@update_content');
    Route::get('/admin/content/hapus/{id}', 'ContentController@hapus_content');
    Route::patch('/admin/content/update-gambar/{id}', 'ContentController@update_gambar');

    // CONTENT CATEGORY
    Route::get('/admin/content-category', 'ContentCategoryController@tampil_category');
    Route::get('/admin/content-category/tambah', 'ContentCategoryController@tambah_category');
    Route::post('/admin/content-category/tambah/simpan', 'ContentCategoryController@tambah_simpan_category');
    Route::get('/admin/content-category/edit/{id}', 'ContentCategoryController@edit_category');
    Route::patch('/admin/content-category/update/{id}', 'ContentCategoryController@update_category');
    Route::get('/admin/content-category/hapus/{id}', 'ContentCategoryController@hapus_category');
    Route::patch('/admin/content-category/update-gambar/{id}', 'ContentCategoryController@update_gambar');
});
