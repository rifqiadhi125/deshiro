<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::get('api','ApiController@api');

Route::get('user','UserapiController@api');
Route::post('store','UserapiController@store');
Route::post('delete','UserapiController@hapus');
Route::post('update','UserapiController@update');
Route::post('tambah','UserapiController@tambah');

Route::post('hapus','UserapiController@hapus');
Route::post('update','UserapiController@update');

